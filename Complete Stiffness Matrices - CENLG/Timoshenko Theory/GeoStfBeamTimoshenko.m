% GeoStfBeamTimoshenko.m -  this file contains 3-dimensional Timoshenko geometric
%                            stiffness matrix using Taylor series expansion.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% Authors - Marcos Antonio Campos Rodrigues (Departamento de Engenharia Civil, Universidade Federal do Esp�rito Santo)
%           Rodrigo Bird Burgos (Departamento de Estruturas e Funda��es, Universidade do Estado do Rio de Janeiro)
%           Luiz Fernando Martha (Departamento de Engenharia Civil e Ambiental, Pontif�cia Universidade Cat�lica do Rio de Janeiro)
%
%            Reference:
%                M. A. C. Rodrigues. "Integrated Solutions for the Formulations of The Geometric Nonlinearity Problem."
%                DSc. Thesis - Pontif�cia Universidade Cat�lica do Rio de Janeiro,
%                Departamento de Engenharia Civil e Ambiental, 2019.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% function [elm_geo] = GeoStfBeamTimoshenko(order,L,E,G,Ax,Ay,Az,Ix,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb)
%
%      L - Current element length;                                                    (  in )
%      E - Young's modulus;                                                           (  in )
%      G - Shear modulus;                                                             (  in )
%      Ax - Element cross section area;                                               (  in )
%      Ay - Effective shear area in y direct.;                                        (  in )
%      Az - Effective shear area in z direct.;                                        (  in )
%      Iy - Mom. of inertia wrt "y" axis;                                             (  in )
%      Iz - Mom. of inertia wrt "z" axis;                                             (  in )
%      Fya, Fza, Mxa, Mya, Mza - Forces and moments (element initial node)            (  in )
%      Fxb, Fyb, Fzb, Mxb, Myb, Mzb - Forces and moments (element final node)         (  in )
%
%      order - Number of terms considered in stiffness matrix                         (  in )
%           Example how to use different orders:
%
%   		 case 2 -> 2 Terms for tangent stiffness matrix (1 elastic + 1 term for axial load P);
%   		    [elm_geo2] = GeoStfBeamTimoshenko(2,L,E,G,Ax,Ay,Az,Ix,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb);	
%
%			 case 3 -> 3 Terms for tangent stiffness matrix (1 elastic + 1 term for axial load P + 1 term for axial load P^2);
%   		    [elm_geo3] = GeoStfBeamTimoshenko(3,L,E,G,Ax,Ay,Az,Ix,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb);
%   		    
%   		 case 4 -> 4 Terms for tangent stiffness matrix (1 elastic + 1 term for axial load P + 1 term for axial load P^2 + 1 term for axial load P^3);
%   		    [elm_geo4] = GeoStfBeamTimoshenko(4,L,E,G,Ax,Ay,Az,Ix,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb);
%
%            elm_geo = elm_geo2      /      elm_geo = elm_geo2 + elm_geo3      /      elm_geo = elm_geo2 + elm_geo3 + elm_geo4
%
%      elm_geo - Geometric stiffness matrix                                           ( out )
%
%      Returns tangent stiffness matrix of the element.
%      This function calculates the local tangent stiffness matrix for an element considering the Timoshenko beam theory.
%      This matrix is obtained directly from the solution of the equilibrium differential equation of an deformed infinitesimal element (Rodrigues, 2019).
%     
% ----------------------------------------------------------------------------------------------------------------------------------------------

function [elm_geo] = GeoStfBeamTimoshenko(order,L,E,G,Ax,Ay,Az,Ix,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb)

Oy = (E*Iz)/(G*Ay*L^2);     % Shear to Flexural rig. ratio y                         
Oz = (E*Iy)/(G*Az*L^2);     % Shear to Flexural rig. ratio z

Fxb2 = Fxb*Fxb;
Fxb3 = Fxb2*Fxb;
niy = (1.0 + 12.0*Oy);
niy2 = niy*niy;
niy3 = niy2*niy;
niy4 = niy3*niy;
niy5 = niy4*niy;
niz = (1.0 + 12.0*Oz);
niz2 = niz*niz;
niz3 = niz2*niz;
niz4 = niz3*niz;
niz5 = niz4*niz;
L2 = L*L;
L3 = L2*L;
L4 = L3*L;
L5 = L4*L;
L6 = L5*L;
Oy2 = Oy*Oy;
Oy3 = Oy2*Oy;
Oy4 = Oy3*Oy;
Oy5 = Oy4*Oy;
Oy6 = Oy5*Oy;
Oy7 = Oy6*Oy;
Oz2 = Oz*Oz;
Oz3 = Oz2*Oz;
Oz4 = Oz3*Oz;
Oz5 = Oz4*Oz;
Oz6 = Oz5*Oz;
Oz7 = Oz6*Oz;
Iy2 = Iy * Iy;
Iy3 = Iy2 * Iy;
Iz2 = Iz * Iz;
Iz3 = Iz2 * Iz;
E2 = E * E;
E3 = E2 * E;
E4 = E3 * E;
E5 = E4 * E;
E6 = E5 * E;
K = Fxb*(Iy + Iz) / Ax;

elm_geo = zeros(12,12);

switch (order)
    
% ========== 2 terms ========== %
case 2
elm_geo(1,1) = Fxb / L;
elm_geo(2,2) = 6.0*Fxb*(120.0*Oy2 + 20.0*Oy + 1.0) / (5.0*niy2*L) + 12.0*Fxb*Iz / (Ax*L3*niy2);
elm_geo(3,3) = 6.0*Fxb*(120.0*Oz2 + 20.0*Oz + 1.0) / (5.0*niz2*L) + 12.0*Fxb*Iy / (Ax*L3*niz2);
elm_geo(4,4) = K / L;
elm_geo(5,5) = 2.0*Fxb*L*(90.0*Oz2 + 15.0*Oz + 1.0) / (15.0*niz2) + 4.0*Fxb*Iy*(36.0*Oz2 + 6.0*Oz + 1.0) / (Ax*L*niz2);
elm_geo(6,6) = 2.0*Fxb*L*(90.0*Oy2 + 15.0*Oy + 1.0) / (15.0*niy2) + 4.0*Fxb*Iz*(36.0*Oy2 + 6.0*Oy + 1.0) / (Ax*L*niy2);

elm_geo(3,4) = Mza / L;
elm_geo(4,5) = -Mza / 3.0 + Mzb / 6.0;
elm_geo(6,7) = Mza / L;
elm_geo(9,10) = -Mzb / L;
elm_geo(10,11) = Mza / 6.0 - Mzb / 3.0;

elm_geo(2,4) = Mya / L;
elm_geo(3,5) = -Fxb / (10.0*niz2) - 6.0*Fxb*Iy / (Ax*L2*niz2);
elm_geo(4,6) = Mya / 3.0 - Myb / 6.0;
elm_geo(5,7) = Mya / L;
elm_geo(6,8) = -Fxb / (10.0*niy2) - 6.0*Fxb*Iz / (Ax*L2*niy2);
elm_geo(8,10) = -Myb / L;
elm_geo(9,11) = Fxb / (10.0*niz2) + 6.0*Fxb*Iy / (Ax*L2*niz2);
elm_geo(10,12) = -Mya / 6.0 + Myb / 3.0;

elm_geo(2,5) = (Mxb / L)*(1 / niz);
elm_geo(3,6) = (Mxb / L)*(1 / niy);
elm_geo(5,8) = (-Mxb / L)*(1 / niz);
elm_geo(6,9) = (-Mxb / L)*(1 / niy);
elm_geo(8,11) = (Mxb / L)*(1 / niz);
elm_geo(9,12) = (Mxb / L)*(1 / niy);

elm_geo(1,5) = -Mya / L;
elm_geo(2,6) = Fxb / (10.0*niy2) + 6.0*Fxb*Iz / (Ax*L2*niy2);
elm_geo(4,8) = -Mya / L;
elm_geo(5,9) = Fxb / (10.0*niz2) + 6.0*Fxb*Iy / (Ax*L2*niz2);
elm_geo(6,10) = Mya / 6.0 + Myb / 6.0;
elm_geo(7,11) = Myb / L;
elm_geo(8,12) = -Fxb / (10.0*niy2) - 6.0*Fxb*Iz / (Ax*L2*niy2);

elm_geo(1,6) = -Mza / L;
elm_geo(4,9) = -Mza / L;
elm_geo(5,10) = -Mza / 6.0 - Mzb / 6.0;
elm_geo(6,11) = (-Mxb / 2.0)*((1 - 144 * Oy*Oz) / (niy*niz));
elm_geo(7,12) = Mzb / L;

elm_geo(1,7) = -Fxb / L;
elm_geo(2,8) = -6.0*Fxb*(120.0*Oy2 + 20.0*Oy + 1.0) / (5.0*niy2*L) - 12.0*Fxb*Iz / (Ax*L3*niy2);
elm_geo(3,9) = -6.0*Fxb*(120.0*Oz2 + 20.0*Oz + 1.0) / (5.0*niz2*L) - 12.0*Fxb*Iy / (Ax*L3*niz2);
elm_geo(4,10) = -K / L;
elm_geo(5,11) = -Fxb*L*(360.0*Oz2 + 60.0*Oz + 1.0) / (30.0*niz2) - 2.0*Fxb*Iy*(72.0*Oz2 + 12.0*Oz - 1.0) / (Ax*L*niz2);
elm_geo(6,12) = -Fxb*L*(360.0*Oy2 + 60.0*Oy + 1.0) / (30.0*niy2) - 2.0*Fxb*Iz*(72.0*Oy2 + 12.0*Oy - 1.0) / (Ax*L*niy2);

elm_geo(3,10) = Mzb / L;
elm_geo(4,11) = -Mza / 6.0 - Mzb / 6.0;
elm_geo(5,12) = (Mxb / 2.0)*((1 - 144 * Oy*Oz) / (niy*niz));

elm_geo(2,10) = Myb / L;
elm_geo(3,11) = -Fxb / (10.0*niz2) - 6.0*Fxb*Iy / (Ax*L2*niz2);
elm_geo(4,12) = Mya / 6.0 + Myb / 6.0;

elm_geo(2,11) = (-Mxb / L)*(1 / niz);
elm_geo(3,12) = (-Mxb / L)*(1 / niy);

elm_geo(1,11) = -Myb / L;
elm_geo(2,12) = Fxb / (10.0*niy2) + 6.0*Fxb*Iz / (Ax*L2*niy2);

elm_geo(1,12) = -Mzb / L;

elm_geo(5,6) = -6 * Mxb*(Oy - Oz) / (niy*niz);
elm_geo(11,12) = 6 * Mxb*(Oy - Oz) / (niy*niz);

% ========== 3 terms ========== %
case 3
elm_geo(1,1) = 0;
elm_geo(2,2) = ((L*(907200 * Oy3 + 172800 * Oy2 + 8640 * Oy + 45)) / (31500 * E*Iz*niy4) - ((L*(1680 * Oy2 + 180 * Oy + 1)) / (350 * niy3) + (24 * Iz*Oy) / (5 * Ax*L*niy3)) / (E*Iz))*Fxb2;
elm_geo(3,3) = ((L*(907200 * Oz3 + 172800 * Oz2 + 8640 * Oz + 45)) / (31500 * E*Iy*niz4) - ((L*(1680 * Oz2 + 180 * Oz + 1)) / (350 * niz3) + (24 * Iy*Oz) / (5 * Ax*L*niz3)) / (E*Iy))*Fxb2;
elm_geo(4,4) = 0;
elm_geo(5,5) = (-((L3 * (907200 * Oz4 + 241920 * Oz3 + 26460 * Oz2 + 1245 * Oz + 11)) / (3150 * niz3) + (6 * Iy*L*Oz) / (5 * Ax*niz3)) / (E*Iy) + (L3 * (163296000 * Oz5 + 57153600 * Oz4 + 8391600 * Oz3 + 621000 * Oz2 + 20655 * Oz + 165)) / (94500 * E*Iy*niz4))*Fxb2;  
elm_geo(6,6) = (-((L3 * (907200 * Oy4 + 241920 * Oy3 + 26460 * Oy2 + 1245 * Oy + 11)) / (3150 * niy3) + (6 * Iz*L*Oy) / (5 * Ax*niy3)) / (E*Iz) + (L3 * (163296000 * Oy5 + 57153600 * Oy4 + 8391600 * Oy3 + 621000 * Oy2 + 20655 * Oy + 165)) / (94500 * E*Iz*niy4))*Fxb2;

elm_geo(3,4) = 0;
elm_geo(4,5) = ((Mza + Mzb)*((40 * L4) / (E2 * Iy2) + (100800 * L4 * Oz2) / (E2 * Iy2) + (3360 * L4 * Oz) / (E2 * Iy2))*Fxb2) / 604800 - (((1680 * L2) / (E*Iy) + (100800 * L2 * Oz) / (E*Iy))*(Mza + Mzb)*Fxb) / 604800;
elm_geo(6,7) = 0;
elm_geo(9,10) = 0;
elm_geo(10,11) = ((Mza + Mzb)*((40 * L4) / (E2 * Iy2) + (100800 * L4 * Oz2) / (E2 * Iy2) + (3360 * L4 * Oz) / (E2 * Iy2))*Fxb2) / 604800 - (((1680 * L2) / (E*Iy) + (100800 * L2 * Oz) / (E*Iy))*(Mza + Mzb)*Fxb) / 604800;

elm_geo(2,4) = 0;
elm_geo(3,5) = (((L2 * (1680 * Oz2 + 180 * Oz + 1)) / (700 * niz3) + (12 * Iy*Oz) / (5 * Ax*niz3)) / (E*Iy) - (L2 * (907200 * Oz3 + 172800 * Oz2 + 8640 * Oz + 45)) / (63000 * E*Iy*niz4))*Fxb2;
elm_geo(4,6) = -((Mya + Myb)*(100800 * L4 * Oy2 + 3360 * L4 * Oy + 40 * L4)*Fxb2) / (604800 * E2 * Iz2) + ((100800 * L2 * Oy + 1680 * L2)*(Mya + Myb)*Fxb) / (604800 * E*Iz);
elm_geo(5,7) = 0;
elm_geo(6,8) = (((L2 * (1680 * Oy2 + 180 * Oy + 1)) / (700 * niy3) + (12 * Iz*Oy) / (5 * Ax*niy3)) / (E*Iz) - (L2 * (907200 * Oy3 + 172800 * Oy2 + 8640 * Oy + 45)) / (63000 * E*Iz*niy4))*Fxb2;
elm_geo(8,10) = 0;
elm_geo(9,11) = ((L2 * (907200 * Oz3 + 172800 * Oz2 + 8640 * Oz + 45)) / (63000 * E*Iy*niz4) - ((L2 * (1680 * Oz2 + 180 * Oz + 1)) / (700 * niz3) + (12 * Iy*Oz) / (5 * Ax*niz3)) / (E*Iy))*Fxb2;
elm_geo(10,12) = -((Mya + Myb)*(100800 * L4 * Oy2 + 3360 * L4 * Oy + 40 * L4)*Fxb2) / (604800 * E2 * Iz2) + ((100800 * L2 * Oy + 1680 * L2)*(Mya + Myb)*Fxb) / (604800 * E*Iz);

elm_geo(2,5) = ((Mxb*(40 * E*Iy*L4 + 1680 * E*Iy*L4 *Oz)) / (100800 * E3 * Iy3 * L*(12 * Oy + 1)) + (L*Mxb*(3891888000 * E4 * Iy2 * Iz2 * L2 + 249080832000 * E4 * Iy2 * Iz2 * L2 * Oy + 8966909952000 * E4 * Iy3 * Iz*L2 * Oy2 + 179338199040000 * E4 * Iy3 * Iz*L2 * Oy3 + 941525544960000 * E4 * Iy3 * Iz*L2 * Oy4 + 4296644352000 * E4 * Iy2 * Iz2 * L2 * Oy2 + 22417274880000 * E4 * Iy2 * Iz2 * L2 * Oy3 + 46702656000 * E4 * Iy3 * Iz*L2 * Oy)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb2 + (-(L*Mxb) / (60 * E*Iy*(12 * Oy + 1)) - (L*Mxb*(156920924160000 * E5 * Iy3 * Iz2 * Oy2 + 941525544960000 * E5 * Iy3 * Iz2 * Oy3 + 6538371840000 * E5 * Iy3 * Iz2 * Oy)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb;
elm_geo(3,6) = ((Mxb*(40 * E*Iz*L4 + 1680 * E*Iz*L4 *Oy)) / (100800 * E3 * Iz3 * L*(12 * Oz + 1)) + (L*Mxb*(3891888000 * E4 * Iy2 * Iz2 * L2 + 249080832000 * E4 * Iy2 * Iz2 * L2 * Oz + 8966909952000 * E4 * Iy*Iz3 * L2 * Oz2 + 179338199040000 * E4 * Iy*Iz3 * L2 * Oz3 + 941525544960000 * E4 * Iy*Iz3 * L2 * Oz4 + 4296644352000 * E4 * Iy2 * Iz2 * L2 * Oz2 + 22417274880000 * E4 * Iy2 * Iz2 * L2 * Oz3 + 46702656000 * E4 * Iy*Iz3 * L2 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb2 + (-(L*Mxb) / (60 * E*Iz*(12 * Oz + 1)) - (L*Mxb*(156920924160000 * E5 * Iy2 * Iz3 * Oz2 + 941525544960000 * E5 * Iy2 * Iz3 * Oz3 + 6538371840000 * E5 * Iy2 * Iz3 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb;
elm_geo(5,8) = (-(Mxb*(40 * E*Iy*L4 + 1680 * E*Iy*L4 * Oz)) / (100800 * E3 * Iy3 * L*(12 * Oy + 1)) - (L*Mxb*(3891888000 * E4 * Iy2 * Iz2 * L2 + 249080832000 * E4 * Iy2 * Iz2 * L2 * Oy + 8966909952000 * E4 * Iy3 * Iz*L2 * Oy2 + 179338199040000 * E4 * Iy3 * Iz*L2 * Oy3 + 941525544960000 * E4 * Iy3 * Iz*L2 * Oy4 + 4296644352000 * E4 * Iy2 * Iz2 * L2 * Oy2 + 22417274880000 * E4 * Iy2 * Iz2 * L2 * Oy3 + 46702656000 * E4 * Iy3 * Iz*L2 * Oy)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb2 + ((L*Mxb) / (60 * E*Iy*(12 * Oy + 1)) + (L*Mxb*(156920924160000 * E5 * Iy3 * Iz2 * Oy2 + 941525544960000 * E5 * Iy3 * Iz2 * Oy3 + 6538371840000 * E5 * Iy3 * Iz2 * Oy)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb;
elm_geo(6,9) = (-(Mxb*(40 * E*Iz*L4 + 1680 * E*Iz*L4 * Oy)) / (100800 * E3 * Iz3 * L*(12 * Oz + 1)) - (L*Mxb*(3891888000 * E4 * Iy2 * Iz2 * L2 + 249080832000 * E4 * Iy2 * Iz2 * L2 * Oz + 8966909952000 * E4 * Iy*Iz3 * L2 * Oz2 + 179338199040000 * E4 * Iy*Iz3 * L2 * Oz3 + 941525544960000 * E4 * Iy*Iz3 * L2 * Oz4 + 4296644352000 * E4 * Iy2 * Iz2 * L2 * Oz2 + 22417274880000 * E4 * Iy2 * Iz2 * L2 * Oz3 + 46702656000 * E4 * Iy*Iz3 * L2 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb2 + ((L*Mxb) / (60 * E*Iz*(12 * Oz + 1)) + (L*Mxb*(156920924160000 * E5 * Iy2 * Iz3 * Oz2 + 941525544960000 * E5 * Iy2 * Iz3 * Oz3 + 6538371840000 * E5 * Iy2 * Iz3 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb;
elm_geo(8,11) = ((Mxb*(40 * E*Iy*L4 + 1680 * E*Iy*L4 * Oz)) / (100800 * E3 * Iy3 * L*(12 * Oy + 1)) + (L*Mxb*(3891888000 * E4 * Iy2 * Iz2 * L2 + 249080832000 * E4 * Iy2 * Iz2 * L2 * Oy + 8966909952000 * E4 * Iy3 * Iz*L2 * Oy2 + 179338199040000 * E4 * Iy3 * Iz*L2 * Oy3 + 941525544960000 * E4 * Iy3 * Iz*L2 * Oy4 + 4296644352000 * E4 * Iy2 * Iz2 * L2 * Oy2 + 22417274880000 * E4 * Iy2 * Iz2 * L2 * Oy3 + 46702656000 * E4 * Iy3 * Iz*L2 * Oy)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb2 + (-(L*Mxb) / (60 * E*Iy*(12 * Oy + 1)) - (L*Mxb*(156920924160000 * E5 * Iy3 * Iz2 * Oy2 + 941525544960000 * E5 * Iy3 * Iz2 * Oy3 + 6538371840000 * E5 * Iy3 * Iz2 * Oy)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb;
elm_geo(9,12) = ((Mxb*(40 * E*Iz*L4 + 1680 * E*Iz*L4 * Oy)) / (100800 * E3 * Iz3 * L*(12 * Oz + 1)) + (L*Mxb*(3891888000 * E4 * Iy2 * Iz2 * L2 + 249080832000 * E4 * Iy2 * Iz2 * L2 * Oz + 8966909952000 * E4 * Iy*Iz3 * L2 * Oz2 + 179338199040000 * E4 * Iy*Iz3 * L2 * Oz3 + 941525544960000 * E4 * Iy*Iz3 * L2 * Oz4 + 4296644352000 * E4 * Iy2 * Iz2 * L2 * Oz2 + 22417274880000 * E4 * Iy2 * Iz2 * L2 * Oz3 + 46702656000 * E4 * Iy*Iz3 * L2 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb2 + (-(L*Mxb) / (60 * E*Iz*(12 * Oz + 1)) - (L*Mxb*(156920924160000 * E5 * Iy2 * Iz3 * Oz2 + 941525544960000 * E5 * Iy2 * Iz3 * Oz3 + 6538371840000 * E5 * Iy2 * Iz3 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb;

elm_geo(1,5) = 0;
elm_geo(2,6) = ((L2 * (907200 * Oy3 + 172800 * Oy2 + 8640 * Oy + 45)) / (63000 * E*Iz*niy4) - ((L2 * (1680 * Oy2 + 180 * Oy + 1)) / (700 * niy3) + (12 * Iz*Oy) / (5 * Ax*niy3)) / (E*Iz))*Fxb2;
elm_geo(4,8) = 0;
elm_geo(5,9) = ((L2 * (907200 * Oz3 + 172800 * Oz2 + 8640 * Oz + 45)) / (63000 * E*Iy*niz4) - ((L2 * (1680 * Oz2 + 180 * Oz + 1)) / (700 * niz3) + (12 * Iy*Oz) / (5 * Ax*niz3)) / (E*Iy))*Fxb2;
elm_geo(6,10) = ((Mya + Myb)*(100800 * L4 * Oy2 + 3360 * L4 * Oy + 40 * L4)*Fxb2) / (604800 * E2 * Iz2) - ((100800 * L2 * Oy + 1680 * L2)*(Mya + Myb)*Fxb) / (604800 * E*Iz);
elm_geo(7,11) = 0;
elm_geo(8,12) = (((L2 * (1680 * Oy2 + 180 * Oy + 1)) / (700 * niy3) + (12 * Iz*Oy) / (5 * Ax*niy3)) / (E*Iz) - (L2 * (907200 * Oy3 + 172800 * Oy2 + 8640 * Oy + 45)) / (63000 * E*Iz*niy4))*Fxb2;

elm_geo(1,6) = 0;
elm_geo(4,9) = 0;
elm_geo(5,10) = -((Mza + Mzb)*((40 * L4) / (E2 * Iy2) + (100800 * L4 * Oz2) / (E2 * Iy2) + (3360 * L4 * Oz) / (E2 * Iy2))*Fxb2) / 604800 + (((1680 * L2) / (E*Iy) + (100800 * L2 * Oz) / (E*Iy))*(Mza + Mzb)*Fxb) / 604800;
elm_geo(6,11) = -(L4 * Mxb*(52254720 * Iy2 * Oy4 * Oz2 + 8709120 * Iy2 * Oy4 * Oz + 362880 * Iy2 * Oy4 + 52254720 * Iy2 * Oy3 * Oz3 + 27371520 * Iy2 * Oy3 * Oz2 + 3473280 * Iy2 * Oy3 * Oz + 129600 * Iy2 * Oy3 + 5598720 * Iy2 * Oy2 * Oz3 + 2799360 * Iy2 * Oy2 * Oz2 + 349920 * Iy2 * Oy2 * Oz + 12960 * Iy2 * Oy2 + 31104 * Iy2 * Oy*Oz3 + 63936 * Iy2 * Oy*Oz2 + 10008 * Iy2 * Oy*Oz + 408 * Iy2 * Oy + 720 * Iy2 * Oz2 + 120 * Iy2 * Oz + 5 * Iy2 + 1244160 * Iy*Iz*Oy3 * Oz2 + 134784 * Iy*Iz*Oy3 * Oz + 2592 * Iy*Iz*Oy3 + 1244160 * Iy*Iz*Oy2 * Oz3 + 622080 * Iy*Iz*Oy2 * Oz2 + 59616 * Iy*Iz*Oy2 * Oz + 1368 * Iy*Iz*Oy2 + 134784 * Iy*Iz*Oy*Oz3 + 59616 * Iy*Iz*Oy*Oz2 + 5616 * Iy*Iz*Oy*Oz + 132 * Iy*Iz*Oy + 2592 * Iy*Iz*Oz3 + 1368 * Iy*Iz*Oz2 + 132 * Iy*Iz*Oz + 3 * Iy*Iz + 52254720 * Iz2 * Oy3 * Oz3 + 5598720 * Iz2 * Oy3 * Oz2 + 31104 * Iz2 * Oy3 * Oz + 52254720 * Iz2 * Oy2 * Oz4 + 27371520 * Iz2 * Oy2 * Oz3 + 2799360 * Iz2 * Oy2 * Oz2 + 63936 * Iz2 * Oy2 * Oz + 720 * Iz2 * Oy2 + 8709120 * Iz2 * Oy*Oz4 + 3473280 * Iz2 * Oy*Oz3 + 349920 * Iz2 * Oy*Oz2 + 10008 * Iz2 * Oy*Oz + 120 * Iz2 * Oy + 362880 * Iz2 * Oz4 + 129600 * Iz2 * Oz3 + 12960 * Iz2 * Oz2 + 408 * Iz2 * Oz + 5 * Iz2)*Fxb2) / (25200 * E2 * Iy2 * Iz2 * niy3 * niz3) + (L2 * Mxb*(Iy + Iz + 144 * Iy*Oy2 + 144 * Iz*Oz2 + 36 * Iy*Oy + 12 * Iy*Oz + 12 * Iz*Oy + 36 * Iz*Oz + 576 * Iy*Oy*Oz + 576 * Iz*Oy*Oz + 1728 * Iy*Oy*Oz2 + 1728 * Iy*Oy2 * Oz + 1728 * Iz*Oy*Oz2 + 1728 * Iz*Oy2 * Oz)*Fxb) / (120 * E*Iy*Iz*niy2 * niz2);
elm_geo(7,12) = 0;  

elm_geo(1,7) = 0;
elm_geo(2,8) = (((L*(1680 * Oy2 + 180 * Oy + 1)) / (350 * niy3) + (24 * Iz*Oy) / (5 * Ax*L*niy3)) / (E*Iz) - (L*(907200 * Oy3 + 172800 * Oy2 + 8640 * Oy + 45)) / (31500 * E*Iz*niy4))*Fxb2;
elm_geo(3,9) = (((L*(1680 * Oz2 + 180 * Oz + 1)) / (350 * niz3) + (24 * Iy*Oz) / (5 * Ax*L*niz3)) / (E*Iy) - (L*(907200 * Oz3 + 172800 * Oz2 + 8640 * Oz + 45)) / (31500 * E*Iy*niz4))*Fxb2;
elm_geo(4,10) = 0;
elm_geo(5,11) = (((L3 * (1814400 * Oz4 + 483840 * Oz3 + 37800 * Oz2 + 870 * Oz + 13)) / (6300 * niz3) - (6 * Iy*L*Oz) / (5 * Ax*niz3)) / (E*Iy) - (L3 * (326592000 * Oz5 + 114307200 * Oz4 + 14061600 * Oz3 + 723600 * Oz2 + 15390 * Oz + 195)) / (189000 * E*Iy*niz4))*Fxb2;
elm_geo(6,12) = (((L3 * (1814400 * Oy4 + 483840 * Oy3 + 37800 * Oy2 + 870 * Oy + 13)) / (6300 * niy3) - (6 * Iz*L*Oy) / (5 * Ax*niy3)) / (E*Iz) - (L3 * (326592000 * Oy5 + 114307200 * Oy4 + 14061600 * Oy3 + 723600 * Oy2 + 15390 * Oy + 195)) / (189000 * E*Iz*niy4))*Fxb2;

elm_geo(3,10) = 0;
elm_geo(4,11) = -((Mza + Mzb)*((40 * L4) / (E2 * Iy2) + (100800 * L4 * Oz2) / (E2 * Iy2) + (3360 * L4 * Oz) / (E2 * Iy2))*Fxb2) / 604800 + (((1680 * L2) / (E*Iy) + (100800 * L2 * Oz) / (E*Iy))*(Mza + Mzb)*Fxb) / 604800;
elm_geo(5,12) = (L4 * Mxb*(52254720 * Iy2 * Oy4 * Oz2 + 8709120 * Iy2 * Oy4 * Oz + 362880 * Iy2 * Oy4 + 52254720 * Iy2 * Oy3 * Oz3 + 27371520 * Iy2 * Oy3 * Oz2 + 3473280 * Iy2 * Oy3 * Oz + 129600 * Iy2 * Oy3 + 5598720 * Iy2 * Oy2 * Oz3 + 2799360 * Iy2 * Oy2 * Oz2 + 349920 * Iy2 * Oy2 * Oz + 12960 * Iy2 * Oy2 + 31104 * Iy2 * Oy*Oz3 + 63936 * Iy2 * Oy*Oz2 + 10008 * Iy2 * Oy*Oz + 408 * Iy2 * Oy + 720 * Iy2 * Oz2 + 120 * Iy2 * Oz + 5 * Iy2 + 1244160 * Iy*Iz*Oy3 * Oz2 + 134784 * Iy*Iz*Oy3 * Oz + 2592 * Iy*Iz*Oy3 + 1244160 * Iy*Iz*Oy2 * Oz3 + 622080 * Iy*Iz*Oy2 * Oz2 + 59616 * Iy*Iz*Oy2 * Oz + 1368 * Iy*Iz*Oy2 + 134784 * Iy*Iz*Oy*Oz3 + 59616 * Iy*Iz*Oy*Oz2 + 5616 * Iy*Iz*Oy*Oz + 132 * Iy*Iz*Oy + 2592 * Iy*Iz*Oz3 + 1368 * Iy*Iz*Oz2 + 132 * Iy*Iz*Oz + 3 * Iy*Iz + 52254720 * Iz2 * Oy3 * Oz3 + 5598720 * Iz2 * Oy3 * Oz2 + 31104 * Iz2 * Oy3 * Oz + 52254720 * Iz2 * Oy2 * Oz4 + 27371520 * Iz2 * Oy2 * Oz3 + 2799360 * Iz2 * Oy2 * Oz2 + 63936 * Iz2 * Oy2 * Oz + 720 * Iz2 * Oy2 + 8709120 * Iz2 * Oy*Oz4 + 3473280 * Iz2 * Oy*Oz3 + 349920 * Iz2 * Oy*Oz2 + 10008 * Iz2 * Oy*Oz + 120 * Iz2 * Oy + 362880 * Iz2 * Oz4 + 129600 * Iz2 * Oz3 + 12960 * Iz2 * Oz2 + 408 * Iz2 * Oz + 5 * Iz2)*Fxb2) / (25200 * E2 * Iy2 * Iz2 * niy3 * niz3) - (L2 * Mxb*(Iy + Iz + 144 * Iy*Oy2 + 144 * Iz*Oz2 + 36 * Iy*Oy + 12 * Iy*Oz + 12 * Iz*Oy + 36 * Iz*Oz + 576 * Iy*Oy*Oz + 576 * Iz*Oy*Oz + 1728 * Iy*Oy*Oz2 + 1728 * Iy*Oy2 * Oz + 1728 * Iz*Oy*Oz2 + 1728 * Iz*Oy2 * Oz)*Fxb) / (120 * E*Iy*Iz*niy2 * niz2);

elm_geo(2,10) = 0;
elm_geo(3,11) = (((L2 * (1680 * Oz2 + 180 * Oz + 1)) / (700 * niz3) + (12 * Iy*Oz) / (5 * Ax*niz3)) / (E*Iy) - (L2 * (907200 * Oz3 + 172800 * Oz2 + 8640 * Oz + 45)) / (63000 * E*Iy*niz4))*Fxb2;
elm_geo(4,12) = ((Mya + Myb)*(100800 * L4 * Oy2 + 3360 * L4 * Oy + 40 * L4)*Fxb2) / (604800 * E2 * Iz2) - ((100800 * L2 * Oy + 1680 * L2)*(Mya + Myb)*Fxb) / (604800 * E*Iz);

elm_geo(2,11) = (-(Mxb*(40 * E*Iy*L4 + 1680 * E*Iy*L4 * Oz)) / (100800 * E3 * Iy3 * L*(12 * Oy + 1)) - (L*Mxb*(3891888000 * E4 * Iy2 * Iz2 * L2 + 249080832000 * E4 * Iy2 * Iz2 * L2 * Oy + 8966909952000 * E4 * Iy3 * Iz*L2 * Oy2 + 179338199040000 * E4 * Iy3 * Iz*L2 * Oy3 + 941525544960000 * E4 * Iy3 * Iz*L2 * Oy4 + 4296644352000 * E4 * Iy2 * Iz2 * L2 * Oy2 + 22417274880000 * E4 * Iy2 * Iz2 * L2 * Oy3 + 46702656000 * E4 * Iy3 * Iz*L2 * Oy)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb2 + ((L*Mxb) / (60 * E*Iy*(12 * Oy + 1)) + (L*Mxb*(156920924160000 * E5 * Iy3 * Iz2 * Oy2 + 941525544960000 * E5 * Iy3 * Iz2 * Oy3 + 6538371840000 * E5 * Iy3 * Iz2 * Oy)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb;
elm_geo(3,12) = (-(Mxb*(40 * E*Iz*L4 + 1680 * E*Iz*L4 * Oy)) / (100800 * E3 * Iz3 * L*(12 * Oz + 1)) - (L*Mxb*(3891888000 * E4 * Iy2 * Iz2 * L2 + 249080832000 * E4 * Iy2 * Iz2 * L2 * Oz + 8966909952000 * E4 * Iy*Iz3 * L2 * Oz2 + 179338199040000 * E4 * Iy*Iz3 * L2 * Oz3 + 941525544960000 * E4 * Iy*Iz3 * L2 * Oz4 + 4296644352000 * E4 * Iy2 * Iz2 * L2 * Oz2 + 22417274880000 * E4 * Iy2 * Iz2 * L2 * Oz3 + 46702656000 * E4 * Iy*Iz3 * L2 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb2 + ((L*Mxb) / (60 * E*Iz*(12 * Oz + 1)) + (L*Mxb*(156920924160000 * E5 * Iy2 * Iz3 * Oz2 + 941525544960000 * E5 * Iy2 * Iz3 * Oz3 + 6538371840000 * E5 * Iy2 * Iz3 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb;

elm_geo(1,11) = 0;
elm_geo(2,12) = ((L2 * (907200 * Oy3 + 172800 * Oy2 + 8640 * Oy + 45)) / (63000 * E*Iz*niy4) - ((L2 * (1680 * Oy2 + 180 * Oy + 1)) / (700 * niy3) + (12 * Iz*Oy) / (5 * Ax*niy3)) / (E*Iz))*Fxb2;

elm_geo(1,12) = 0;

elm_geo(5,6) = -(L4 * Mxb*(52254720 * Iy2 * Oy4 * Oz2 + 8709120 * Iy2 * Oy4 * Oz + 362880 * Iy2 * Oy4 - 52254720 * Iy2 * Oy3 * Oz3 + 1244160 * Iy2 * Oy3 * Oz2 + 1296000 * Iy2 * Oy3 * Oz + 69120 * Iy2 * Oy3 - 5598720 * Iy2 * Oy2 * Oz3 + 116640 * Iy2 * Oy2 * Oz + 6480 * Iy2 * Oy2 - 31104 * Iy2 * Oy*Oz3 + 48384 * Iy2 * Oy*Oz2 + 8712 * Iy2 * Oy*Oz + 372 * Iy2 * Oy + 720 * Iy2 * Oz2 + 120 * Iy2 * Oz + 5 * Iy2 + 1244160 * Iy*Iz*Oy3 * Oz2 + 134784 * Iy*Iz*Oy3 * Oz + 2592 * Iy*Iz*Oy3 - 1244160 * Iy*Iz*Oy2 * Oz3 + 7776 * Iy*Iz*Oy2 * Oz - 72 * Iy*Iz*Oy2 - 134784 * Iy*Iz*Oy*Oz3 - 7776 * Iy*Iz*Oy*Oz2 - 24 * Iy*Iz*Oy - 2592 * Iy*Iz*Oz3 + 72 * Iy*Iz*Oz2 + 24 * Iy*Iz*Oz + 52254720 * Iz2 * Oy3 * Oz3 + 5598720 * Iz2 * Oy3 * Oz2 + 31104 * Iz2 * Oy3 * Oz - 52254720 * Iz2 * Oy2 * Oz4 - 1244160 * Iz2 * Oy2 * Oz3 - 48384 * Iz2 * Oy2 * Oz - 720 * Iz2 * Oy2 - 8709120 * Iz2 * Oy*Oz4 - 1296000 * Iz2 * Oy*Oz3 - 116640 * Iz2 * Oy*Oz2 - 8712 * Iz2 * Oy*Oz - 120 * Iz2 * Oy - 362880 * Iz2 * Oz4 - 69120 * Iz2 * Oz3 - 6480 * Iz2 * Oz2 - 372 * Iz2 * Oz - 5 * Iz2)*Fxb2) / (25200 * E2 * Iy2 * Iz2 * niy3 * niz3) + (L2 * Mxb*(Iy - Iz + 144 * Iy*Oy2 - 144 * Iz*Oz2 + 12 * Iy*Oy + 12 * Iy*Oz - 12 * Iz*Oy - 12 * Iz*Oz - 1728 * Iy*Oy*Oz2 + 1728 * Iy*Oy2 * Oz - 1728 * Iz*Oy*Oz2 + 1728 * Iz*Oy2 * Oz)*Fxb) / (120 * E*Iy*Iz*niy2 * niz2);
elm_geo(11,12) = (L4 * Mxb*(52254720 * Iy2 * Oy4 * Oz2 + 8709120 * Iy2 * Oy4 * Oz + 362880 * Iy2 * Oy4 - 52254720 * Iy2 * Oy3 * Oz3 + 1244160 * Iy2 * Oy3 * Oz2 + 1296000 * Iy2 * Oy3 * Oz + 69120 * Iy2 * Oy3 - 5598720 * Iy2 * Oy2 * Oz3 + 116640 * Iy2 * Oy2 * Oz + 6480 * Iy2 * Oy2 - 31104 * Iy2 * Oy*Oz3 + 48384 * Iy2 * Oy*Oz2 + 8712 * Iy2 * Oy*Oz + 372 * Iy2 * Oy + 720 * Iy2 * Oz2 + 120 * Iy2 * Oz + 5 * Iy2 + 1244160 * Iy*Iz*Oy3 * Oz2 + 134784 * Iy*Iz*Oy3 * Oz + 2592 * Iy*Iz*Oy3 - 1244160 * Iy*Iz*Oy2 * Oz3 + 7776 * Iy*Iz*Oy2 * Oz - 72 * Iy*Iz*Oy2 - 134784 * Iy*Iz*Oy*Oz3 - 7776 * Iy*Iz*Oy*Oz2 - 24 * Iy*Iz*Oy - 2592 * Iy*Iz*Oz3 + 72 * Iy*Iz*Oz2 + 24 * Iy*Iz*Oz + 52254720 * Iz2 * Oy3 * Oz3 + 5598720 * Iz2 * Oy3 * Oz2 + 31104 * Iz2 * Oy3 * Oz - 52254720 * Iz2 * Oy2 * Oz4 - 1244160 * Iz2 * Oy2 * Oz3 - 48384 * Iz2 * Oy2 * Oz - 720 * Iz2 * Oy2 - 8709120 * Iz2 * Oy*Oz4 - 1296000 * Iz2 * Oy*Oz3 - 116640 * Iz2 * Oy*Oz2 - 8712 * Iz2 * Oy*Oz - 120 * Iz2 * Oy - 362880 * Iz2 * Oz4 - 69120 * Iz2 * Oz3 - 6480 * Iz2 * Oz2 - 372 * Iz2 * Oz - 5 * Iz2)*Fxb2) / (25200 * E2 * Iy2 * Iz2 * niy3 * niz3) - (L2 * Mxb*(Iy - Iz + 144 * Iy*Oy2 - 144 * Iz*Oz2 + 12 * Iy*Oy + 12 * Iy*Oz - 12 * Iz*Oy - 12 * Iz*Oz - 1728 * Iy*Oy*Oz2 + 1728 * Iy*Oy2 * Oz - 1728 * Iz*Oy*Oz2 + 1728 * Iz*Oy2 * Oz)*Fxb) / (120 * E*Iy*Iz*niy2 * niz2);

% ========== 4 terms ========== %
case 4
elm_geo(1,1) = 0;
elm_geo(2,2) = (((L3 * (21772800 * Oy5 + 6480000 * Oy4 + 665280 * Oy3 + 25920 * Oy2 + 252 * Oy + 1)) / (21000 * niy5) + (Iz*L*(483840 * Oy4 + 97920 * Oy3 + 5376 * Oy2 + 60 * Oy + 1)) / (700 * Ax*niy5)) / (E2 * Iz2) - (L*(1814400 * L2 * Oy4 + 388800 * L2 * Oy3 + 23040 * L2 * Oy2 + 240 * L2 * Oy + L2)) / (31500 * E2 * Iz2 * niy4))*Fxb3;
elm_geo(3,3) = (((L3 * (21772800 * Oz5 + 6480000 * Oz4 + 665280 * Oz3 + 25920 * Oz2 + 252 * Oz + 1)) / (21000 * niz5) + (Iy*L*(483840 * Oz4 + 97920 * Oz3 + 5376 * Oz2 + 60 * Oz + 1)) / (700 * Ax*niz5)) / (E2 * Iy2) - (L*(1814400 * L2 * Oz4 + 388800 * L2 * Oz3 + 23040 * L2 * Oz2 + 240 * L2 * Oz + L2)) / (31500 * E2 * Iy2 * niz4))*Fxb3;
elm_geo(4,4) = 0;
elm_geo(5,5) = (((L5 * (3919104000 * Oz7 + 1763596800 * Oz6 + 344476800 * Oz5 + 37260000 * Oz4 + 2307960 * Oz3 + 75690 * Oz2 + 1089 * Oz + 7)) / (63000 * niz5) + (Iy*L3 * (2177280 * Oz5 + 1995840 * Oz4 + 371520 * Oz3 + 24696 * Oz2 + 660 * Oz + 11)) / (6300 * Ax*niz5)) / (E2 * Iy2) - (L3 * (326592000 * L2 * Oz6 + 119750400 * L2 * Oz5 + 18727200 * L2 * Oz4 + 1544400 * L2 * Oz3 + 63630 * L2 * Oz2 + 1005 * L2 * Oz + 7 * L2)) / (94500 * E2 * Iy2 * niz4))*Fxb3;
elm_geo(6,6) = (((L5 * (3919104000 * Oy7 + 1763596800 * Oy6 + 344476800 * Oy5 + 37260000 * Oy4 + 2307960 * Oy3 + 75690 * Oy2 + 1089 * Oy + 7)) / (63000 * niy5) + (Iz*L3 * (2177280 * Oy5 + 1995840 * Oy4 + 371520 * Oy3 + 24696 * Oy2 + 660 * Oy + 11)) / (6300 * Ax*niy5)) / (E2 * Iz2) - (L3 * (326592000 * L2 * Oy6 + 119750400 * L2 * Oy5 + 18727200 * L2 * Oy4 + 1544400 * L2 * Oy3 + 63630 * L2 * Oy2 + 1005 * L2 * Oy + 7 * L2)) / (94500 * E2 * Iz2 * niy4))*Fxb3;

elm_geo(3,4) = 0;
elm_geo(4,5) = -((Mza + Mzb)*(L6 / (E3 * Iy3) + (5040 * L6 * Oz2) / (E3 * Iy3) + (100800 * L6 * Oz3) / (E3 * Iy3) + (120 * L6 * Oz) / (E3 * Iy3))*Fxb3) / 604800;
elm_geo(6,7) = 0;
elm_geo(9,10) = 0;
elm_geo(10,11) = -((Mza + Mzb)*(L6 / (E3 * Iy3) + (5040 * L6 * Oz2) / (E3 * Iy3) + (100800 * L6 * Oz3) / (E3 * Iy3) + (120 * L6 * Oz) / (E3 * Iy3))*Fxb3) / 604800;

elm_geo(2,4) = 0;
elm_geo(3,5) = (-((L4 * (21772800 * Oz5 + 6480000 * Oz4 + 665280 * Oz3 + 25920 * Oz2 + 252 * Oz + 1)) / (42000 * niz5) + (Iy*L2 * (483840 * Oz4 + 97920 * Oz3 + 5376 * Oz2 + 60 * Oz + 1)) / (1400 * Ax*niz5)) / (E2 * Iy2) + (L2 * (1814400 * L2 * Oz4 + 388800 * L2 * Oz3 + 23040 * L2 * Oz2 + 240 * L2 * Oz + L2)) / (63000 * E2 * Iy2 * niz4))*Fxb3;
elm_geo(4,6) = ((Mya + Myb)*(100800 * L6 * Oy3 + 5040 * L6 * Oy2 + 120 * L6 * Oy + L6)*Fxb3) / (604800 * E3 * Iz3);
elm_geo(5,7) = 0;
elm_geo(6,8) = (-((L4 * (21772800 * Oy5 + 6480000 * Oy4 + 665280 * Oy3 + 25920 * Oy2 + 252 * Oy + 1)) / (42000 * niy5) + (Iz*L2 * (483840 * Oy4 + 97920 * Oy3 + 5376 * Oy2 + 60 * Oy + 1)) / (1400 * Ax*niy5)) / (E2 * Iz2) + (L2 * (1814400 * L2 * Oy4 + 388800 * L2 * Oy3 + 23040 * L2 * Oy2 + 240 * L2 * Oy + L2)) / (63000 * E2 * Iz2 * niy4))*Fxb3;
elm_geo(8,10) = 0;
elm_geo(9,11) = (((L4 * (21772800 * Oz5 + 6480000 * Oz4 + 665280 * Oz3 + 25920 * Oz2 + 252 * Oz + 1)) / (42000 * niz5) + (Iy*L2 * (483840 * Oz4 + 97920 * Oz3 + 5376 * Oz2 + 60 * Oz + 1)) / (1400 * Ax*niz5)) / (E2 * Iy2) - (L2 * (1814400 * L2 * Oz4 + 388800 * L2 * Oz3 + 23040 * L2 * Oz2 + 240 * L2 * Oz + L2)) / (63000 * E2 * Iy2 * niz4))*Fxb3;
elm_geo(10,12) = ((Mya + Myb)*(100800 * L6 * Oy3 + 5040 * L6 * Oy2 + 120 * L6 * Oy + L6)*Fxb3) / (604800 * E3 * Iz3);

elm_geo(2,5) = (-(Mxb*(1680 * L6 * Oz2 + 80 * L6 * Oz + L6)) / (100800 * E3 * Iy3 * L*(12 * Oy + 1)) - (L*Mxb*(124540416000 * E3 * Iy3 * L4 * Oy2 + 11955879936000 * E3 * Iy3 * L4 * Oy3 + 201755473920000 * E3 * Iy3 * L4 * Oy4 + 941525544960000 * E3 * Iy3 * L4 * Oy5 + 108108000 * E3 * Iy*Iz2 * L4 + 43243200 * E3 * Iy2 * Iz*L4 + 518918400 * E3 * Iy3 * L4 * Oy + 108972864000 * E3 * Iy*Iz2 * L4 * Oy2 + 326918592000 * E3 * Iy2 * Iz*L4 * Oy2 + 560431872000 * E3 * Iy*Iz2 * L4 * Oy3 + 4857076224000 * E3 * Iy2 * Iz*L4 * Oy3 + 22417274880000 * E3 * Iy2 * Iz*L4 * Oy4 + 6486480000 * E3 * Iy*Iz2 * L4 * Oy + 7005398400 * E3 * Iy2 * Iz*L4 * Oy + 3891888000 * E3 * Iy*Iz2 * L4 * Oz + 249080832000 * E3 * Iy*Iz2 * L4 * Oy*Oz + 4296644352000 * E3 * Iy*Iz2 * L4 * Oy2 * Oz + 22417274880000 * E3 * Iy*Iz2 * L4 * Oy3 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb3;
elm_geo(3,6) = (-(Mxb*(1680 * L6 * Oy2 + 80 * L6 * Oy + L6)) / (100800 * E3 * Iz3 * L*(12 * Oz + 1)) - (L*Mxb*(124540416000 * E3 * Iz3 * L4 * Oz2 + 11955879936000 * E3 * Iz3 * L4 * Oz3 + 201755473920000 * E3 * Iz3 * L4 * Oz4 + 941525544960000 * E3 * Iz3 * L4 * Oz5 + 43243200 * E3 * Iy*Iz2 * L4 + 108108000 * E3 * Iy2 * Iz*L4 + 518918400 * E3 * Iz3 * L4 * Oz + 326918592000 * E3 * Iy*Iz2 * L4 * Oz2 + 108972864000 * E3 * Iy2 * Iz*L4 * Oz2 + 4857076224000 * E3 * Iy*Iz2 * L4 * Oz3 + 560431872000 * E3 * Iy2 * Iz*L4 * Oz3 + 22417274880000 * E3 * Iy*Iz2 * L4 * Oz4 + 3891888000 * E3 * Iy2 * Iz*L4 * Oy + 7005398400 * E3 * Iy*Iz2 * L4 * Oz + 6486480000 * E3 * Iy2 * Iz*L4 * Oz + 249080832000 * E3 * Iy2 * Iz*L4 * Oy*Oz + 4296644352000 * E3 * Iy2 * Iz*L4 * Oy*Oz2 + 22417274880000 * E3 * Iy2 * Iz*L4 * Oy*Oz3)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb3;
elm_geo(5,8) = ((Mxb*(1680 * L6 * Oz2 + 80 * L6 * Oz + L6)) / (100800 * E3 * Iy3 * L*(12 * Oy + 1)) + (L*Mxb*(124540416000 * E3 * Iy3 * L4 * Oy2 + 11955879936000 * E3 * Iy3 * L4 * Oy3 + 201755473920000 * E3 * Iy3 * L4 * Oy4 + 941525544960000 * E3 * Iy3 * L4 * Oy5 + 108108000 * E3 * Iy*Iz2 * L4 + 43243200 * E3 * Iy2 * Iz*L4 + 518918400 * E3 * Iy3 * L4 * Oy + 108972864000 * E3 * Iy*Iz2 * L4 * Oy2 + 326918592000 * E3 * Iy2 * Iz*L4 * Oy2 + 560431872000 * E3 * Iy*Iz2 * L4 * Oy3 + 4857076224000 * E3 * Iy2 * Iz*L4 * Oy3 + 22417274880000 * E3 * Iy2 * Iz*L4 * Oy4 + 6486480000 * E3 * Iy*Iz2 * L4 * Oy + 7005398400 * E3 * Iy2 * Iz*L4 * Oy + 3891888000 * E3 * Iy*Iz2 * L4 * Oz + 249080832000 * E3 * Iy*Iz2 * L4 * Oy*Oz + 4296644352000 * E3 * Iy*Iz2 * L4 * Oy2 * Oz + 22417274880000 * E3 * Iy*Iz2 * L4 * Oy3 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb3;
elm_geo(6,9) = ((Mxb*(1680 * L6 * Oy2 + 80 * L6 * Oy + L6)) / (100800 * E3 * Iz3 * L*(12 * Oz + 1)) + (L*Mxb*(124540416000 * E3 * Iz3 * L4 * Oz2 + 11955879936000 * E3 * Iz3 * L4 * Oz3 + 201755473920000 * E3 * Iz3 * L4 * Oz4 + 941525544960000 * E3 * Iz3 * L4 * Oz5 + 43243200 * E3 * Iy*Iz2 * L4 + 108108000 * E3 * Iy2 * Iz*L4 + 518918400 * E3 * Iz3 * L4 * Oz + 326918592000 * E3 * Iy*Iz2 * L4 * Oz2 + 108972864000 * E3 * Iy2 * Iz*L4 * Oz2 + 4857076224000 * E3 * Iy*Iz2 * L4 * Oz3 + 560431872000 * E3 * Iy2 * Iz*L4 * Oz3 + 22417274880000 * E3 * Iy*Iz2 * L4 * Oz4 + 3891888000 * E3 * Iy2 * Iz*L4 * Oy + 7005398400 * E3 * Iy*Iz2 * L4 * Oz + 6486480000 * E3 * Iy2 * Iz*L4 * Oz + 249080832000 * E3 * Iy2 * Iz*L4 * Oy*Oz + 4296644352000 * E3 * Iy2 * Iz*L4 * Oy*Oz2 + 22417274880000 * E3 * Iy2 * Iz*L4 * Oy*Oz3)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb3;
elm_geo(8,11) = (-(Mxb*(1680 * L6 * Oz2 + 80 * L6 * Oz + L6)) / (100800 * E3 * Iy3 * L*(12 * Oy + 1)) - (L*Mxb*(124540416000 * E3 * Iy3 * L4 * Oy2 + 11955879936000 * E3 * Iy3 * L4 * Oy3 + 201755473920000 * E3 * Iy3 * L4 * Oy4 + 941525544960000 * E3 * Iy3 * L4 * Oy5 + 108108000 * E3 * Iy*Iz2 * L4 + 43243200 * E3 * Iy2 * Iz*L4 + 518918400 * E3 * Iy3 * L4 * Oy + 108972864000 * E3 * Iy*Iz2 * L4 * Oy2 + 326918592000 * E3 * Iy2 * Iz*L4 * Oy2 + 560431872000 * E3 * Iy*Iz2 * L4 * Oy3 + 4857076224000 * E3 * Iy2 * Iz*L4 * Oy3 + 22417274880000 * E3 * Iy2 * Iz*L4 * Oy4 + 6486480000 * E3 * Iy*Iz2 * L4 * Oy + 7005398400 * E3 * Iy2 * Iz*L4 * Oy + 3891888000 * E3 * Iy*Iz2 * L4 * Oz + 249080832000 * E3 * Iy*Iz2 * L4 * Oy*Oz + 4296644352000 * E3 * Iy*Iz2 * L4 * Oy2 * Oz + 22417274880000 * E3 * Iy*Iz2 * L4 * Oy3 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb3;
elm_geo(9,12) = (-(Mxb*(1680 * L6 * Oy2 + 80 * L6 * Oy + L6)) / (100800 * E3 * Iz3 * L*(12 * Oz + 1)) - (L*Mxb*(124540416000 * E3 * Iz3 * L4 * Oz2 + 11955879936000 * E3 * Iz3 * L4 * Oz3 + 201755473920000 * E3 * Iz3 * L4 * Oz4 + 941525544960000 * E3 * Iz3 * L4 * Oz5 + 43243200 * E3 * Iy*Iz2 * L4 + 108108000 * E3 * Iy2 * Iz*L4 + 518918400 * E3 * Iz3 * L4 * Oz + 326918592000 * E3 * Iy*Iz2 * L4 * Oz2 + 108972864000 * E3 * Iy2 * Iz*L4 * Oz2 + 4857076224000 * E3 * Iy*Iz2 * L4 * Oz3 + 560431872000 * E3 * Iy2 * Iz*L4 * Oz3 + 22417274880000 * E3 * Iy*Iz2 * L4 * Oz4 + 3891888000 * E3 * Iy2 * Iz*L4 * Oy + 7005398400 * E3 * Iy*Iz2 * L4 * Oz + 6486480000 * E3 * Iy2 * Iz*L4 * Oz + 249080832000 * E3 * Iy2 * Iz*L4 * Oy*Oz + 4296644352000 * E3 * Iy2 * Iz*L4 * Oy*Oz2 + 22417274880000 * E3 * Iy2 * Iz*L4 * Oy*Oz3)) / (32691859200000 * E6 * Iy3 * Iz3 * niz4))*Fxb3;

elm_geo(1,5) = 0;
elm_geo(2,6) = (((L4 * (21772800 * Oy5 + 6480000 * Oy4 + 665280 * Oy3 + 25920 * Oy2 + 252 * Oy + 1)) / (42000 * niy5) + (Iz*L2 * (483840 * Oy4 + 97920 * Oy3 + 5376 * Oy2 + 60 * Oy + 1)) / (1400 * Ax*niy5)) / (E2 * Iz2) - (L2 * (1814400 * L2 * Oy4 + 388800 * L2 * Oy3 + 23040 * L2 * Oy2 + 240 * L2 * Oy + L2)) / (63000 * E2 * Iz2 * niy4))*Fxb3;
elm_geo(4,8) = 0;
elm_geo(5,9) = (((L4 * (21772800 * Oz5 + 6480000 * Oz4 + 665280 * Oz3 + 25920 * Oz2 + 252 * Oz + 1)) / (42000 * niz5) + (Iy*L2 * (483840 * Oz4 + 97920 * Oz3 + 5376 * Oz2 + 60 * Oz + 1)) / (1400 * Ax*niy5)) / (E2 * Iy2) - (L2 * (1814400 * L2 * Oz4 + 388800 * L2 * Oz3 + 23040 * L2 * Oz2 + 240 * L2 * Oz + L2)) / (63000 * E2 * Iy2 * niz4))*Fxb3;
elm_geo(6,10) = -((Mya + Myb)*(100800 * L6 * Oy3 + 5040 * L6 * Oy2 + 120 * L6 * Oy + L6)*Fxb3) / (604800 * E3 * Iz3);
elm_geo(7,11) = 0;
elm_geo(8,12) = (-((L4 * (21772800 * Oy5 + 6480000 * Oy4 + 665280 * Oy3 + 25920 * Oy2 + 252 * Oy + 1)) / (42000 * niy5) + (Iz*L2 * (483840 * Oy4 + 97920 * Oy3 + 5376 * Oy2 + 60 * Oy + 1)) / (1400 * Ax*niy5)) / (E2 * Iz2) + (L2 * (1814400 * L2 * Oy4 + 388800 * L2 * Oy3 + 23040 * L2 * Oy2 + 240 * L2 * Oy + L2)) / (63000 * E2 * Iz2 * niy4))*Fxb3;

elm_geo(1,6) = 0;
elm_geo(4,9) = 0;
elm_geo(5,10) = ((Mza + Mzb)*(L6 / (E3 * Iy3) + (5040 * L6 * Oz2) / (E3 * Iy3) + (100800 * L6 * Oz3) / (E3 * Iy3) + (120 * L6 * Oz) / (E3 * Iy3))*Fxb3) / 604800;
elm_geo(6,11) = ((L6 * Mxb*(648 * Oz + 23040 * Oy*Oz2 + 2211840 * Oy*Oz3 + 37324800 * Oy*Oz4 + 174182400 * Oy*Oz5 + 33840 * Oz2 + 967680 * Oz3 + 13236480 * Oz4 + 80870400 * Oz5 + 174182400 * Oz6 + 96 * Oy*Oz + 5)) / (1008000 * E3 * Iy3 * niy*niz4) + (L6 * Mxb*(648 * Oy + 23040 * Oy2 * Oz + 2211840 * Oy3 * Oz + 37324800 * Oy4 * Oz + 174182400 * Oy5 * Oz + 33840 * Oy2 + 967680 * Oy3 + 13236480 * Oy4 + 80870400 * Oy5 + 174182400 * Oy6 + 96 * Oy*Oz + 5)) / (1008000 * E3 * Iz3 * niy4 * niz) + (L6 * Mxb*(12441600 * Oy4 * Oz + 311040 * Oy4 + 12441600 * Oy3 * Oz2 + 5495040 * Oy3 * Oz + 172800 * Oy3 + 1658880 * Oy2 * Oz2 + 613440 * Oy2 * Oz + 20160 * Oy2 + 43200 * Oy*Oz2 + 20880 * Oy*Oz + 660 * Oy + 288 * Oz2 + 228 * Oz + 7)) / (3024000 * E3 * Iy*Iz2 * niy3 * niz2) + (L6 * Mxb*(12441600 * Oy2 * Oz3 + 1658880 * Oy2 * Oz2 + 43200 * Oy2 * Oz + 288 * Oy2 + 12441600 * Oy*Oz4 + 5495040 * Oy*Oz3 + 613440 * Oy*Oz2 + 20880 * Oy*Oz + 228 * Oy + 311040 * Oz4 + 172800 * Oz3 + 20160 * Oz2 + 660 * Oz + 7)) / (3024000 * E3 * Iy2 * Iz*niy2 * niz3)) * Fxb3;
elm_geo(7,12) = 0;

elm_geo(1,7) = 0;
elm_geo(2,8) = (-((L3 * (21772800 * Oy5 + 6480000 * Oy4 + 665280 * Oy3 + 25920 * Oy2 + 252 * Oy + 1)) / (21000 * niy5) + (Iz*L*(483840 * Oy4 + 97920 * Oy3 + 5376 * Oy2 + 60 * Oy + 1)) / (700 * Ax*niy5)) / (E2 * Iz2) + (L*(1814400 * L2 * Oy4 + 388800 * L2 * Oy3 + 23040 * L2 * Oy2 + 240 * L2 * Oy + L2)) / (31500 * E2 * Iz2 * niy4))*Fxb3;
elm_geo(3,9) = (-((L3 * (21772800 * Oz5 + 6480000 * Oz4 + 665280 * Oz3 + 25920 * Oz2 + 252 * Oz + 1)) / (21000 * niz5) + (Iy*L*(483840 * Oz4 + 97920 * Oz3 + 5376 * Oz2 + 60 * Oz + 1)) / (700 * Ax*niz5)) / (E2 * Iy2) + (L*(1814400 * L2 * Oz4 + 388800 * L2 * Oz3 + 23040 * L2 * Oz2 + 240 * L2 * Oz + L2)) / (31500 * E2 * Iy2 * niz4))*Fxb3;
elm_geo(4,10) = 0;
elm_geo(5,11) = (-((L5 * (7838208000 * Oz7 + 3527193600 * Oz6 + 623635200 * Oz5 + 55080000 * Oz4 + 2620080 * Oz3 + 73620 * Oz2 + 1422 * Oz + 11)) / (126000 * niz5) + (Iy*L3 * (4354560 * Oz5 - 362880 * Oz4 - 138240 * Oz3 + 1008 * Oz2 + 780 * Oz + 13)) / (12600 * Ax*niz5)) / (E2 * Iy2) + (L3 * (653184000 * L2 * Oz6 + 239500800 * L2 * Oz5 + 32011200 * L2 * Oz4 + 1922400 * L2 * Oz3 + 58140 * L2 * Oz2 + 1290 * L2 * Oz + 11 * L2)) / (189000 * E2 * Iy2 * niz4))*Fxb3;
elm_geo(6,12) = (-((L5 * (7838208000 * Oy7 + 3527193600 * Oy6 + 623635200 * Oy5 + 55080000 * Oy4 + 2620080 * Oy3 + 73620 * Oy2 + 1422 * Oy + 11)) / (126000 * niy5) + (Iz*L3 * (4354560 * Oy5 - 362880 * Oy4 - 138240 * Oy3 + 1008 * Oy2 + 780 * Oy + 13)) / (12600 * Ax*niy5)) / (E2 * Iz2) + (L3 * (653184000 * L2 * Oy6 + 239500800 * L2 * Oy5 + 32011200 * L2 * Oy4 + 1922400 * L2 * Oy3 + 58140 * L2 * Oy2 + 1290 * L2 * Oy + 11 * L2)) / (189000 * E2 * Iz2 * niy4))*Fxb3;

elm_geo(3,10) = 0;
elm_geo(4,11) = ((Mza + Mzb)*(L6 / (E3 * Iy3) + (5040 * L6 * Oz2) / (E3 * Iy3) + (100800 * L6 * Oz3) / (E3 * Iy3) + (120 * L6 * Oz) / (E3 * Iy3))*Fxb3) / 604800;
elm_geo(5,12) = -((L6 * Mxb*(648 * Oz + 23040 * Oy*Oz2 + 2211840 * Oy*Oz3 + 37324800 * Oy*Oz4 + 174182400 * Oy*Oz5 + 33840 * Oz2 + 967680 * Oz3 + 13236480 * Oz4 + 80870400 * Oz5 + 174182400 * Oz6 + 96 * Oy*Oz + 5)) / (1008000 * E3 * Iy3 * niy*niz4) + (L6 * Mxb*(648 * Oy + 23040 * Oy2 * Oz + 2211840 * Oy3 * Oz + 37324800 * Oy4 * Oz + 174182400 * Oy5 * Oz + 33840 * Oy2 + 967680 * Oy3 + 13236480 * Oy4 + 80870400 * Oy5 + 174182400 * Oy6 + 96 * Oy*Oz + 5)) / (1008000 * E3 * Iz3 * niy4 * niz) + (L6 * Mxb*(12441600 * Oy4 * Oz + 311040 * Oy4 + 12441600 * Oy3 * Oz2 + 5495040 * Oy3 * Oz + 172800 * Oy3 + 1658880 * Oy2 * Oz2 + 613440 * Oy2 * Oz + 20160 * Oy2 + 43200 * Oy*Oz2 + 20880 * Oy*Oz + 660 * Oy + 288 * Oz2 + 228 * Oz + 7)) / (3024000 * E3 * Iy*Iz2 * niy3 * niz2) + (L6 * Mxb*(12441600 * Oy2 * Oz3 + 1658880 * Oy2 * Oz2 + 43200 * Oy2 * Oz + 288 * Oy2 + 12441600 * Oy*Oz4 + 5495040 * Oy*Oz3 + 613440 * Oy*Oz2 + 20880 * Oy*Oz + 228 * Oy + 311040 * Oz4 + 172800 * Oz3 + 20160 * Oz2 + 660 * Oz + 7)) / (3024000 * E3 * Iy2 * Iz*niy2 * niz3))*Fxb3;

elm_geo(2,10) = 0;
elm_geo(3,11) = (-((L4 * (21772800 * Oz5 + 6480000 * Oz4 + 665280 * Oz3 + 25920 * Oz2 + 252 * Oz + 1)) / (42000 * niz5) + (Iy*L2 * (483840 * Oz4 + 97920 * Oz3 + 5376 * Oz2 + 60 * Oz + 1)) / (1400 * Ax*niz5)) / (E2 * Iy2) + (L2 * (1814400 * L2 * Oz4 + 388800 * L2 * Oz3 + 23040 * L2 * Oz2 + 240 * L2 * Oz + L2)) / (63000 * E2 * Iy2 * niz4))*Fxb3;
elm_geo(4,12) = -((Mya + Myb)*(100800 * L6 * Oy3 + 5040 * L6 * Oy2 + 120 * L6 * Oy + L6)*Fxb3) / (604800 * E3 * Iz3);

elm_geo(2,11) = ((Mxb*(1680 * L6 * Oz2 + 80 * L6 * Oz + L6)) / (100800 * E3 * Iy3 * L*(12 * Oy + 1)) + (L*Mxb*(124540416000 * E3 * Iy3 * L4 * Oy2 + 11955879936000 * E3 * Iy3 * L4 * Oy3 + 201755473920000 * E3 * Iy3 * L4 * Oy4 + 941525544960000 * E3 * Iy3 * L4 * Oy5 + 108108000 * E3 * Iy*Iz2 * L4 + 43243200 * E3 * Iy2 * Iz*L4 + 518918400 * E3 * Iy3 * L4 * Oy + 108972864000 * E3 * Iy*Iz2 * L4 * Oy2 + 326918592000 * E3 * Iy2 * Iz*L4 * Oy2 + 560431872000 * E3 * Iy*Iz2 * L4 * Oy3 + 4857076224000 * E3 * Iy2 * Iz*L4 * Oy3 + 22417274880000 * E3 * Iy2 * Iz*L4 * Oy4 + 6486480000 * E3 * Iy*Iz2 * L4 * Oy + 7005398400 * E3 * Iy2 * Iz*L4 * Oy + 3891888000 * E3 * Iy*Iz2 * L4 * Oz + 249080832000 * E3 * Iy*Iz2 * L4 * Oy*Oz + 4296644352000 * E3 * Iy*Iz2 * L4 * Oy2 * Oz + 22417274880000 * E3 * Iy*Iz2 * L4 * Oy3 * Oz)) / (32691859200000 * E6 * Iy3 * Iz3 * niy4))*Fxb3;
elm_geo(3,12) = ((Mxb*(1680 * L6 * Oy2 + 80 * L6 * Oy + L6)) / (100800 * E3 * Iz3 * L*(12 * Oz + 1)) + (L*Mxb*(124540416000 * E3 * Iz3 * L4 * Oz2 + 11955879936000 * E3 * Iz3 * L4 * Oz3 + 201755473920000 * E3 * Iz3 * L4 * Oz4 + 941525544960000 * E3 * Iz3 * L4 * Oz5 + 108108000 * E3 * Iz*Iy2 * L4 + 43243200 * E3 * Iz2 * Iy*L4 + 518918400 * E3 * Iz3 * L4 * Oz + 108972864000 * E3 * Iz*Iy2 * L4 * Oz2 + 326918592000 * E3 * Iz2 * Iy*L4 * Oz2 + 560431872000 * E3 * Iz*Iy2 * L4 * Oz3 + 4857076224000 * E3 * Iz2 * Iy*L4 * Oz3 + 22417274880000 * E3 * Iz2 * Iy*L4 * Oz4 + 6486480000 * E3 * Iz*Iy2 * L4 * Oz + 7005398400 * E3 * Iz2 * Iy*L4 * Oz + 3891888000 * E3 * Iz*Iy2 * L4 * Oz + 249080832000 * E3 * Iz*Iy2 * L4 * Oz*Oy + 4296644352000 * E3 * Iz*Iy2 * L4 * Oz2 * Oy + 22417274880000 * E3 * Iz*Iy2 * L4 * Oz3 * Oy)) / (32691859200000 * E6 * Iz3 * Iy3 * niz4))*Fxb3;

elm_geo(1,11) = 0;
elm_geo(2,12) = (((L4 * (21772800 * Oy5 + 6480000 * Oy4 + 665280 * Oy3 + 25920 * Oy2 + 252 * Oy + 1)) / (42000 * niy5) + (Iz*L2 * (483840 * Oy4 + 97920 * Oy3 + 5376 * Oy2 + 60 * Oy + 1)) / (1400 * Ax*niy5)) / (E2 * Iz2) - (L2 * (1814400 * L2 * Oy4 + 388800 * L2 * Oy3 + 23040 * L2 * Oy2 + 240 * L2 * Oy + L2)) / (63000 * E2 * Iz2 * niy4))*Fxb3;

elm_geo(1,12) = 0;

elm_geo(5,6) = ((Iy*L6 * Mxb*Fxb3 * (49766400 * Oy4 * Oz2 + 5391360 * Oy4 * Oz + 103680 * Oy4 - 49766400 * Oy3 * Oz3 + 1244160 * Oy3 * Oz2 + 449280 * Oy3 * Oz - 6635520 * Oy2 * Oz3 - 311040 * Oy2 * Oz2 + 8640 * Oy2 * Oz - 960 * Oy2 - 172800 * Oy*Oz3 + 11520 * Oy*Oz2 + 2400 * Oy*Oz + 20 * Oy - 1152 * Oz3 + 432 * Oz2 + 56 * Oz + 1)) / 1008000 - (Iz*L6 * Mxb*Fxb3 * (-49766400 * Oy3 * Oz3 - 6635520 * Oy3 * Oz2 - 172800 * Oy3 * Oz - 1152 * Oy3 + 49766400 * Oy2 * Oz4 + 1244160 * Oy2 * Oz3 - 311040 * Oy2 * Oz2 + 11520 * Oy2 * Oz + 432 * Oy2 + 5391360 * Oy*Oz4 + 449280 * Oy*Oz3 + 8640 * Oy*Oz2 + 2400 * Oy*Oz + 56 * Oy + 103680 * Oz4 - 960 * Oz2 + 20 * Oz + 1)) / 1008000) / (E3 * Iy2 * Iz2 * niy3 * niz3) + (L6 * Mxb*Fxb3 * (632 * Oy - 23040 * Oy2 * Oz - 2211840 * Oy3 * Oz - 37324800 * Oy4 * Oz - 174182400 * Oy5 * Oz + 30000 * Oy2 + 599040 * Oy3 + 7015680 * Oy4 + 51840000 * Oy5 + 174182400 * Oy6 - 96 * Oy*Oz + 5)) / (1008000 * E3 * Iz3 * niy4 * niz) - (L6 * Mxb*Fxb3 * (-25082265600 * Oy3 * Oz5 - 5374771200 * Oy3 * Oz4 - 318504960 * Oy3 * Oz3 - 3317760 * Oy3 * Oz2 - 13824 * Oy3 * Oz + 25082265600 * Oy2 * Oz6 + 3284582400 * Oy2 * Oz5 + 114462720 * Oy2 * Oz4 + 33177600 * Oy2 * Oz3 + 3767040 * Oy2 * Oz2 + 88704 * Oy2 * Oz + 720 * Oy2 + 4180377600 * Oy*Oz6 + 1069977600 * Oy*Oz5 + 131051520 * Oy*Oz4 + 12165120 * Oy*Oz3 + 696960 * Oy*Oz2 + 15072 * Oy*Oz + 120 * Oy + 174182400 * Oz6 + 51840000 * Oz5 + 7015680 * Oz4 + 599040 * Oz3 + 30000 * Oz2 + 632 * Oz + 5)) / (1008000 * E3 * Iy3 * niy3 * niz4);
elm_geo(11,12) = (L6 * Mxb*Fxb3 * (-25082265600 * Oy3 * Oz5 - 5374771200 * Oy3 * Oz4 - 318504960 * Oy3 * Oz3 - 3317760 * Oy3 * Oz2 - 13824 * Oy3 * Oz + 25082265600 * Oy2 * Oz6 + 3284582400 * Oy2 * Oz5 + 114462720 * Oy2 * Oz4 + 33177600 * Oy2 * Oz3 + 3767040 * Oy2 * Oz2 + 88704 * Oy2 * Oz + 720 * Oy2 + 4180377600 * Oy*Oz6 + 1069977600 * Oy*Oz5 + 131051520 * Oy*Oz4 + 12165120 * Oy*Oz3 + 696960 * Oy*Oz2 + 15072 * Oy*Oz + 120 * Oy + 174182400 * Oz6 + 51840000 * Oz5 + 7015680 * Oz4 + 599040 * Oz3 + 30000 * Oz2 + 632 * Oz + 5)) / (1008000 * E3 * Iy3 * niy3 * niz4) - (L6 * Mxb*Fxb3 * (632 * Oy - 23040 * Oy2 * Oz - 2211840 * Oy3 * Oz - 37324800 * Oy4 * Oz - 174182400 * Oy5 * Oz + 30000 * Oy2 + 599040 * Oy3 + 7015680 * Oy4 + 51840000 * Oy5 + 174182400 * Oy6 - 96 * Oy*Oz + 5)) / (1008000 * E3 * Iz3 * niy4 * niz) - ((Iy*L6 * Mxb*Fxb3 * (49766400 * Oy4 * Oz2 + 5391360 * Oy4 * Oz + 103680 * Oy4 - 49766400 * Oy3 * Oz3 + 1244160 * Oy3 * Oz2 + 449280 * Oy3 * Oz - 6635520 * Oy2 * Oz3 - 311040 * Oy2 * Oz2 + 8640 * Oy2 * Oz - 960 * Oy2 - 172800 * Oy*Oz3 + 11520 * Oy*Oz2 + 2400 * Oy*Oz + 20 * Oy - 1152 * Oz3 + 432 * Oz2 + 56 * Oz + 1)) / 1008000 - (Iz*L6 * Mxb*Fxb3 * (-49766400 * Oy3 * Oz3 - 6635520 * Oy3 * Oz2 - 172800 * Oy3 * Oz - 1152 * Oy3 + 49766400 * Oy2 * Oz4 + 1244160 * Oy2 * Oz3 - 311040 * Oy2 * Oz2 + 11520 * Oy2 * Oz + 432 * Oy2 + 5391360 * Oy*Oz4 + 449280 * Oy*Oz3 + 8640 * Oy*Oz2 + 2400 * Oy*Oz + 56 * Oy + 103680 * Oz4 - 960 * Oz2 + 20 * Oz + 1)) / 1008000) / (E3 * Iy2 * Iz2 * niy3 * niz3);
end

% Filling the rest of the matrix
for i = 1:12
    for j = 1:12
         elm_geo(j,i)= elm_geo(i,j);
    end
end  
for i = 1:6
        elm_geo(i+6,i+6) = elm_geo(i,i);
end 
    
end

% =========================================================== End of File */
