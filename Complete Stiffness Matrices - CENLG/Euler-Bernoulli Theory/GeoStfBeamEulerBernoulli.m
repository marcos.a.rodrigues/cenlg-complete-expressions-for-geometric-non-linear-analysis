% GeoStfBeamEulerBernoulli.m -  this file contains 3-dimensional Euler-Bernoulli geometric
%                            stiffness matrix using Taylor series expansion.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% Authors - Marcos Antonio Campos Rodrigues (Departamento de Engenharia Civil, Universidade Federal do Esp�rito Santo)
%           Rodrigo Bird Burgos (Departamento de Estruturas e Funda��es, Universidade do Estado do Rio de Janeiro)
%           Luiz Fernando Martha (Departamento de Engenharia Civil e Ambiental, Pontif�cia Universidade Cat�lica do Rio de Janeiro)
%
%            Reference:
%                M. A. C. Rodrigues. "Integrated Solutions for the Formulations of The Geometric Nonlinearity Problem."
%                DSc. Thesis - Pontif�cia Universidade Cat�lica do Rio de Janeiro,
%                Departamento de Engenharia Civil e Ambiental, 2019.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% function [elm_geo] = GeoStfBeamEulerBernoulli(order,L,A,E,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb)
%
%      L - Current element length;                                                    (  in )
%      E - Young's modulus;                                                           (  in )
%      A - Element cross section area;                                                (  in )
%      Iy - Mom. of inertia wrt "y" axis;                                             (  in )
%      Iz - Mom. of inertia wrt "z" axis;                                             (  in )
%      Fya, Fza, Mxa, Mya, Mza - Forces and moments (element initial node)            (  in )
%      Fxb, Fyb, Fzb, Mxb, Myb, Mzb - Forces and moments (element final node)         (  in )
%
%      order - Number of terms considered in stiffness matrix                         (  in )
%           Example how to use different orders:
%
%   		 case 2 -> 2 Terms for tangent stiffness matrix (1 elastic + 1 term for axial load P);
%   		    [elm_geo2] = GeoStfBeamEulerBernoulli(2,L,A,E,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb);	
%
%			 case 3 -> 3 Terms for tangent stiffness matrix (1 elastic + 1 term for axial load P + 1 term for axial load P^2);
%   		    [elm_geo3] = GeoStfBeamEulerBernoulli(3,L,A,E,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb);
%   		    
%   		 case 4 -> 4 Terms for tangent stiffness matrix (1 elastic + 1 term for axial load P + 1 term for axial load P^2 + 1 term for axial load P^3);
%   		    [elm_geo4] = GeoStfBeamEulerBernoulli(4,L,A,E,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb);
%
%            elm_geo = elm_geo2      /      elm_geo = elm_geo2 + elm_geo3      /      elm_geo = elm_geo2 + elm_geo3 + elm_geo4
%
%      elm_geo - Geometric stiffness matrix                                           ( out )
%
%      Returns tangent stiffness matrix of the element.
%      This function calculates the local tangent stiffness matrix for an element considering the Euler-Bernoulli beam theory.
%      This matrix is obtained directly from the solution of the equilibrium differential equation of an deformed infinitesimal element (Rodrigues, 2019).
%     
% ----------------------------------------------------------------------------------------------------------------------------------------------

function [elm_geo] = GeoStfBeamEulerBernoulli(order,L,A,E,Iy,Iz,Fya,Fza,Mxa,Mya,Mza,Fxb,Fyb,Fzb,Mxb,Myb,Mzb)

Fxb2 = Fxb*Fxb;
Fxb3 = Fxb2*Fxb;
L2 = L*L;
L3 = L2*L;
L4 = L3*L;
L5 = L4*L;
L6 = L5*L;
E2 = E*E;
E3 = E2*E;
Iy2 = Iy*Iy;
Iy3 = Iy2*Iy;
Iz2 = Iz*Iz;
Iz3 = Iz2*Iz;
K = Fxb*(Iy + Iz) / A;

elm_geo = zeros(12,12);

switch (order)
% ========== 2 terms ========== %
case 2
elm_geo(1,1) = Fxb / L;
elm_geo(2,2) = 6.0*Fxb / (5.0*L) + 12.0*Fxb*Iz / (A*L3);  
elm_geo(3,3) = 6.0*Fxb / (5.0*L) + 12.0*Fxb*Iy / (A*L3);
elm_geo(4,4) = K / L;
elm_geo(5,5) = 2.0*Fxb*L / 15.0 + 4.0*Fxb*Iy / (A*L);  
elm_geo(6,6) = 2.0*Fxb*L / 15.0 + 4.0*Fxb*Iz / (A*L);

elm_geo(3,4) = Mza / L;
elm_geo(4,5) = -Mza / 3.0 + Mzb / 6.0;
elm_geo(6,7) = Mza / L;
elm_geo(9,10) = -Mzb / L;
elm_geo(10,11) = Mza / 6.0 - Mzb / 3.0;

elm_geo(2,4) = Mya / L;
elm_geo(3,5) = -Fxb / 10.0 - 6.0*Fxb*Iy / (A*L2);
elm_geo(4,6) = Mya / 3.0 - Myb / 6.0;
elm_geo(5,7) = Mya / L;
elm_geo(6,8) = -Fxb / 10.0 - 6.0*Fxb*Iz / (A*L2);
elm_geo(8,10) = -Myb / L;
elm_geo(9,11) = Fxb / 10.0 + 6.0*Fxb*Iy / (A*L2);
elm_geo(10,12) = -Mya / 6.0 + Myb / 3.0;

elm_geo(2,5) = Mxb / L;
elm_geo(3,6) = Mxb / L;
elm_geo(5,8) = -Mxb / L;
elm_geo(6,9) = -Mxb / L;
elm_geo(8,11) = Mxb / L;
elm_geo(9,12) = Mxb / L;

elm_geo(1,5) = -Mya / L;
elm_geo(2,6) = Fxb / 10.0 + 6.0*Fxb*Iz / (A*L2);
elm_geo(4,8) = -Mya / L;
elm_geo(5,9) = Fxb / 10.0 + 6.0*Fxb*Iy / (A*L2);
elm_geo(6,10) = Mya / 6.0 + Myb / 6.0;
elm_geo(7,11) = Myb / L;
elm_geo(8,12) = -Fxb / 10.0 - 6.0*Fxb*Iz / (A*L2);

elm_geo(1,6) = -Mza / L;
elm_geo(4,9) = -Mza / L;
elm_geo(5,10) = -Mza / 6.0 - Mzb / 6.0;
elm_geo(6,11) = -Mxb / 2.0;
elm_geo(7,12) = Mzb / L;

elm_geo(1,7) = -Fxb / L;
elm_geo(2,8) = -6.0*Fxb / (5.0*L) - 12.0*Fxb*Iz / (A*L3);
elm_geo(3,9) = -6.0*Fxb / (5.0*L) - 12.0*Fxb*Iy / (A*L3);
elm_geo(4,10) = -K / L;
elm_geo(5,11) = -Fxb*L / 30.0 + 2.0*Fxb*Iy / (A*L);
elm_geo(6,12) = -Fxb*L / 30.0 + 2.0*Fxb*Iz / (A*L);

elm_geo(3,10) = Mzb / L;
elm_geo(4,11) = -Mza / 6.0 - Mzb / 6.0;
elm_geo(5,12) = Mxb / 2.0;

elm_geo(2,10) = Myb / L;
elm_geo(3,11) = -Fxb / 10.0 - 6.0*Fxb*Iy / (A*L2);
elm_geo(4,12) = Mya / 6.0 + Myb / 6.0;

elm_geo(2,11) = -Mxb / L;
elm_geo(3,12) = -Mxb / L;

elm_geo(1,11) = -Myb / L;
elm_geo(2,12) = Fxb / 10.0 + 6.0*Fxb*Iz / (A*L2);

elm_geo(1,12) = -Mzb / L;

% ========== 3 terms ========== %
case 3
elm_geo(1,1) = 0;
elm_geo(2,2) = (-L / (700 * E*Iz))*Fxb2; 
elm_geo(3,3) = (-L / (700 * E*Iy))*Fxb2;
elm_geo(4,4) = 0;
elm_geo(5,5) = -(11 * L3 * Fxb2) / (6300 * E*Iy);  
elm_geo(6,6) = -(11 * L3 * Fxb2) / (6300 * E*Iz);

elm_geo(3,4) = 0;
elm_geo(4,5) = (L4 * (Mza + Mzb)*Fxb2) / (15120 * E2 * Iy2) - (L2 * (Mza + Mzb)*Fxb) / (360 * E*Iy);
elm_geo(6,7) = 0;
elm_geo(9,10) = 0;
elm_geo(10,11) = (L4 * (Mza + Mzb)*Fxb2) / (15120 * E2 * Iy2) - (L2 * (Mza + Mzb)*Fxb) / (360 * E*Iy);

elm_geo(2,4) = 0;
elm_geo(3,5) = (L2 / (1400 * E*Iy))*Fxb2;
elm_geo(4,6) = (L4 * (Mya + Myb)*Fxb2) / (15120 * E2 * Iz2) + (L2 * (Mya + Myb)*Fxb) / (360 * E*Iz);
elm_geo(5,7) = 0;
elm_geo(6,8) = (L2 / (1400 * E*Iz))*Fxb2;
elm_geo(8,10) = 0;
elm_geo(9,11) = (-L2 / (1400 * E*Iy))*Fxb2;
elm_geo(10,12) = (L4 * (Mya + Myb)*Fxb2) / (15120 * E2 * Iz2) + (L2 * (Mya + Myb)*Fxb) / (360 * E*Iz);

elm_geo(2,5) = ((L3 * Mxb) / (2520 * E2 * Iy2) + (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 - (L*Mxb*Fxb) / (60 * E*Iy);
elm_geo(3,6) = ((L3 * Mxb) / (2520 * E2 * Iz2) + (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 - (L*Mxb*Fxb) / (60 * E*Iz);
elm_geo(5,8) = (-(L3 * Mxb) / (2520 * E2 * Iy2) - (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (L*Mxb*Fxb) / (60 * E*Iy);
elm_geo(6,9) = (-(L3 * Mxb) / (2520 * E2 * Iz2) - (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (L*Mxb*Fxb) / (60 * E*Iz);
elm_geo(8,11) = ((L3 * Mxb) / (2520 * E2 * Iy2) + (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 - (L*Mxb*Fxb) / (60 * E*Iy);
elm_geo(9,12) = ((L3 * Mxb) / (2520 * E2 * Iz2) + (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 - (L*Mxb*Fxb) / (60 * E*Iz);

elm_geo(1,5) = 0;
elm_geo(2,6) = (-L2 / (1400 * E*Iz))*Fxb2;
elm_geo(4,8) = 0;
elm_geo(5,9) = (-L2 / (1400 * E*Iy))*Fxb2;
elm_geo(6,10) = (L4 * (Mya + Myb)*Fxb2) / (15120 * E2 * Iz2) - (L2 * (Mya + Myb)*Fxb) / (360 * E*Iz);
elm_geo(7,11) = 0;
elm_geo(8,12) = (L2 / (1400 * E*Iz))*Fxb2;

elm_geo(1,6) = 0;
elm_geo(4,9) = 0;
elm_geo(5,10) = -(L4 * (Mza + Mzb)*Fxb2) / (15120 * E2 * Iy2) + (L2 * (Mza + Mzb)*Fxb) / (360 * E*Iy);
elm_geo(6,11) = (-(L4 * Mxb) / (5040 * E2 * Iy2) - (L4 * Mxb) / (5040 * E2 * Iz2) - (L4 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + ((L2 * Mxb) / (120 * E*Iy) + (L2 * Mxb) / (120 * E*Iz))*Fxb;
elm_geo(7,12) = 0;  

elm_geo(1,7) = 0;
elm_geo(2,8) = (L / (700 * E*Iz))*Fxb2;
elm_geo(3,9) = (L / (700 * E*Iy))*Fxb2;
elm_geo(4,10) = 0;
elm_geo(5,11) = (13 * L3 * Fxb2) / (12600 * E*Iy);
elm_geo(6,12) = (13 * L3 * Fxb2) / (12600 * E*Iz);

elm_geo(3,10) = 0;
elm_geo(4,11) = -(L4 * (Mza + Mzb)*Fxb2) / (15120 * E2 * Iy2) + (L2 * (Mza + Mzb)*Fxb) / (360 * E*Iy);
elm_geo(5,12) = ((L4 * Mxb) / (5040 * E2 * Iy2) + (L4 * Mxb) / (5040 * E2 * Iz2) + (L4 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (-(L2 * Mxb) / (120 * E*Iy) - (L2 * Mxb) / (120 * E*Iz))*Fxb;

elm_geo(2,10) = 0;
elm_geo(3,11) = (L2 / (1400 * E*Iy))*Fxb2;
elm_geo(4,12) = (L4 * (Mya + Myb)*Fxb2) / (15120 * E2 * Iz2) - (L2 * (Mya + Myb)*Fxb) / (360 * E*Iz);

elm_geo(2,11) = (-(L3 * Mxb) / (2520 * E2 * Iy2) - (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (L*Mxb*Fxb) / (60 * E*Iy);
elm_geo(3,12) = (-(L3 * Mxb) / (2520 * E2 * Iz2) - (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (L*Mxb*Fxb) / (60 * E*Iz);

elm_geo(1,11) = 0;
elm_geo(2,12) = (-L2 / (1400 * E*Iz))*Fxb2;

elm_geo(1,12) = 0;

elm_geo(5,6) = ((L4 * Mxb) / (5040 * E2 * Iy2) - (L4 * Mxb) / (5040 * E2 * Iz2))*Fxb2 + (-(L2 * Mxb) / (120 * E*Iy) + (L2 * Mxb) / (120 * E*Iz))*Fxb;
elm_geo(11,12) = (-(L4 * Mxb) / (5040 * E2 * Iy2) + (L4 * Mxb) / (5040 * E2 * Iz2))*Fxb2 + ((L2 * Mxb) / (120 * E*Iy) - (L2 * Mxb) / (120 * E*Iz))*Fxb;

% ========== 4 terms ========== %
case 4
elm_geo(1,1) = 0;
elm_geo(2,2) = ((L3 / 21000 + (Iz*L) / (700 * A)) / (E2 * Iz2) - L3 / (31500 * E2 * Iz2))*Fxb3;
elm_geo(3,3) = ((L3 / 21000 + (Iy*L) / (700 * A)) / (E2 * Iy2) - L3 / (31500 * E2 * Iy2))*Fxb3;
elm_geo(4,4) = 0;
elm_geo(5,5) = ((L5 / 9000 + (11 * Iy*L3) / (6300 * A)) / (E2 * Iy2) - L5 / (13500 * E2 * Iy2))*Fxb3;
elm_geo(6,6) = ((L5 / 9000 + (11 * Iz*L3) / (6300 * A)) / (E2 * Iz2) - L5 / (13500 * E2 * Iz2))*Fxb3;

elm_geo(3,4) = 0;
elm_geo(4,5) = -(L6 * (Mza + Mzb)*Fxb3) / (604800 * E3 * Iy3);
elm_geo(6,7) = 0;
elm_geo(9,10) = 0;
elm_geo(10,11) = -(L6 * (Mza + Mzb)*Fxb3) / (604800 * E3 * Iy3);

elm_geo(2,4) = 0;
elm_geo(3,5) = (L4 / (63000 * E2 * Iy2) - (L4 / 42000 + (Iy*L2) / (1400 * A)) / (E2 * Iy2))*Fxb3;
elm_geo(4,6) = (L6 * (Mya + Myb)*Fxb3) / (604800 * E3 * Iz3);
elm_geo(5,7) = 0;
elm_geo(6,8) = (L4 / (63000 * E2 * Iz2) - (L4 / 42000 + (Iz*L2) / (1400 * A)) / (E2 * Iz2))*Fxb3;
elm_geo(8,10) = 0;
elm_geo(9,11) = ((L4 / 42000 + (Iy*L2) / (1400 * A)) / (E2 * Iy2) - L4 / (63000 * E2 * Iy2))*Fxb3;
elm_geo(10,12) = (L6 * (Mya + Myb)*Fxb3) / (604800 * E3 * Iz3);

elm_geo(2,5) = (-(L5 * Mxb) / (100800 * E3 * Iy3) - (L5 * Mxb) / (756000 * E3 * Iy*Iz2) - (L5 * Mxb) / (302400 * E3 * Iy2 * Iz))*Fxb3;
elm_geo(3,6) = (-(L5 * Mxb) / (100800 * E3 * Iz3) - (L5 * Mxb) / (302400 * E3 * Iy*Iz2) - (L5 * Mxb) / (756000 * E3 * Iy2 * Iz))*Fxb3;
elm_geo(5,8) = ((L5 * Mxb) / (100800 * E3 * Iy3) + (L5 * Mxb) / (756000 * E3 * Iy*Iz2) + (L5 * Mxb) / (302400 * E3 * Iy2 * Iz))*Fxb3;
elm_geo(6,9) = ((L5 * Mxb) / (100800 * E3 * Iz3) + (L5 * Mxb) / (302400 * E3 * Iy*Iz2) + (L5 * Mxb) / (756000 * E3 * Iy2 * Iz))*Fxb3;
elm_geo(8,11) = (-(L5 * Mxb) / (100800 * E3 * Iy3) - (L5 * Mxb) / (756000 * E3 * Iy*Iz2) - (L5 * Mxb) / (302400 * E3 * Iy2 * Iz))*Fxb3;
elm_geo(9,12) = (-(L5 * Mxb) / (100800 * E3 * Iz3) - (L5 * Mxb) / (302400 * E3 * Iy*Iz2) - (L5 * Mxb) / (756000 * E3 * Iy2 * Iz))*Fxb3;

elm_geo(1,5) = 0;
elm_geo(2,6) = ((L4 / 42000 + (Iz*L2) / (1400 * A)) / (E2 * Iz2) - L4 / (63000 * E2 * Iz2))*Fxb3;
elm_geo(4,8) = 0;
elm_geo(5,9) = ((L4 / 42000 + (Iy*L2) / (1400 * A)) / (E2 * Iy2) - L4 / (63000 * E2 * Iy2))*Fxb3;
elm_geo(6,10) = -(L6 * (Mya + Myb)*Fxb3) / (604800 * E3 * Iz3);
elm_geo(7,11) = 0;
elm_geo(8,12) = (L4 / (63000 * E2 * Iz2) - (L4 / 42000 + (Iz*L2) / (1400 * A)) / (E2 * Iz2))*Fxb3;

elm_geo(1,6) = 0;
elm_geo(4,9) = 0;
elm_geo(5,10) = (L6 * (Mza + Mzb)*Fxb3) / (604800 * E3 * Iy3);
elm_geo(6,11) = ((L6 * Mxb) / (201600 * E3 * Iy3) + (L6 * Mxb) / (201600 * E3 * Iz3) + (L6 * Mxb) / (432000 * E3 * Iy*Iz2) + (L6 * Mxb) / (432000 * E3 * Iy2 * Iz))*Fxb3;
elm_geo(7,12) = 0;

elm_geo(1,7) = 0;
elm_geo(2,8) = (L3 / (31500 * E2 * Iz2) - (L3 / 21000 + (Iz*L) / (700 * A)) / (E2 * Iz2))*Fxb3;
elm_geo(3,9) = (L3 / (31500 * E2 * Iy2) - (L3 / 21000 + (Iy*L) / (700 * A)) / (E2 * Iy2))*Fxb3;
elm_geo(4,10) = 0;
elm_geo(5,11) = (-((11 * L5) / 126000 + (13 * Iy*L3) / (12600 * A)) / (E2 * Iy2) + (11 * L5) / (189000 * E2 * Iy2))*Fxb3;
elm_geo(6,12) = (-((11 * L5) / 126000 + (13 * Iz*L3) / (12600 * A)) / (E2 * Iz2) + (11 * L5) / (189000 * E2 * Iz2))*Fxb3;

elm_geo(3,10) = 0;
elm_geo(4,11) = (L6 * (Mza + Mzb)*Fxb3) / (604800 * E3 * Iy3);
elm_geo(5,12) = (-(L6 * Mxb) / (201600 * E3 * Iy3) - (L6 * Mxb) / (201600 * E3 * Iz3) - (L6 * Mxb) / (432000 * E3 * Iy*Iz2) - (L6 * Mxb) / (432000 * E3 * Iy2 * Iz))*Fxb3;

elm_geo(2,10) = 0;
elm_geo(3,11) = (L4 / (63000 * E2 * Iy2) - (L4 / 42000 + (Iy*L2) / (1400 * A)) / (E2 * Iy2))*Fxb3;
elm_geo(4,12) = -(L6 * (Mya + Myb)*Fxb3) / (604800 * E3 * Iz3);

elm_geo(2,11) = ((L5 * Mxb) / (100800 * E3 * Iy3) + (L5 * Mxb) / (756000 * E3 * Iy*Iz2) + (L5 * Mxb) / (302400 * E3 * Iy2 * Iz))*Fxb3;
elm_geo(3,12) = ((L5 * Mxb) / (100800 * E3 * Iz3) + (L5 * Mxb) / (302400 * E3 * Iy*Iz2) + (L5 * Mxb) / (756000 * E3 * Iy2 * Iz))*Fxb3;

elm_geo(1,11) = 0;
elm_geo(2,12) = ((L4 / 42000 + (Iz*L2) / (1400 * A)) / (E2 * Iz2) - L4 / (63000 * E2 * Iz2))*Fxb3;

elm_geo(1,12) = 0;

elm_geo(5,6) = (-(L6 * Mxb) / (201600 * E3 * Iy3) + (L6 * Mxb) / (201600 * E3 * Iz3) + (L6 * Mxb) / (1008000 * E3 * Iy*Iz2) - (L6 * Mxb) / (1008000 * E3 * Iy2 * Iz))*Fxb3;
elm_geo(11,12) = ((L6 * Mxb) / (201600 * E3 * Iy3) - (L6 * Mxb) / (201600 * E3 * Iz3) - (L6 * Mxb) / (1008000 * E3 * Iy*Iz2) + (L6 * Mxb) / (1008000 * E3 * Iy2 * Iz))*Fxb3;
end

% Filling the rest of the matrix
for i = 1:12
    for j = 1:12
         elm_geo(j,i)= elm_geo(i,j);
    end
end  
for i = 1:6
        elm_geo(i+6,i+6) = elm_geo(i,i);
end
    
end

% =========================================================== End of File */
