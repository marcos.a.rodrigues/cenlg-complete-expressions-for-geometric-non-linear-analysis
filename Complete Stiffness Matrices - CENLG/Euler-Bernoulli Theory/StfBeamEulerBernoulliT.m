% StfBeamEulerBernoulliT.m -  this file contains 3-dimensional Euler-Bernoulli complete 
%                             tangent stiffness matrix for positive axial force.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% Authors - Marcos Antonio Campos Rodrigues (Departamento de Engenharia Civil, Universidade Federal do Esp�rito Santo)
%           Rodrigo Bird Burgos (Departamento de Estruturas e Funda��es, Universidade do Estado do Rio de Janeiro)
%           Luiz Fernando Martha (Departamento de Engenharia Civil e Ambiental, Pontif�cia Universidade Cat�lica do Rio de Janeiro)
%
%            Reference:
%                M. A. C. Rodrigues. "Integrated Solutions for the Formulations of The Geometric Nonlinearity Problem."
%                DSc. Thesis - Pontif�cia Universidade Cat�lica do Rio de Janeiro,
%                Departamento de Engenharia Civil e Ambiental, 2019.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% function [elm_tangstf] = StfBeamEulerBernoulliT(L,E,Iz,Iy,P,A,Mza,Mzb,Mya,Myb,Mxb)
%
%      L - Current element length;                                                    (  in )
%      E - Young's modulus;                                                           (  in )
%      A - Element cross section area;                                                (  in )
%      Iy - Mom. of inertia wrt "y" axis;                                             (  in )
%      Iz - Mom. of inertia wrt "z" axis;                                             (  in )
%      Fya, Fza, Mxa, Mya, Mza - Forces and moments (element initial node)            (  in )
%      P, Fyb, Fzb, Mxb, Myb, Mzb - Forces and moments (element final node)           (  in )
%      elm_tangstf - Tangent stiffness matrix                                         ( out )
%
%      Returns tangent stiffness matrix of the element.
%      This function calculates the local tangent stiffness matrix for an element considering the Euler-Bernoulli beam theory.
%      This matrix is obtained directly from the solution of the equilibrium differential equation of an deformed infinitesimal element (Rodrigues, 2019).
%     
% ----------------------------------------------------------------------------------------------------------------------------------------------

function [elm_tangstf] = StfBeamEulerBernoulliT(L,E,Iz,Iy,P,A,Mza,Mzb,Mya,Myb,Mxb)

mu_y = sqrt(P / (E*Iz));
mu_z = sqrt(P / (E*Iy));
Jp = P *(Iy + Iz) / A;

elm_tangstf = zeros(12,12);

elm_tangstf(1,1) = (P + A*E)/L;
elm_tangstf(2,2) = (L*(mu_y*(A*P*mu_y + A*P*mu_y*(cosh(2*L*mu_y) + sinh(2*L*mu_y))) - mu_y*(cosh(L*mu_y) + sinh(L*mu_y))*(2*Iz*P*mu_y^3 - 4*A*P*mu_y + 2*A*E*Iz*mu_y^3)) + mu_y*(3*A*P - 3*A*P*(cosh(2*L*mu_y) + sinh(2*L*mu_y)) - Iz*P*mu_y^2 + Iz*P*mu_y^2*(cosh(2*L*mu_y) + sinh(2*L*mu_y)) - A*E*Iz*mu_y^2 + A*E*Iz*mu_y^2*(cosh(2*L*mu_y) + sinh(2*L*mu_y))))/(A*(L*mu_y - 2*sinh(L*mu_y) - 2*cosh(L*mu_y) + L*mu_y*(cosh(L*mu_y) + sinh(L*mu_y)) + 2)^2);   
elm_tangstf(3,3) = (L*(mu_z*(A*P*mu_z + A*P*mu_z*(cosh(2*L*mu_z) + sinh(2*L*mu_z))) - mu_z*(cosh(L*mu_z) + sinh(L*mu_z))*(2*Iy*P*mu_z^3 - 4*A*P*mu_z + 2*A*E*Iy*mu_z^3)) + mu_z*(3*A*P - 3*A*P*(cosh(2*L*mu_z) + sinh(2*L*mu_z)) - Iy*P*mu_z^2 + Iy*P*mu_z^2*(cosh(2*L*mu_z) + sinh(2*L*mu_z)) - A*E*Iy*mu_z^2 + A*E*Iy*mu_z^2*(cosh(2*L*mu_z) + sinh(2*L*mu_z))))/(A*(L*mu_z - 2*sinh(L*mu_z) - 2*cosh(L*mu_z) + L*mu_z*(cosh(L*mu_z) + sinh(L*mu_z)) + 2)^2);
elm_tangstf(4,4) = (Jp*P)/(A*L);
elm_tangstf(5,5) = ((cosh((L*mu_z)/2)^2 - 1)*(sinh(L*mu_z)*(Iy*P*L^2*mu_z^4 + 2*Iy*P*mu_z^2) + 2*Iy*L*P*mu_z^3) + (Iy*L^3*P*mu_z^5)/2 - Iy*L*P*mu_z^3*(cosh(L*mu_z)^2 - 1) - (Iy*L^2*P*mu_z^4*sinh(L*mu_z))/2)/(A*mu_z*(- 4*cosh((L*mu_z)/2)^2 + L*mu_z*sinh(L*mu_z) + 4)^2) - (sinh(L*mu_z)*((E*Iy*L^2*mu_z^4)/2 - P*L^2*mu_z^2 + P) - (P*sinh(2*L*mu_z))/2 - (cosh((L*mu_z)/2)^2 - 1)*(sinh(L*mu_z)*(E*Iy*L^2*mu_z^4 + 2*E*Iy*mu_z^2) + 2*L*P*mu_z + 2*E*Iy*L*mu_z^3) + (L^3*P*mu_z^3)/2 - (E*Iy*L^3*mu_z^5)/2 + 2*L*P*mu_z*(cosh(L*mu_z)^2 - 1) - (L^2*P*mu_z^2*sinh(2*L*mu_z))/4 + E*Iy*L*mu_z^3*(cosh(L*mu_z)^2 - 1))/(mu_z*(- 4*cosh((L*mu_z)/2)^2 + L*mu_z*sinh(L*mu_z) + 4)^2);  
elm_tangstf(6,6) = ((cosh((L*mu_y)/2)^2 - 1)*(sinh(L*mu_y)*(Iz*P*L^2*mu_y^4 + 2*Iz*P*mu_y^2) + 2*Iz*L*P*mu_y^3) + (Iz*L^3*P*mu_y^5)/2 - Iz*L*P*mu_y^3*(cosh(L*mu_y)^2 - 1) - (Iz*L^2*P*mu_y^4*sinh(L*mu_y))/2)/(A*mu_y*(- 4*cosh((L*mu_y)/2)^2 + L*mu_y*sinh(L*mu_y) + 4)^2) - (sinh(L*mu_y)*((E*Iz*L^2*mu_y^4)/2 - P*L^2*mu_y^2 + P) - (P*sinh(2*L*mu_y))/2 - (cosh((L*mu_y)/2)^2 - 1)*(sinh(L*mu_y)*(E*Iz*L^2*mu_y^4 + 2*E*Iz*mu_y^2) + 2*L*P*mu_y + 2*E*Iz*L*mu_y^3) + (L^3*P*mu_y^3)/2 - (E*Iz*L^3*mu_y^5)/2 + 2*L*P*mu_y*(cosh(L*mu_y)^2 - 1) - (L^2*P*mu_y^2*sinh(2*L*mu_y))/4 + E*Iz*L*mu_y^3*(cosh(L*mu_y)^2 - 1))/(mu_y*(- 4*cosh((L*mu_y)/2)^2 + L*mu_y*sinh(L*mu_y) + 4)^2);

elm_tangstf(3,4) = Mza/L;
elm_tangstf(4,5) = (cosh((L*mu_z)/2)*(Mza + Mzb))/(L*mu_z*sinh((L*mu_z)/2)) - (2*(Mza + Mzb))/(L^2*mu_z^2) - Mza/2;    
elm_tangstf(6,7) = Mza/L;
elm_tangstf(9,10) = -Mzb/L;
elm_tangstf(10,11) = (cosh((L*mu_z)/2)*(Mza + Mzb))/(L*mu_z*sinh((L*mu_z)/2)) - (2*(Mza + Mzb))/(L^2*mu_z^2) - Mzb/2;

elm_tangstf(2,4) = Mya/L;
elm_tangstf(3,5) = -(A*L^2*P*mu_z^2 - 8*A*P*(cosh((L*mu_z)/2)^2 - 1) - Iy*L^2*P*mu_z^4 + A*L*P*mu_z*sinh(L*mu_z) + Iy*L*P*mu_z^3*sinh(L*mu_z) - A*E*Iy*L^2*mu_z^4 + A*E*Iy*L*mu_z^3*sinh(L*mu_z))/(4*A*(- 2*L*mu_z*cosh((L*mu_z)/4)^2 + 2*sinh((L*mu_z)/2) + L*mu_z)^2);
elm_tangstf(4,6) = Mya/2 + (2*(Mya + Myb))/(L^2*mu_y^2) - (cosh((L*mu_y)/2)*(Mya + Myb))/(L*mu_y*sinh((L*mu_y)/2));
elm_tangstf(5,7) = Mya/L;
elm_tangstf(6,8) = -(A*L^2*P*mu_y^2 - 8*A*P*(cosh((L*mu_y)/2)^2 - 1) - Iz*L^2*P*mu_y^4 + A*L*P*mu_y*sinh(L*mu_y) + Iz*L*P*mu_y^3*sinh(L*mu_y) - A*E*Iz*L^2*mu_y^4 + A*E*Iz*L*mu_y^3*sinh(L*mu_y))/(4*A*(- 2*L*mu_y*cosh((L*mu_y)/4)^2 + 2*sinh((L*mu_y)/2) + L*mu_y)^2);
elm_tangstf(8,10) = -Myb/L;
elm_tangstf(9,11) = (A*L^2*P*mu_z^2 - 8*A*P*(cosh((L*mu_z)/2)^2 - 1) - Iy*L^2*P*mu_z^4 + A*L*P*mu_z*sinh(L*mu_z) + Iy*L*P*mu_z^3*sinh(L*mu_z) - A*E*Iy*L^2*mu_z^4 + A*E*Iy*L*mu_z^3*sinh(L*mu_z))/(4*A*(- 2*L*mu_z*cosh((L*mu_z)/4)^2 + 2*sinh((L*mu_z)/2) + L*mu_z)^2);
elm_tangstf(10,12) = Myb/2 + (2*(Mya + Myb))/(L^2*mu_y^2) - (cosh((L*mu_y)/2)*(Mya + Myb))/(L*mu_y*sinh((L*mu_y)/2));

elm_tangstf(2,5) = -(Mxb*mu_y^2*(mu_y*cosh((L*mu_y)/2)*sinh((L*mu_z)/2) - mu_z*cosh((L*mu_z)/2)*sinh((L*mu_y)/2)))/(sinh((L*mu_z)/2)*(mu_y^2 - mu_z^2)*(2*sinh((L*mu_y)/2) - L*mu_y*cosh((L*mu_y)/2)));
elm_tangstf(3,6) = -(Mxb*mu_z^2*(mu_y*cosh((L*mu_y)/2)*sinh((L*mu_z)/2) - mu_z*cosh((L*mu_z)/2)*sinh((L*mu_y)/2)))/(sinh((L*mu_y)/2)*(mu_y^2 - mu_z^2)*(2*sinh((L*mu_z)/2) - L*mu_z*cosh((L*mu_z)/2)));
elm_tangstf(5,8) = (Mxb*mu_y^2*(mu_y*cosh((L*mu_y)/2)*sinh((L*mu_z)/2) - mu_z*cosh((L*mu_z)/2)*sinh((L*mu_y)/2)))/(sinh((L*mu_z)/2)*(mu_y^2 - mu_z^2)*(2*sinh((L*mu_y)/2) - L*mu_y*cosh((L*mu_y)/2)));
elm_tangstf(6,9) = (Mxb*mu_z^2*(mu_y*cosh((L*mu_y)/2)*sinh((L*mu_z)/2) - mu_z*cosh((L*mu_z)/2)*sinh((L*mu_y)/2)))/(sinh((L*mu_y)/2)*(mu_y^2 - mu_z^2)*(2*sinh((L*mu_z)/2) - L*mu_z*cosh((L*mu_z)/2)));
elm_tangstf(8,11) = -(Mxb*mu_y^2*(mu_y*cosh((L*mu_y)/2)*sinh((L*mu_z)/2) - mu_z*cosh((L*mu_z)/2)*sinh((L*mu_y)/2)))/(sinh((L*mu_z)/2)*(mu_y^2 - mu_z^2)*(2*sinh((L*mu_y)/2) - L*mu_y*cosh((L*mu_y)/2)));
elm_tangstf(9,12) = -(Mxb*mu_z^2*(mu_y*cosh((L*mu_y)/2)*sinh((L*mu_z)/2) - mu_z*cosh((L*mu_z)/2)*sinh((L*mu_y)/2)))/(sinh((L*mu_y)/2)*(mu_y^2 - mu_z^2)*(2*sinh((L*mu_z)/2) - L*mu_z*cosh((L*mu_z)/2)));

elm_tangstf(1,5) = -Mya/L;
elm_tangstf(2,6) = (A*L^2*P*mu_y^2 - 8*A*P*(cosh((L*mu_y)/2)^2 - 1) - Iz*L^2*P*mu_y^4 + A*L*P*mu_y*sinh(L*mu_y) + Iz*L*P*mu_y^3*sinh(L*mu_y) - A*E*Iz*L^2*mu_y^4 + A*E*Iz*L*mu_y^3*sinh(L*mu_y))/(4*A*(- 2*L*mu_y*cosh((L*mu_y)/4)^2 + 2*sinh((L*mu_y)/2) + L*mu_y)^2);
elm_tangstf(4,8) = -Mya/L;
elm_tangstf(5,9) = (A*L^2*P*mu_z^2 - 8*A*P*(cosh((L*mu_z)/2)^2 - 1) - Iy*L^2*P*mu_z^4 + A*L*P*mu_z*sinh(L*mu_z) + Iy*L*P*mu_z^3*sinh(L*mu_z) - A*E*Iy*L^2*mu_z^4 + A*E*Iy*L*mu_z^3*sinh(L*mu_z))/(4*A*(- 2*L*mu_z*cosh((L*mu_z)/4)^2 + 2*sinh((L*mu_z)/2) + L*mu_z)^2);
elm_tangstf(6,10) = (((L*mu_y*cosh((L*mu_y)/2))/sinh((L*mu_y)/2) - 2)*(Mya + Myb))/(L^2*mu_y^2);
elm_tangstf(7,11) = Myb/L;
elm_tangstf(8,12) = -(A*L^2*P*mu_y^2 - 8*A*P*(cosh((L*mu_y)/2)^2 - 1) - Iz*L^2*P*mu_y^4 + A*L*P*mu_y*sinh(L*mu_y) + Iz*L*P*mu_y^3*sinh(L*mu_y) - A*E*Iz*L^2*mu_y^4 + A*E*Iz*L*mu_y^3*sinh(L*mu_y))/(4*A*(- 2*L*mu_y*cosh((L*mu_y)/4)^2 + 2*sinh((L*mu_y)/2) + L*mu_y)^2);

elm_tangstf(1,6) = -Mza/L;
elm_tangstf(4,9) = -Mza/L;
elm_tangstf(5,10) = -(((L*mu_z*cosh((L*mu_z)/2))/sinh((L*mu_z)/2) - 2)*(Mza + Mzb))/(L^2*mu_z^2);
elm_tangstf(6,11) = (mu_z^2*(2*Mxb*(4*sinh((L*mu_y)/2)^2 + 4*sinh((L*mu_z)/2)^2 - 4*(sinh((L*mu_y)/2)^2 + 1)*(sinh((L*mu_z)/2)^2 + 1) + 4) + 2*Mxb*mu_y^2*(L^2*(sinh((L*mu_y)/2)^2 + 1) - L^2*(sinh((L*mu_z)/2)^2 + 1)) - 2*Mxb*mu_y*(2*L*sinh(L*mu_y) - 4*L*sinh((L*mu_y)/2)*(sinh((L*mu_z)/2)^2 + 1)*(2*sinh((L*mu_y)/4)^2 + 1))) - 2*Mxb*mu_y^2*(4*sinh((L*mu_y)/2)^2 + 4*sinh((L*mu_z)/2)^2 - 4*(sinh((L*mu_y)/2)^2 + 1)*(sinh((L*mu_z)/2)^2 + 1) + 4) + 2*Mxb*mu_y^2*mu_z*(2*L*sinh(L*mu_z) - 4*L*sinh((L*mu_z)/2)*(sinh((L*mu_y)/2)^2 + 1)*(2*sinh((L*mu_z)/4)^2 + 1)))/((4*sinh((L*mu_y)/2)^2 - L*mu_y*sinh(L*mu_y))*(4*sinh((L*mu_z)/2)^2 - L*mu_z*sinh(L*mu_z))*(mu_y^2 - mu_z^2));
elm_tangstf(7,12) = Mzb/L;

elm_tangstf(1,7) = -(P + A*E)/L;
elm_tangstf(2,8) = -(L*(mu_y*(A*P*mu_y + A*P*mu_y*(cosh(2*L*mu_y) + sinh(2*L*mu_y))) - mu_y*(cosh(L*mu_y) + sinh(L*mu_y))*(2*Iz*P*mu_y^3 - 4*A*P*mu_y + 2*A*E*Iz*mu_y^3)) + mu_y*(3*A*P - 3*A*P*(cosh(2*L*mu_y) + sinh(2*L*mu_y)) - Iz*P*mu_y^2 + Iz*P*mu_y^2*(cosh(2*L*mu_y) + sinh(2*L*mu_y)) - A*E*Iz*mu_y^2 + A*E*Iz*mu_y^2*(cosh(2*L*mu_y) + sinh(2*L*mu_y))))/(A*(L*mu_y - 2*sinh(L*mu_y) - 2*cosh(L*mu_y) + L*mu_y*(cosh(L*mu_y) + sinh(L*mu_y)) + 2)^2);
elm_tangstf(3,9) = -(L*(mu_z*(A*P*mu_z + A*P*mu_z*(cosh(2*L*mu_z) + sinh(2*L*mu_z))) - mu_z*(cosh(L*mu_z) + sinh(L*mu_z))*(2*Iy*P*mu_z^3 - 4*A*P*mu_z + 2*A*E*Iy*mu_z^3)) + mu_z*(3*A*P - 3*A*P*(cosh(2*L*mu_z) + sinh(2*L*mu_z)) - Iy*P*mu_z^2 + Iy*P*mu_z^2*(cosh(2*L*mu_z) + sinh(2*L*mu_z)) - A*E*Iy*mu_z^2 + A*E*Iy*mu_z^2*(cosh(2*L*mu_z) + sinh(2*L*mu_z))))/(A*(L*mu_z - 2*sinh(L*mu_z) - 2*cosh(L*mu_z) + L*mu_z*(cosh(L*mu_z) + sinh(L*mu_z)) + 2)^2);
elm_tangstf(4,10) = -(Jp*P)/(A*L);
elm_tangstf(5,11) = (2*A*P*sinh(L*mu_z) - A*P*sinh(2*L*mu_z) + 2*Iy*P*mu_z^2*sinh(L*mu_z) - Iy*P*mu_z^2*sinh(2*L*mu_z) - 6*A*L*P*mu_z + 6*A*L*P*mu_z*cosh(L*mu_z) + 2*A*E*Iy*mu_z^2*sinh(L*mu_z) - A*E*Iy*mu_z^2*sinh(2*L*mu_z) - 2*Iy*L*P*mu_z^3*cosh(L*mu_z) + A*L^3*P*mu_z^3*cosh(L*mu_z) + 2*Iy*L*P*mu_z^3*cosh(L*mu_z)^2 - Iy*L^3*P*mu_z^5*cosh(L*mu_z) - 3*A*L^2*P*mu_z^2*sinh(L*mu_z) + Iy*L^2*P*mu_z^4*sinh(L*mu_z) + 2*A*E*Iy*L*mu_z^3*cosh(L*mu_z)^2 - A*E*Iy*L^3*mu_z^5*cosh(L*mu_z) + A*E*Iy*L^2*mu_z^4*sinh(L*mu_z) - 2*A*E*Iy*L*mu_z^3*cosh(L*mu_z))/(2*A*mu_z*(cosh(L*mu_z) - 1)*(4*cosh(L*mu_z) + L^2*mu_z^2 + L^2*mu_z^2*cosh(L*mu_z) - 4*L*mu_z*sinh(L*mu_z) - 4));
elm_tangstf(6,12) = (2*A*P*sinh(L*mu_y) - A*P*sinh(2*L*mu_y) + 2*Iz*P*mu_y^2*sinh(L*mu_y) - Iz*P*mu_y^2*sinh(2*L*mu_y) - 6*A*L*P*mu_y + 6*A*L*P*mu_y*cosh(L*mu_y) + 2*A*E*Iz*mu_y^2*sinh(L*mu_y) - A*E*Iz*mu_y^2*sinh(2*L*mu_y) - 2*Iz*L*P*mu_y^3*cosh(L*mu_y) + A*L^3*P*mu_y^3*cosh(L*mu_y) + 2*Iz*L*P*mu_y^3*cosh(L*mu_y)^2 - Iz*L^3*P*mu_y^5*cosh(L*mu_y) - 3*A*L^2*P*mu_y^2*sinh(L*mu_y) + Iz*L^2*P*mu_y^4*sinh(L*mu_y) + 2*A*E*Iz*L*mu_y^3*cosh(L*mu_y)^2 - A*E*Iz*L^3*mu_y^5*cosh(L*mu_y) + A*E*Iz*L^2*mu_y^4*sinh(L*mu_y) - 2*A*E*Iz*L*mu_y^3*cosh(L*mu_y))/(2*A*mu_y*(cosh(L*mu_y) - 1)*(4*cosh(L*mu_y) + L^2*mu_y^2 + L^2*mu_y^2*cosh(L*mu_y) - 4*L*mu_y*sinh(L*mu_y) - 4));

elm_tangstf(3,10) = Mzb/L;
elm_tangstf(4,11) = -(((L*mu_z*cosh((L*mu_z)/2))/sinh((L*mu_z)/2) - 2)*(Mza + Mzb))/(L^2*mu_z^2);
elm_tangstf(5,12) = -(mu_z^2*(2*Mxb*(4*sinh((L*mu_y)/2)^2 + 4*sinh((L*mu_z)/2)^2 - 4*(sinh((L*mu_y)/2)^2 + 1)*(sinh((L*mu_z)/2)^2 + 1) + 4) + 2*Mxb*mu_y^2*(L^2*(sinh((L*mu_y)/2)^2 + 1) - L^2*(sinh((L*mu_z)/2)^2 + 1)) - 2*Mxb*mu_y*(2*L*sinh(L*mu_y) - 4*L*sinh((L*mu_y)/2)*(sinh((L*mu_z)/2)^2 + 1)*(2*sinh((L*mu_y)/4)^2 + 1))) - 2*Mxb*mu_y^2*(4*sinh((L*mu_y)/2)^2 + 4*sinh((L*mu_z)/2)^2 - 4*(sinh((L*mu_y)/2)^2 + 1)*(sinh((L*mu_z)/2)^2 + 1) + 4) + 2*Mxb*mu_y^2*mu_z*(2*L*sinh(L*mu_z) - 4*L*sinh((L*mu_z)/2)*(sinh((L*mu_y)/2)^2 + 1)*(2*sinh((L*mu_z)/4)^2 + 1)))/((4*sinh((L*mu_y)/2)^2 - L*mu_y*sinh(L*mu_y))*(4*sinh((L*mu_z)/2)^2 - L*mu_z*sinh(L*mu_z))*(mu_y^2 - mu_z^2));

elm_tangstf(2,10) = Myb/L;
elm_tangstf(3,11) = -(A*L^2*P*mu_z^2 - 8*A*P*(cosh((L*mu_z)/2)^2 - 1) - Iy*L^2*P*mu_z^4 + A*L*P*mu_z*sinh(L*mu_z) + Iy*L*P*mu_z^3*sinh(L*mu_z) - A*E*Iy*L^2*mu_z^4 + A*E*Iy*L*mu_z^3*sinh(L*mu_z))/(4*A*(- 2*L*mu_z*cosh((L*mu_z)/4)^2 + 2*sinh((L*mu_z)/2) + L*mu_z)^2);
elm_tangstf(4,12) = (((L*mu_y*cosh((L*mu_y)/2))/sinh((L*mu_y)/2) - 2)*(Mya + Myb))/(L^2*mu_y^2);

elm_tangstf(2,11) = (Mxb*mu_y^2*(mu_y*cosh((L*mu_y)/2)*sinh((L*mu_z)/2) - mu_z*cosh((L*mu_z)/2)*sinh((L*mu_y)/2)))/(sinh((L*mu_z)/2)*(mu_y^2 - mu_z^2)*(2*sinh((L*mu_y)/2) - L*mu_y*cosh((L*mu_y)/2)));
elm_tangstf(3,12) = (Mxb*mu_z^2*(mu_y*cosh((L*mu_y)/2)*sinh((L*mu_z)/2) - mu_z*cosh((L*mu_z)/2)*sinh((L*mu_y)/2)))/(sinh((L*mu_y)/2)*(mu_y^2 - mu_z^2)*(2*sinh((L*mu_z)/2) - L*mu_z*cosh((L*mu_z)/2)));

elm_tangstf(1,11) = -Myb/L;
elm_tangstf(2,12) = (A*L^2*P*mu_y^2 - 8*A*P*(cosh((L*mu_y)/2)^2 - 1) - Iz*L^2*P*mu_y^4 + A*L*P*mu_y*sinh(L*mu_y) + Iz*L*P*mu_y^3*sinh(L*mu_y) - A*E*Iz*L^2*mu_y^4 + A*E*Iz*L*mu_y^3*sinh(L*mu_y))/(4*A*(- 2*L*mu_y*cosh((L*mu_y)/4)^2 + 2*sinh((L*mu_y)/2) + L*mu_y)^2);

elm_tangstf(1,12) = -Mzb/L;

elm_tangstf(5,6) = (L*Mxb*(4*mu_y^3*sinh(L*mu_y) + 4*mu_z^3*sinh(L*mu_z) - 4*mu_y*mu_z^2*sinh(L*mu_y) - 4*mu_y^2*mu_z*sinh(L*mu_z) + 4*L*mu_y^2*mu_z^2*(sinh((L*mu_y)/2)^2 + 1) + 4*L*mu_y^2*mu_z^2*(sinh((L*mu_z)/2)^2 + 1) - 8*mu_y^3*sinh((L*mu_y)/2)*(sinh((L*mu_z)/2)^2 + 1)*(2*sinh((L*mu_y)/4)^2 + 1) - 8*mu_z^3*sinh((L*mu_z)/2)*(sinh((L*mu_y)/2)^2 + 1)*(2*sinh((L*mu_z)/4)^2 + 1) + 8*mu_y*mu_z^2*sinh((L*mu_y)/2)*(sinh((L*mu_z)/2)^2 + 1)*(2*sinh((L*mu_y)/4)^2 + 1) + 8*mu_y^2*mu_z*sinh((L*mu_z)/2)*(sinh((L*mu_y)/2)^2 + 1)*(2*sinh((L*mu_z)/4)^2 + 1) - 8*L*mu_y^2*mu_z^2*(sinh((L*mu_y)/2)^2 + 1)*(sinh((L*mu_z)/2)^2 + 1) + L*mu_y*mu_z^3*sinh(L*mu_y)*sinh(L*mu_z) + L*mu_y^3*mu_z*sinh(L*mu_y)*sinh(L*mu_z)))/((4*sinh((L*mu_z)/2)^2 - L*mu_z*sinh(L*mu_z))*(8*sinh((L*mu_y)/2)^2 - 2*L*mu_y*sinh(L*mu_y))*(mu_y^2 - mu_z^2));
elm_tangstf(11,12) = -(L*Mxb*(4*mu_y^3*sinh(L*mu_y) + 4*mu_z^3*sinh(L*mu_z) - 4*mu_y*mu_z^2*sinh(L*mu_y) - 4*mu_y^2*mu_z*sinh(L*mu_z) + 4*L*mu_y^2*mu_z^2*(sinh((L*mu_y)/2)^2 + 1) + 4*L*mu_y^2*mu_z^2*(sinh((L*mu_z)/2)^2 + 1) - 8*mu_y^3*sinh((L*mu_y)/2)*(sinh((L*mu_z)/2)^2 + 1)*(2*sinh((L*mu_y)/4)^2 + 1) - 8*mu_z^3*sinh((L*mu_z)/2)*(sinh((L*mu_y)/2)^2 + 1)*(2*sinh((L*mu_z)/4)^2 + 1) + 8*mu_y*mu_z^2*sinh((L*mu_y)/2)*(sinh((L*mu_z)/2)^2 + 1)*(2*sinh((L*mu_y)/4)^2 + 1) + 8*mu_y^2*mu_z*sinh((L*mu_z)/2)*(sinh((L*mu_y)/2)^2 + 1)*(2*sinh((L*mu_z)/4)^2 + 1) - 8*L*mu_y^2*mu_z^2*(sinh((L*mu_y)/2)^2 + 1)*(sinh((L*mu_z)/2)^2 + 1) + L*mu_y*mu_z^3*sinh(L*mu_y)*sinh(L*mu_z) + L*mu_y^3*mu_z*sinh(L*mu_y)*sinh(L*mu_z)))/(2*(4*sinh((L*mu_y)/2)^2 - L*mu_y*sinh(L*mu_y))*(4*sinh((L*mu_z)/2)^2 - L*mu_z*sinh(L*mu_z))*(mu_y^2 - mu_z^2));

% --------------------------------------------- Torsion for symetric sections --------------------------------------------- */
    if (Iy == Iz) 
      elm_tangstf(2,5) = (Mxb*mu_y*(sinh(L*mu_y) - L*mu_y))/(2*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2));
      elm_tangstf(3,6) = (Mxb*mu_y*(sinh(L*mu_y) - L*mu_y))/(2*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2));
      elm_tangstf(5,8) = -(Mxb*mu_y*(sinh(L*mu_y) - L*mu_y))/(2*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2));
      elm_tangstf(6,9) = -(Mxb*mu_y*(sinh(L*mu_y) - L*mu_y))/(2*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2));
      elm_tangstf(8,11) = (Mxb*mu_y*(sinh(L*mu_y) - L*mu_y))/(2*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2));
      elm_tangstf(9,12) = (Mxb*mu_y*(sinh(L*mu_y) - L*mu_y))/(2*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2));

      elm_tangstf(6,11) = (Mxb*(L^2*mu_y^2 - 2*cosh(L*mu_y) + 2)*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2))/(2*(cosh(L*mu_y) - 1)*(4*cosh(L*mu_y) + L^2*mu_y^2 + L^2*mu_y^2*cosh(L*mu_y) - 4*L*mu_y*sinh(L*mu_y) - 4));
      elm_tangstf(5,12) = -(Mxb*(L^2*mu_y^2 - 2*cosh(L*mu_y) + 2)*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2))/(2*(cosh(L*mu_y) - 1)*(4*cosh(L*mu_y) + L^2*mu_y^2 + L^2*mu_y^2*cosh(L*mu_y) - 4*L*mu_y*sinh(L*mu_y) - 4));

      elm_tangstf(2,11) = -(Mxb*mu_y*(sinh(L*mu_y) - L*mu_y))/(2*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2));
      elm_tangstf(3,12) = -(Mxb*mu_y*(sinh(L*mu_y) - L*mu_y))/(2*(L*mu_y*sinh(L*mu_y) - 2*cosh(L*mu_y) + 2));

      elm_tangstf(5,6) = 0;
      elm_tangstf(11,12) = 0;
    end
% ------------------------------------------------------------------------------------------------------------------------- */    
% Filling the rest of the matrix
for i = 1:12
    for j = 1:12
         elm_tangstf(j,i)= elm_tangstf(i,j);
    end
end  
for i = 1:6
        elm_tangstf(i+6,i+6) = elm_tangstf(i,i);
end

end

% =========================================================== End of File */
