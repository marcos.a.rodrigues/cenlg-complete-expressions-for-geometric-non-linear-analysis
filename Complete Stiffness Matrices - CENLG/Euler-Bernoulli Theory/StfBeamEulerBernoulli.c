/*
** StfBeamEulerBernoulli.c -  this file contains 3-dimensional Euler-Bernoulli geometric stiffness matrix,
**                            and a complete tangent stiffness matrix.
**
** ----------------------------------------------------------------------------------------------------------------------------------------------
** Authors - Marcos Antonio Campos Rodrigues (Departamento de Engenharia Civil, Universidade Federal do Espírito Santo)
**           Rodrigo Bird Burgos (Departamento de Estruturas e Fundações, Universidade do Estado do Rio de Janeiro)
**			 Luiz Fernando Martha (Departamento de Engenharia Civil e Ambiental, Pontifícia Universidade Católica do Rio de Janeiro)
**
**            Reference:
**                M. A. C. Rodrigues. "Integrated Solutions for the Formulations of The Geometric Nonlinearity Problem."
**                DSc. Thesis - Pontifícia Universidade Católica do Rio de Janeiro,
**                Departamento de Engenharia Civil e Ambiental, 2019.
**
** ----------------------------------------------------------------------------------------------------------------------------------------------
** static void GeoStfBeamEulerBernoulli(int order, double L, double A, double E,
**                                                  double Iy, double Iz,
**                                                  double Fya, double Fza,
**                                      double Mxa, double Mya, double Mza,
**                                      double Fxb, double Fyb, double Fzb,
**                                      double Mxb, double Myb, double Mzb,
**                                      double *elm_geo)
**
**      L - Current element length;                                                    (  in )
**      E - Young's modulus;                                                           (  in )
**      A - Element cross section area;                                                (  in )
**      Iy - Mom. of inertia wrt "y" axis;                                             (  in )
**      Iz - Mom. of inertia wrt "z" axis;                                             (  in )
**      Fya, Fza, Mxa, Mya, Mza - Forces and moments (element initial node)            (  in )
**      Fxb, Fyb, Fzb, Mxb, Myb, Mzb - Forces and moments (element final node)         (  in )
**
**      order - Number of terms considered in stiffness matrix                         (  in )
**
**           Example how to use different orders:
**
**			 case 2 -> 2 Terms for tangent stiffness matrix (1 elastic + 1 term for axial load P);
** 
**			 " 	 LargeRotGeometricStiff(2, L, A, E, Iy, Iz, Fya, Fza, Mxa, Mya, Mza,
**		                                Fxb, Fyb, Fzb, Mxb, Myb, Mzb, elm_geo); "
**
**			 case 3 -> 3 Terms for tangent stiffness matrix (1 elastic + 1 term for axial load P + 1 term for axial load P^2);
**
**			 " 	 LargeRotGeometricStiff(2, L, A, E, Iy, Iz, Fya, Fza, Mxa, Mya, Mza,
**		                                Fxb, Fyb, Fzb, Mxb, Myb, Mzb, elm_geo2);
**	             LargeRotGeometricStiff(3, L, A, E, Iy, Iz, Fya, Fza, Mxa, Mya, Mza,
**		                                Fxb, Fyb, Fzb, Mxb, Myb, Mzb, elm_geo3);
**	            for (i = 0; i < 144; i++) {
**		             elm_geo[i] = elm_geo2[i] + elm_geo3[i];
**	            }
**
**			 case 4 -> 4 Terms for tangent stiffness matrix (1 elastic + 1 term for axial load P + 1 term for axial load P^2 + 1 term for axial load P^3);
**
**			 "  LargeRotGeometricStiff(2, L, A, E, Iy, Iz, Fya, Fza, Mxa, Mya, Mza,
**		                               Fxb, Fyb, Fzb, Mxb, Myb, Mzb, elm_geo2);
**	            LargeRotGeometricStiff(3, L, A, E, Iy, Iz, Fya, Fza, Mxa, Mya, Mza,
**		                               Fxb, Fyb, Fzb, Mxb, Myb, Mzb, elm_geo3);
**	            LargeRotGeometricStiff(4, L, A, E, Iy, Iz, Fya, Fza, Mxa, Mya, Mza,
**		                               Fxb, Fyb, Fzb, Mxb, Myb, Mzb, elm_geo4);
**	            for (i = 0; i < 144; i++) {
**		             elm_geo[i] = elm_geo2[i] + elm_geo3[i] + elm_geo4[i];
**	            }
**
**      elm_geo - Geometric stiffness matrix                                           ( out )
**
**      Returns geometric stiffness matrix of the element.
**      This method calculates the local geometric stiffness matrix for an element considering the Euler-Bernoulli beam theory. Can consider differents number of
**      terms in the matrix. For more precise results, the case 4 is indicated.
**
** ----------------------------------------------------------------------------------------------------------------------------------------------
**
** static void TangStfBeamEulerBernoulli(double L, double E, double G, double A,
**                                       double Ix, double Iy, double Iz,
**                                                   double Fya, double Fza,
**                                       double Mxa, double Mya, double Mza,
**                                       double P, double Fyb, double Fzb,
**                                       double Mxb, double Myb, double Mzb,
**                                       double *elm_tangstf)
**
**      L - Current element length;                                                    (  in )
**      E - Young's modulus;                                                           (  in )
**      G - Shear modulus;                                                             (  in )
**      A - Element cross section area;                                                (  in )
**      Ix - Mom. of inertia wrt "x" axis;                                             (  in )
**      Iy - Mom. of inertia wrt "y" axis;                                             (  in )
**      Iz - Mom. of inertia wrt "z" axis;                                             (  in )
**      Fya, Fza, Mxa, Mya, Mza - Forces and moments (element initial node)            (  in )
**      P, Fyb, Fzb, Mxb, Myb, Mzb - Forces and moments (element final node)           (  in )
**
**      elm_tangstf - Tangent stiffness matrix                                         ( out )
**
**      Returns tangent stiffness matrix of the element.
**      This method calculates the local tangent stiffness matrix for an element considering the Euler-Bernoulli beam theory.
**      This matrix is obtained directly from the solution of the equilibrium differential equation of an deformed infinitesimal element (Rodrigues, 2019).
**
*/

/* ======================== GeoStfBeamEulerBernoulli ======================= */

static void GeoStfBeamEulerBernoulli(int order, double L, double A, double E,
	                                             double Iy, double Iz,
	                                             double Fya, double Fza,
	                                 double Mxa, double Mya, double Mza,
	                                 double Fxb, double Fyb, double Fzb,
	                                 double Mxb, double Myb, double Mzb,
	                                 double *elm_geo)
{

	double  K;                            /* coefficient K */
	double  Fxb2 = Fxb*Fxb;
	double  Fxb3 = Fxb2*Fxb;
	double  L2 = L*L;
	double  L3 = L2*L;
	double  L4 = L3*L;
	double  L5 = L4*L;
	double  L6 = L5*L;
	double  E2 = E*E;
	double  E3 = E2*E;
	double  Iy2 = Iy*Iy;
	double  Iy3 = Iy2*Iy;
	double  Iz2 = Iz*Iz;
	double  Iz3 = Iz2*Iz;

	K = Fxb*(Iy + Iz) / A;

	switch (order)
	{
    /* ========== 2 terms ========== */
	case 2:
		elm_geo[0 * 12 + 0] = elm_geo[6 * 12 + 6] = Fxb / L;
		elm_geo[1 * 12 + 1] = elm_geo[7 * 12 + 7] = 6.0*Fxb / (5.0*L) + 12.0*Fxb*Iz / (A*L3);
		elm_geo[2 * 12 + 2] = elm_geo[8 * 12 + 8] = 6.0*Fxb / (5.0*L) + 12.0*Fxb*Iy / (A*L3);
		elm_geo[3 * 12 + 3] = elm_geo[9 * 12 + 9] = K / L;
		elm_geo[4 * 12 + 4] = elm_geo[10 * 12 + 10] = 2.0*Fxb*L / 15.0 + 4.0*Fxb*Iy / (A*L);
		elm_geo[5 * 12 + 5] = elm_geo[11 * 12 + 11] = 2.0*Fxb*L / 15.0 + 4.0*Fxb*Iz / (A*L);

		elm_geo[2 * 12 + 3] = elm_geo[3 * 12 + 2] = Mza / L;
		elm_geo[3 * 12 + 4] = elm_geo[4 * 12 + 3] = -Mza / 3.0 + Mzb / 6.0;
		elm_geo[5 * 12 + 6] = elm_geo[6 * 12 + 5] = Mza / L;
		elm_geo[8 * 12 + 9] = elm_geo[9 * 12 + 8] = -Mzb / L;
		elm_geo[9 * 12 + 10] = elm_geo[10 * 12 + 9] = Mza / 6.0 - Mzb / 3.0;

		elm_geo[1 * 12 + 3] = elm_geo[3 * 12 + 1] = Mya / L;
		elm_geo[2 * 12 + 4] = elm_geo[4 * 12 + 2] = -Fxb / 10.0 - 6.0*Fxb*Iy / (A*L2);
		elm_geo[3 * 10 + 5] = elm_geo[5 * 12 + 3] = Mya / 3.0 - Myb / 6.0;
		elm_geo[4 * 12 + 6] = elm_geo[6 * 12 + 4] = Mya / L;
		elm_geo[5 * 12 + 7] = elm_geo[7 * 12 + 5] = -Fxb / 10.0 - 6.0*Fxb*Iz / (A*L2);
		elm_geo[7 * 12 + 9] = elm_geo[9 * 12 + 7] = -Myb / L;
		elm_geo[8 * 12 + 10] = elm_geo[10 * 12 + 8] = Fxb / 10.0 + 6.0*Fxb*Iy / (A*L2);
		elm_geo[9 * 12 + 11] = elm_geo[11 * 12 + 9] = -Mya / 6.0 + Myb / 3.0;

		elm_geo[1 * 12 + 4] = elm_geo[4 * 12 + 1] = Mxb / L;
		elm_geo[2 * 12 + 5] = elm_geo[5 * 12 + 2] = Mxb / L;
		elm_geo[4 * 12 + 7] = elm_geo[7 * 12 + 4] = -Mxb / L;
		elm_geo[5 * 12 + 8] = elm_geo[8 * 12 + 5] = -Mxb / L;
		elm_geo[7 * 12 + 10] = elm_geo[10 * 12 + 7] = Mxb / L;
		elm_geo[8 * 12 + 11] = elm_geo[11 * 12 + 8] = Mxb / L;

		elm_geo[0 * 12 + 4] = elm_geo[4 * 12 + 0] = -Mya / L;
		elm_geo[1 * 12 + 5] = elm_geo[5 * 12 + 1] = Fxb / 10.0 + 6.0*Fxb*Iz / (A*L2);
		elm_geo[3 * 12 + 7] = elm_geo[7 * 12 + 3] = -Mya / L;
		elm_geo[4 * 12 + 8] = elm_geo[8 * 12 + 4] = Fxb / 10.0 + 6.0*Fxb*Iy / (A*L2);
		elm_geo[5 * 12 + 9] = elm_geo[9 * 12 + 5] = Mya / 6.0 + Myb / 6.0;
		elm_geo[6 * 12 + 10] = elm_geo[10 * 12 + 6] = Myb / L;
		elm_geo[7 * 12 + 11] = elm_geo[11 * 12 + 7] = -Fxb / 10.0 - 6.0*Fxb*Iz / (A*L2);

		elm_geo[0 * 12 + 5] = elm_geo[5 * 12 + 0] = -Mza / L;
		elm_geo[3 * 12 + 8] = elm_geo[8 * 12 + 3] = -Mza / L;
		elm_geo[4 * 12 + 9] = elm_geo[9 * 12 + 4] = -Mza / 6.0 - Mzb / 6.0;
		elm_geo[5 * 12 + 10] = elm_geo[10 * 12 + 5] = -Mxb / 2.0;
		elm_geo[6 * 12 + 11] = elm_geo[11 * 12 + 6] = Mzb / L;

		elm_geo[0 * 12 + 6] = elm_geo[6 * 12 + 0] = -Fxb / L;
		elm_geo[1 * 12 + 7] = elm_geo[7 * 12 + 1] = -6.0*Fxb / (5.0*L) - 12.0*Fxb*Iz / (A*L3);
		elm_geo[2 * 12 + 8] = elm_geo[8 * 12 + 2] = -6.0*Fxb / (5.0*L) - 12.0*Fxb*Iy / (A*L3);
		elm_geo[3 * 12 + 9] = elm_geo[9 * 12 + 3] = -K / L;
		elm_geo[4 * 12 + 10] = elm_geo[10 * 12 + 4] = -Fxb*L / 30.0 + 2.0*Fxb*Iy / (A*L);
		elm_geo[5 * 12 + 11] = elm_geo[11 * 12 + 5] = -Fxb*L / 30.0 + 2.0*Fxb*Iz / (A*L);

		elm_geo[2 * 12 + 9] = elm_geo[9 * 12 + 2] = Mzb / L;
		elm_geo[3 * 12 + 10] = elm_geo[10 * 12 + 3] = -Mza / 6.0 - Mzb / 6.0;
		elm_geo[4 * 12 + 11] = elm_geo[11 * 12 + 4] = Mxb / 2.0;

		elm_geo[1 * 12 + 9] = elm_geo[9 * 12 + 1] = Myb / L;
		elm_geo[2 * 12 + 10] = elm_geo[10 * 12 + 2] = -Fxb / 10.0 - 6.0*Fxb*Iy / (A*L2);
		elm_geo[3 * 12 + 11] = elm_geo[11 * 12 + 3] = Mya / 6.0 + Myb / 6.0;

		elm_geo[1 * 12 + 10] = elm_geo[10 * 12 + 1] = -Mxb / L;
		elm_geo[2 * 12 + 11] = elm_geo[11 * 12 + 2] = -Mxb / L;

		elm_geo[0 * 12 + 10] = elm_geo[10 * 12 + 0] = -Myb / L;
		elm_geo[1 * 12 + 11] = elm_geo[11 * 12 + 1] = Fxb / 10.0 + 6.0*Fxb*Iz / (A*L2);

		elm_geo[0 * 12 + 11] = elm_geo[11 * 12 + 0] = -Mzb / L;
		break;
	/* ========== 3 terms ========== */
	case 3:
		elm_geo[0 * 12 + 0] = elm_geo[6 * 12 + 6] = 0;
		elm_geo[1 * 12 + 1] = elm_geo[7 * 12 + 7] = (-L / (700 * E*Iz))*Fxb2;
		elm_geo[2 * 12 + 2] = elm_geo[8 * 12 + 8] = (-L / (700 * E*Iy))*Fxb2;
		elm_geo[3 * 12 + 3] = elm_geo[9 * 12 + 9] = 0;
		elm_geo[4 * 12 + 4] = elm_geo[10 * 12 + 10] = -(11 * L3 * Fxb2) / (6300 * E*Iy);
		elm_geo[5 * 12 + 5] = elm_geo[11 * 12 + 11] = -(11 * L3 * Fxb2) / (6300 * E*Iz);

		elm_geo[2 * 12 + 3] = elm_geo[3 * 12 + 2] = 0;
		elm_geo[3 * 12 + 4] = elm_geo[4 * 12 + 3] = (L4 * (Mza + Mzb)*Fxb2) / (15120 * E2 * Iy2) - (L2 * (Mza + Mzb)*Fxb) / (360 * E*Iy);
		elm_geo[5 * 12 + 6] = elm_geo[6 * 12 + 5] = 0;
		elm_geo[8 * 12 + 9] = elm_geo[9 * 12 + 8] = 0;
		elm_geo[9 * 12 + 10] = elm_geo[10 * 12 + 9] = (L4 * (Mza + Mzb)*Fxb2) / (15120 * E2 * Iy2) - (L2 * (Mza + Mzb)*Fxb) / (360 * E*Iy);
		
		elm_geo[1 * 12 + 3] = elm_geo[3 * 12 + 1] = 0;
		elm_geo[2 * 12 + 4] = elm_geo[4 * 12 + 2] = (L2 / (1400 * E*Iy))*Fxb2;
		elm_geo[3 * 10 + 5] = elm_geo[5 * 12 + 3] = (L4 * (Mya + Myb)*Fxb2) / (15120 * E2 * Iz2) + (L2 * (Mya + Myb)*Fxb) / (360 * E*Iz);
		elm_geo[4 * 12 + 6] = elm_geo[6 * 12 + 4] = 0;
		elm_geo[5 * 12 + 7] = elm_geo[7 * 12 + 5] = (L2 / (1400 * E*Iz))*Fxb2;
		elm_geo[7 * 12 + 9] = elm_geo[9 * 12 + 7] = 0;
		elm_geo[8 * 12 + 10] = elm_geo[10 * 12 + 8] = (-L2 / (1400 * E*Iy))*Fxb2;
		elm_geo[9 * 12 + 11] = elm_geo[11 * 12 + 9] = (L4 * (Mya + Myb)*Fxb2) / (15120 * E2 * Iz2) + (L2 * (Mya + Myb)*Fxb) / (360 * E*Iz);

		elm_geo[1 * 12 + 4] = elm_geo[4 * 12 + 1] = ((L3 * Mxb) / (2520 * E2 * Iy2) + (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 - (L*Mxb*Fxb) / (60 * E*Iy);
		elm_geo[2 * 12 + 5] = elm_geo[5 * 12 + 2] = ((L3 * Mxb) / (2520 * E2 * Iz2) + (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 - (L*Mxb*Fxb) / (60 * E*Iz);
		elm_geo[4 * 12 + 7] = elm_geo[7 * 12 + 4] = (-(L3 * Mxb) / (2520 * E2 * Iy2) - (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (L*Mxb*Fxb) / (60 * E*Iy);
		elm_geo[5 * 12 + 8] = elm_geo[8 * 12 + 5] = (-(L3 * Mxb) / (2520 * E2 * Iz2) - (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (L*Mxb*Fxb) / (60 * E*Iz);
		elm_geo[7 * 12 + 10] = elm_geo[10 * 12 + 7] = ((L3 * Mxb) / (2520 * E2 * Iy2) + (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 - (L*Mxb*Fxb) / (60 * E*Iy);
		elm_geo[8 * 12 + 11] = elm_geo[11 * 12 + 8] = ((L3 * Mxb) / (2520 * E2 * Iz2) + (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 - (L*Mxb*Fxb) / (60 * E*Iz);

		elm_geo[0 * 12 + 4] = elm_geo[4 * 12 + 0] = 0;
		elm_geo[1 * 12 + 5] = elm_geo[5 * 12 + 1] = (-L2 / (1400 * E*Iz))*Fxb2;
		elm_geo[3 * 12 + 7] = elm_geo[7 * 12 + 3] = 0;
		elm_geo[4 * 12 + 8] = elm_geo[8 * 12 + 4] = (-L2 / (1400 * E*Iy))*Fxb2;
		elm_geo[5 * 12 + 9] = elm_geo[9 * 12 + 5] = (L4 * (Mya + Myb)*Fxb2) / (15120 * E2 * Iz2) - (L2 * (Mya + Myb)*Fxb) / (360 * E*Iz);
		elm_geo[6 * 12 + 10] = elm_geo[10 * 12 + 6] = 0;
		elm_geo[7 * 12 + 11] = elm_geo[11 * 12 + 7] = (L2 / (1400 * E*Iz))*Fxb2;

		elm_geo[0 * 12 + 5] = elm_geo[5 * 12 + 0] = 0;
		elm_geo[3 * 12 + 8] = elm_geo[8 * 12 + 3] = 0;
		elm_geo[4 * 12 + 9] = elm_geo[9 * 12 + 4] = -(L4 * (Mza + Mzb)*Fxb2) / (15120 * E2 * Iy2) + (L2 * (Mza + Mzb)*Fxb) / (360 * E*Iy);
		elm_geo[5 * 12 + 10] = elm_geo[10 * 12 + 5] = (-(L4 * Mxb) / (5040 * E2 * Iy2) - (L4 * Mxb) / (5040 * E2 * Iz2) - (L4 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + ((L2 * Mxb) / (120 * E*Iy) + (L2 * Mxb) / (120 * E*Iz))*Fxb;
		elm_geo[6 * 12 + 11] = elm_geo[11 * 12 + 6] = 0;

		elm_geo[0 * 12 + 6] = elm_geo[6 * 12 + 0] = 0;
		elm_geo[1 * 12 + 7] = elm_geo[7 * 12 + 1] = (L / (700 * E*Iz))*Fxb2;
		elm_geo[2 * 12 + 8] = elm_geo[8 * 12 + 2] = (L / (700 * E*Iy))*Fxb2;
		elm_geo[3 * 12 + 9] = elm_geo[9 * 12 + 3] = 0;
		elm_geo[4 * 12 + 10] = elm_geo[10 * 12 + 4] = (13 * L3 * Fxb2) / (12600 * E*Iy);
		elm_geo[5 * 12 + 11] = elm_geo[11 * 12 + 5] = (13 * L3 * Fxb2) / (12600 * E*Iz);

		elm_geo[2 * 12 + 9] = elm_geo[9 * 12 + 2] = 0;
		elm_geo[3 * 12 + 10] = elm_geo[10 * 12 + 3] = -(L4 * (Mza + Mzb)*Fxb2) / (15120 * E2 * Iy2) + (L2 * (Mza + Mzb)*Fxb) / (360 * E*Iy);
		elm_geo[4 * 12 + 11] = elm_geo[11 * 12 + 4] = ((L4 * Mxb) / (5040 * E2 * Iy2) + (L4 * Mxb) / (5040 * E2 * Iz2) + (L4 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (-(L2 * Mxb) / (120 * E*Iy) - (L2 * Mxb) / (120 * E*Iz))*Fxb;

		elm_geo[1 * 12 + 9] = elm_geo[9 * 12 + 1] = 0;
		elm_geo[2 * 12 + 10] = elm_geo[10 * 12 + 2] = (L2 / (1400 * E*Iy))*Fxb2;
		elm_geo[3 * 12 + 11] = elm_geo[11 * 12 + 3] = (L4 * (Mya + Myb)*Fxb2) / (15120 * E2 * Iz2) - (L2 * (Mya + Myb)*Fxb) / (360 * E*Iz);

		elm_geo[1 * 12 + 10] = elm_geo[10 * 12 + 1] = (-(L3 * Mxb) / (2520 * E2 * Iy2) - (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (L*Mxb*Fxb) / (60 * E*Iy);
		elm_geo[2 * 12 + 11] = elm_geo[11 * 12 + 2] = (-(L3 * Mxb) / (2520 * E2 * Iz2) - (L3 * Mxb) / (8400 * E2 * Iy*Iz))*Fxb2 + (L*Mxb*Fxb) / (60 * E*Iz);

		elm_geo[0 * 12 + 10] = elm_geo[10 * 12 + 0] = 0;
		elm_geo[1 * 12 + 11] = elm_geo[11 * 12 + 1] = (-L2 / (1400 * E*Iz))*Fxb2;

		elm_geo[0 * 12 + 11] = elm_geo[11 * 12 + 0] = 0;

		elm_geo[4 * 12 + 5] = elm_geo[5 * 12 + 4] = ((L4 * Mxb) / (5040 * E2 * Iy2) - (L4 * Mxb) / (5040 * E2 * Iz2))*Fxb2 + (-(L2 * Mxb) / (120 * E*Iy) + (L2 * Mxb) / (120 * E*Iz))*Fxb;
		elm_geo[10 * 12 + 11] = elm_geo[11 * 12 + 10] = (-(L4 * Mxb) / (5040 * E2 * Iy2) + (L4 * Mxb) / (5040 * E2 * Iz2))*Fxb2 + ((L2 * Mxb) / (120 * E*Iy) - (L2 * Mxb) / (120 * E*Iz))*Fxb;
		break;
	/* ========== 4 terms ========== */
	case 4:
		elm_geo[0 * 12 + 0] = elm_geo[6 * 12 + 6] = 0;
		elm_geo[1 * 12 + 1] = elm_geo[7 * 12 + 7] = ((L3 / 21000 + (Iz*L) / (700 * A)) / (E2 * Iz2) - L3 / (31500 * E2 * Iz2))*Fxb3;
		elm_geo[2 * 12 + 2] = elm_geo[8 * 12 + 8] = ((L3 / 21000 + (Iy*L) / (700 * A)) / (E2 * Iy2) - L3 / (31500 * E2 * Iy2))*Fxb3;
		elm_geo[3 * 12 + 3] = elm_geo[9 * 12 + 9] = 0;
		elm_geo[4 * 12 + 4] = elm_geo[10 * 12 + 10] = ((L5 / 9000 + (11 * Iy*L3) / (6300 * A)) / (E2 * Iy2) - L5 / (13500 * E2 * Iy2))*Fxb3;
		elm_geo[5 * 12 + 5] = elm_geo[11 * 12 + 11] = ((L5 / 9000 + (11 * Iz*L3) / (6300 * A)) / (E2 * Iz2) - L5 / (13500 * E2 * Iz2))*Fxb3;

		elm_geo[2 * 12 + 3] = elm_geo[3 * 12 + 2] = 0;
		elm_geo[3 * 12 + 4] = elm_geo[4 * 12 + 3] = -(L6 * (Mza + Mzb)*Fxb3) / (604800 * E3 * Iy3);
		elm_geo[5 * 12 + 6] = elm_geo[6 * 12 + 5] = 0;
		elm_geo[8 * 12 + 9] = elm_geo[9 * 12 + 8] = 0;
		elm_geo[9 * 12 + 10] = elm_geo[10 * 12 + 9] = -(L6 * (Mza + Mzb)*Fxb3) / (604800 * E3 * Iy3);

		elm_geo[1 * 12 + 3] = elm_geo[3 * 12 + 1] = 0;
		elm_geo[2 * 12 + 4] = elm_geo[4 * 12 + 2] = (L4 / (63000 * E2 * Iy2) - (L4 / 42000 + (Iy*L2) / (1400 * A)) / (E2 * Iy2))*Fxb3;
		elm_geo[3 * 10 + 5] = elm_geo[5 * 12 + 3] = (L6 * (Mya + Myb)*Fxb3) / (604800 * E3 * Iz3);
		elm_geo[4 * 12 + 6] = elm_geo[6 * 12 + 4] = 0;
		elm_geo[5 * 12 + 7] = elm_geo[7 * 12 + 5] = (L4 / (63000 * E2 * Iz2) - (L4 / 42000 + (Iz*L2) / (1400 * A)) / (E2 * Iz2))*Fxb3;
		elm_geo[7 * 12 + 9] = elm_geo[9 * 12 + 7] = 0;
		elm_geo[8 * 12 + 10] = elm_geo[10 * 12 + 8] = ((L4 / 42000 + (Iy*L2) / (1400 * A)) / (E2 * Iy2) - L4 / (63000 * E2 * Iy2))*Fxb3;
		elm_geo[9 * 12 + 11] = elm_geo[11 * 12 + 9] = (L6 * (Mya + Myb)*Fxb3) / (604800 * E3 * Iz3);

		elm_geo[1 * 12 + 4] = elm_geo[4 * 12 + 1] = (-(L5 * Mxb) / (100800 * E3 * Iy3) - (L5 * Mxb) / (756000 * E3 * Iy*Iz2) - (L5 * Mxb) / (302400 * E3 * Iy2 * Iz))*Fxb3;
		elm_geo[2 * 12 + 5] = elm_geo[5 * 12 + 2] = (-(L5 * Mxb) / (100800 * E3 * Iz3) - (L5 * Mxb) / (302400 * E3 * Iy*Iz2) - (L5 * Mxb) / (756000 * E3 * Iy2 * Iz))*Fxb3;
		elm_geo[4 * 12 + 7] = elm_geo[7 * 12 + 4] = ((L5 * Mxb) / (100800 * E3 * Iy3) + (L5 * Mxb) / (756000 * E3 * Iy*Iz2) + (L5 * Mxb) / (302400 * E3 * Iy2 * Iz))*Fxb3;
		elm_geo[5 * 12 + 8] = elm_geo[8 * 12 + 5] = ((L5 * Mxb) / (100800 * E3 * Iz3) + (L5 * Mxb) / (302400 * E3 * Iy*Iz2) + (L5 * Mxb) / (756000 * E3 * Iy2 * Iz))*Fxb3;
		elm_geo[7 * 12 + 10] = elm_geo[10 * 12 + 7] = (-(L5 * Mxb) / (100800 * E3 * Iy3) - (L5 * Mxb) / (756000 * E3 * Iy*Iz2) - (L5 * Mxb) / (302400 * E3 * Iy2 * Iz))*Fxb3;
		elm_geo[8 * 12 + 11] = elm_geo[11 * 12 + 8] = (-(L5 * Mxb) / (100800 * E3 * Iz3) - (L5 * Mxb) / (302400 * E3 * Iy*Iz2) - (L5 * Mxb) / (756000 * E3 * Iy2 * Iz))*Fxb3;

		elm_geo[0 * 12 + 4] = elm_geo[4 * 12 + 0] = 0;
		elm_geo[1 * 12 + 5] = elm_geo[5 * 12 + 1] = ((L4 / 42000 + (Iz*L2) / (1400 * A)) / (E2 * Iz2) - L4 / (63000 * E2 * Iz2))*Fxb3;
		elm_geo[3 * 12 + 7] = elm_geo[7 * 12 + 3] = 0;
		elm_geo[4 * 12 + 8] = elm_geo[8 * 12 + 4] = ((L4 / 42000 + (Iy*L2) / (1400 * A)) / (E2 * Iy2) - L4 / (63000 * E2 * Iy2))*Fxb3;
		elm_geo[5 * 12 + 9] = elm_geo[9 * 12 + 5] = -(L6 * (Mya + Myb)*Fxb3) / (604800 * E3 * Iz3);
		elm_geo[6 * 12 + 10] = elm_geo[10 * 12 + 6] = 0;
		elm_geo[7 * 12 + 11] = elm_geo[11 * 12 + 7] = (L4 / (63000 * E2 * Iz2) - (L4 / 42000 + (Iz*L2) / (1400 * A)) / (E2 * Iz2))*Fxb3;

		elm_geo[0 * 12 + 5] = elm_geo[5 * 12 + 0] = 0;
		elm_geo[3 * 12 + 8] = elm_geo[8 * 12 + 3] = 0;
		elm_geo[4 * 12 + 9] = elm_geo[9 * 12 + 4] = (L6 * (Mza + Mzb)*Fxb3) / (604800 * E3 * Iy3);
		elm_geo[5 * 12 + 10] = elm_geo[10 * 12 + 5] = ((L6 * Mxb) / (201600 * E3 * Iy3) + (L6 * Mxb) / (201600 * E3 * Iz3) + (L6 * Mxb) / (432000 * E3 * Iy*Iz2) + (L6 * Mxb) / (432000 * E3 * Iy2 * Iz))*Fxb3;
		elm_geo[6 * 12 + 11] = elm_geo[11 * 12 + 6] = 0;

		elm_geo[0 * 12 + 6] = elm_geo[6 * 12 + 0] = 0;
		elm_geo[1 * 12 + 7] = elm_geo[7 * 12 + 1] = (L3 / (31500 * E2 * Iz2) - (L3 / 21000 + (Iz*L) / (700 * A)) / (E2 * Iz2))*Fxb3;
		elm_geo[2 * 12 + 8] = elm_geo[8 * 12 + 2] = (L3 / (31500 * E2 * Iy2) - (L3 / 21000 + (Iy*L) / (700 * A)) / (E2 * Iy2))*Fxb3;
		elm_geo[3 * 12 + 9] = elm_geo[9 * 12 + 3] = 0;
		elm_geo[4 * 12 + 10] = elm_geo[10 * 12 + 4] = (-((11 * L5) / 126000 + (13 * Iy*L3) / (12600 * A)) / (E2 * Iy2) + (11 * L5) / (189000 * E2 * Iy2))*Fxb3;
		elm_geo[5 * 12 + 11] = elm_geo[11 * 12 + 5] = (-((11 * L5) / 126000 + (13 * Iz*L3) / (12600 * A)) / (E2 * Iz2) + (11 * L5) / (189000 * E2 * Iz2))*Fxb3;

		elm_geo[2 * 12 + 9] = elm_geo[9 * 12 + 2] = 0;
		elm_geo[3 * 12 + 10] = elm_geo[10 * 12 + 3] = (L6 * (Mza + Mzb)*Fxb3) / (604800 * E3 * Iy3);
		elm_geo[4 * 12 + 11] = elm_geo[11 * 12 + 4] = (-(L6 * Mxb) / (201600 * E3 * Iy3) - (L6 * Mxb) / (201600 * E3 * Iz3) - (L6 * Mxb) / (432000 * E3 * Iy*Iz2) - (L6 * Mxb) / (432000 * E3 * Iy2 * Iz))*Fxb3;

		elm_geo[1 * 12 + 9] = elm_geo[9 * 12 + 1] = 0;
		elm_geo[2 * 12 + 10] = elm_geo[10 * 12 + 2] = (L4 / (63000 * E2 * Iy2) - (L4 / 42000 + (Iy*L2) / (1400 * A)) / (E2 * Iy2))*Fxb3;
		elm_geo[3 * 12 + 11] = elm_geo[11 * 12 + 3] = -(L6 * (Mya + Myb)*Fxb3) / (604800 * E3 * Iz3);

		elm_geo[1 * 12 + 10] = elm_geo[10 * 12 + 1] = ((L5 * Mxb) / (100800 * E3 * Iy3) + (L5 * Mxb) / (756000 * E3 * Iy*Iz2) + (L5 * Mxb) / (302400 * E3 * Iy2 * Iz))*Fxb3;
		elm_geo[2 * 12 + 11] = elm_geo[11 * 12 + 2] = ((L5 * Mxb) / (100800 * E3 * Iz3) + (L5 * Mxb) / (302400 * E3 * Iy*Iz2) + (L5 * Mxb) / (756000 * E3 * Iy2 * Iz))*Fxb3;

		elm_geo[0 * 12 + 10] = elm_geo[10 * 12 + 0] = 0;
		elm_geo[1 * 12 + 11] = elm_geo[11 * 12 + 1] = ((L4 / 42000 + (Iz*L2) / (1400 * A)) / (E2 * Iz2) - L4 / (63000 * E2 * Iz2))*Fxb3;

		elm_geo[0 * 12 + 11] = elm_geo[11 * 12 + 0] = 0;

		elm_geo[4 * 12 + 5] = elm_geo[5 * 12 + 4] = (-(L6 * Mxb) / (201600 * E3 * Iy3) + (L6 * Mxb) / (201600 * E3 * Iz3) + (L6 * Mxb) / (1008000 * E3 * Iy*Iz2) - (L6 * Mxb) / (1008000 * E3 * Iy2 * Iz))*Fxb3;
		elm_geo[10 * 12 + 11] = elm_geo[11 * 12 + 10] = ((L6 * Mxb) / (201600 * E3 * Iy3) - (L6 * Mxb) / (201600 * E3 * Iz3) - (L6 * Mxb) / (1008000 * E3 * Iy*Iz2) + (L6 * Mxb) / (1008000 * E3 * Iy2 * Iz))*Fxb3;
		break;
	}
}


/* ======================= TangStfBeamEulerBernoulli ========================= */

static void TangStfBeamEulerBernoulli(double L, double E, double G, double A,
	                                  double Ix, double Iy, double Iz,
	                                              double Fya, double Fza,
	                                  double Mxa, double Mya, double Mza,
	                                  double P, double Fyb, double Fzb,
	                                  double Mxb, double Myb, double Mzb,
	                                  double *elm_tangstf)
{
	double  Imax;                    /* maximum cross section mom. inertia */
	double  fac;                     /* factor axial force to Euler load */
	double  toler = 0.001;           /* tolerance for checking null axial force */
	double  L2 = L*L;
	double  L3 = L2*L;
	double  mu_y, mu_z;
	double  ay, az;
	double  by, bz;
	double  cy, cz;
	double  K;                       /* coefficient K */

	if ((Iy != 0.0) && (Iz != 0.0))
	{
		Imax = (Iy > Iz) ? Iy : Iz;
	}
	else if ((Iy == 0.0) && (Iz != 0.0))
	{
		Imax = Iz;
	}
	else if ((Iy != 0.0) && (Iz == 0.0))
	{
		Imax = Iy;
	}
	else
	{
		return;
	}

	fac = P / (E*Imax);            /* Verifies if it is important consider axial load in the problem - Tries to avoid numerical instability*/

	if (fac > toler)
	{
		/* --------------------------------------------- Positive axial force --------------------------------------------- */
		mu_y = sqrt(P / (E*Iz));
		mu_z = sqrt(P / (E*Iy));

		K = P *(Iy + Iz) / A;

		ay = (((L * mu_y - 2.0 * sinh(L * mu_y)) - 2.0 * cosh(L * mu_y)) + L * mu_y * (cosh(L * mu_y) + sinh(L * mu_y))) + 2.0;
		az = (((L * mu_z - 2.0 * sinh(L * mu_z)) - 2.0 * cosh(L * mu_z)) + L * mu_z * (cosh(L * mu_z) + sinh(L * mu_z))) + 2.0;
		by = (-4.0 * (cosh(L * mu_y / 2.0) * cosh(L * mu_y / 2.0)) + L * mu_y * sinh(L * mu_y)) + 4.0;
		bz = (-4.0 * (cosh(L * mu_z / 2.0) * cosh(L * mu_z / 2.0)) + L * mu_z * sinh(L * mu_z)) + 4.0;
		cy = (-2.0 * L * mu_y * (cosh(L * mu_y / 4.0) * cosh(L * mu_y / 4.0)) + 2.0 * sinh(L * mu_y / 2.0)) + L * mu_y;
		cz = (-2.0 * L * mu_z * (cosh(L * mu_z / 4.0) * cosh(L * mu_z / 4.0)) + 2.0 * sinh(L * mu_z / 2.0)) + L * mu_z;

		elm_tangstf[0 * 12 + 0] = elm_tangstf[6 * 12 + 6] = (P + A * E) / L;
		elm_tangstf[1 * 12 + 1] = elm_tangstf[7 * 12 + 7] = (L * (mu_y * (A * P * mu_y + A * P * mu_y * (cosh(2.0 * L * mu_y) + sinh(2.0 * L * mu_y))) - mu_y * (cosh(L * mu_y) + sinh(L * mu_y)) * ((2.0 * Iz * P * pow(mu_y, 3.0) - 4.0 * A * P * mu_y) + 2.0 * A * E * Iz * pow(mu_y, 3.0))) + mu_y * (((((3.0 * A * P - 3.0 * A * P * (cosh(2.0 * L * mu_y) + sinh(2.0 * L * mu_y))) - Iz * P * (mu_y * mu_y)) + Iz * P * (mu_y * mu_y) * (cosh(2.0 * L * mu_y) + sinh(2.0 * L * mu_y))) - A * E * Iz * (mu_y * mu_y)) + A * E * Iz * (mu_y * mu_y) * (cosh(2.0 * L * mu_y) + sinh(2.0 * L * mu_y)))) / (A * (ay * ay));
		elm_tangstf[2 * 12 + 2] = elm_tangstf[8 * 12 + 8] = (L * (mu_z * (A * P * mu_z + A * P * mu_z * (cosh(2.0 * L * mu_z) + sinh(2.0 * L * mu_z))) - mu_z * (cosh(L * mu_z) + sinh(L * mu_z)) * ((2.0 * Iy * P * pow(mu_z, 3.0) - 4.0 * A * P * mu_z) + 2.0 * A * E * Iy * pow(mu_z, 3.0))) + mu_z * (((((3.0 * A * P - 3.0 * A * P * (cosh(2.0 * L * mu_z) + sinh(2.0 * L * mu_z))) - Iy * P * (mu_z * mu_z)) + Iy * P * (mu_z * mu_z) * (cosh(2.0 * L * mu_z) + sinh(2.0 * L * mu_z))) - A * E * Iy * (mu_z * mu_z)) + A * E * Iy * (mu_z * mu_z) * (cosh(2.0 * L * mu_z) + sinh(2.0 * L * mu_z)))) / (A * (az * az));
		elm_tangstf[3 * 12 + 3] = elm_tangstf[9 * 12 + 9] = K / L + G*Ix / L;
		elm_tangstf[4 * 12 + 4] = elm_tangstf[10 * 12 + 10] = ((((cosh(L * mu_z / 2.0) * cosh(L * mu_z / 2.0) - 1.0) * (sinh(L * mu_z) * (Iy * P * (L * L) * pow(mu_z, 4.0) + 2.0 * Iy * P * (mu_z * mu_z)) + 2.0 * Iy * L * P * pow(mu_z, 3.0)) + Iy * pow(L, 3.0) * P * pow(mu_z, 5.0) / 2.0) - Iy * L * P * pow(mu_z, 3.0) * (cosh(L * mu_z) * cosh(L * mu_z) - 1.0)) - Iy * (L * L) * P * pow(mu_z, 4.0) * sinh(L * mu_z) / 2.0) / (A * mu_z * (bz * bz)) - (((((((sinh(L * mu_z) * ((E * Iy * (L * L) * pow(mu_z, 4.0) / 2.0 - P * (L * L) * (mu_z * mu_z)) + P) - P * sinh(2.0 * L * mu_z) / 2.0) - (cosh(L * mu_z / 2.0) * cosh(L * mu_z / 2.0) - 1.0) * ((sinh(L * mu_z) * (E * Iy * (L * L) * pow(mu_z, 4.0) + 2.0 * E * Iy * (mu_z * mu_z)) + 2.0 * L * P * mu_z) + 2.0 * E * Iy * L * pow(mu_z, 3.0))) + pow(L, 3.0) * P * pow(mu_z, 3.0) / 2.0) - E * Iy * pow(L, 3.0) * pow(mu_z, 5.0) / 2.0) + 2.0 * L * P * mu_z * (cosh(L * mu_z) * cosh(L * mu_z) - 1.0)) - L * L * P * (mu_z * mu_z) * sinh(2.0 * L * mu_z) / 4.0) + E * Iy * L * pow(mu_z, 3.0) * (cosh(L * mu_z) * cosh(L * mu_z) - 1.0)) / (mu_z * (bz * bz));
		elm_tangstf[5 * 12 + 5] = elm_tangstf[11 * 12 + 11] = ((((cosh(L * mu_y / 2.0) * cosh(L * mu_y / 2.0) - 1.0) * (sinh(L * mu_y) * (Iz * P * (L * L) * pow(mu_y, 4.0) + 2.0 * Iz * P * (mu_y * mu_y)) + 2.0 * Iz * L * P * pow(mu_y, 3.0)) + Iz * pow(L, 3.0) * P * pow(mu_y, 5.0) / 2.0) - Iz * L * P * pow(mu_y, 3.0) * (cosh(L * mu_y) * cosh(L * mu_y) - 1.0)) - Iz * (L * L) * P * pow(mu_y, 4.0) * sinh(L * mu_y) / 2.0) / (A * mu_y * (by * by)) - (((((((sinh(L * mu_y) * ((E * Iz * (L * L) * pow(mu_y, 4.0) / 2.0 - P * (L * L) * (mu_y * mu_y)) + P) - P * sinh(2.0 * L * mu_y) / 2.0) - (cosh(L * mu_y / 2.0) * cosh(L * mu_y / 2.0) - 1.0) * ((sinh(L * mu_y) * (E * Iz * (L * L) * pow(mu_y, 4.0) + 2.0 * E * Iz * (mu_y * mu_y)) + 2.0 * L * P * mu_y) + 2.0 * E * Iz * L * pow(mu_y, 3.0))) + pow(L, 3.0) * P * pow(mu_y, 3.0) / 2.0) - E * Iz * pow(L, 3.0) * pow(mu_y, 5.0) / 2.0) + 2.0 * L * P * mu_y * (cosh(L * mu_y) * cosh(L * mu_y) - 1.0)) - L * L * P * (mu_y * mu_y) * sinh(2.0 * L * mu_y) / 4.0) + E * Iz * L * pow(mu_y, 3.0) * (cosh(L * mu_y) * cosh(L * mu_y) - 1.0)) / (mu_y * (by * by));

		elm_tangstf[2 * 12 + 3] = elm_tangstf[3 * 12 + 2] = Mza / L;
		elm_tangstf[3 * 12 + 4] = elm_tangstf[4 * 12 + 3] = (cosh(L * mu_z / 2.0) * (Mza + Mzb) / (L * mu_z * sinh(L * mu_z / 2.0)) - 2.0 * (Mza + Mzb) / (L * L * (mu_z * mu_z))) - Mza / 2.0;
		elm_tangstf[4 * 12 + 5] = elm_tangstf[5 * 12 + 4] = 0;
		elm_tangstf[5 * 12 + 6] = elm_tangstf[6 * 12 + 5] = Mza / L;
		elm_tangstf[8 * 12 + 9] = elm_tangstf[9 * 12 + 8] = -Mzb / L;
		elm_tangstf[9 * 12 + 10] = elm_tangstf[10 * 12 + 9] = (cosh(L * mu_z / 2.0) * (Mza + Mzb) / (L * mu_z * sinh(L * mu_z / 2.0)) - 2.0 * (Mza + Mzb) / (L * L * (mu_z * mu_z))) - Mzb / 2.0;
		elm_tangstf[10 * 12 + 11] = elm_tangstf[11 * 12 + 10] = 0;

		elm_tangstf[1 * 12 + 3] = elm_tangstf[3 * 12 + 1] = Mya / L;
		elm_tangstf[2 * 12 + 4] = elm_tangstf[4 * 12 + 2] = -((((((A * (L * L) * P * (mu_z * mu_z) - 8.0 * A * P * (cosh(L * mu_z / 2.0) * cosh(L * mu_z / 2.0) - 1.0)) - Iy * (L * L) * P * pow(mu_z, 4.0)) + A * L * P * mu_z * sinh(L * mu_z)) + Iy * L * P * pow(mu_z, 3.0) * sinh(L * mu_z)) - A * E * Iy * (L * L) * pow(mu_z, 4.0)) + A * E * Iy * L * pow(mu_z, 3.0) * sinh(L * mu_z)) / (4.0 * A * (cz * cz));
		elm_tangstf[3 * 10 + 5] = elm_tangstf[5 * 12 + 3] = (Mya / 2.0 + 2.0 * (Mya + Myb) / (L * L * (mu_y * mu_y))) - cosh(L * mu_y / 2.0) * (Mya + Myb) / (L * mu_y * sinh(L * mu_y / 2.0));
		elm_tangstf[4 * 12 + 6] = elm_tangstf[6 * 12 + 4] = Mya / L;
		elm_tangstf[5 * 12 + 7] = elm_tangstf[7 * 12 + 5] = -((((((A * (L * L) * P * (mu_y * mu_y) - 8.0 * A * P * (cosh(L * mu_y / 2.0) * cosh(L * mu_y / 2.0) - 1.0)) - Iz * (L * L) * P * pow(mu_y, 4.0)) + A * L * P * mu_y * sinh(L * mu_y)) + Iz * L * P * pow(mu_y, 3.0) * sinh(L * mu_y)) - A * E * Iz * (L * L) * pow(mu_y, 4.0)) + A * E * Iz * L * pow(mu_y, 3.0) * sinh(L * mu_y)) / (4.0 * A * (cy * cy));
		elm_tangstf[7 * 12 + 9] = elm_tangstf[9 * 12 + 7] = -Myb / L;
		elm_tangstf[8 * 12 + 10] = elm_tangstf[10 * 12 + 8] = ((((((A * (L * L) * P * (mu_z * mu_z) - 8.0 * A * P * (cosh(L * mu_z / 2.0) * cosh(L * mu_z / 2.0) - 1.0)) - Iy * (L * L) * P * pow(mu_z, 4.0)) + A * L * P * mu_z * sinh(L * mu_z)) + Iy * L * P * pow(mu_z, 3.0) * sinh(L * mu_z)) - A * E * Iy * (L * L) * pow(mu_z, 4.0)) + A * E * Iy * L * pow(mu_z, 3.0) * sinh(L * mu_z)) / (4.0 * A * (cz * cz));
		elm_tangstf[9 * 12 + 11] = elm_tangstf[11 * 12 + 9] = (Myb / 2.0 + 2.0 * (Mya + Myb) / (L * L * (mu_y * mu_y))) - cosh(L * mu_y / 2.0) * (Mya + Myb) / (L * mu_y * sinh(L * mu_y / 2.0));

     	elm_tangstf[1 * 12 + 4] = elm_tangstf[4 * 12 + 1] = -(Mxb * (mu_y * mu_y) * (mu_y * cosh(L * mu_y / 2.0) * sinh(L * mu_z / 2.0) - mu_z * cosh(L * mu_z / 2.0) * sinh(L * mu_y / 2.0))) / (sinh(L * mu_z / 2.0) * (mu_y * mu_y - mu_z * mu_z) * (2.0 * sinh(L * mu_y / 2.0) - L * mu_y * cosh(L * mu_y / 2.0)));
		elm_tangstf[2 * 12 + 5] = elm_tangstf[5 * 12 + 2] = -(Mxb * (mu_z * mu_z) * (mu_y * cosh(L * mu_y / 2.0) * sinh(L * mu_z / 2.0) - mu_z * cosh(L * mu_z / 2.0) * sinh(L * mu_y / 2.0))) / (sinh(L * mu_y / 2.0) * (mu_y * mu_y - mu_z * mu_z) * (2.0 * sinh(L * mu_z / 2.0) - L * mu_z * cosh(L * mu_z / 2.0)));
		elm_tangstf[4 * 12 + 7] = elm_tangstf[7 * 12 + 4] = Mxb * (mu_y * mu_y) * (mu_y * cosh(L * mu_y / 2.0) * sinh(L * mu_z / 2.0) - mu_z * cosh(L * mu_z / 2.0) * sinh(L * mu_y / 2.0)) / (sinh(L * mu_z / 2.0) * (mu_y * mu_y - mu_z * mu_z) * (2.0 * sinh(L * mu_y / 2.0) - L * mu_y * cosh(L * mu_y / 2.0)));
		elm_tangstf[5 * 12 + 8] = elm_tangstf[8 * 12 + 5] = Mxb * (mu_z * mu_z) * (mu_y * cosh(L * mu_y / 2.0) * sinh(L * mu_z / 2.0) - mu_z * cosh(L * mu_z / 2.0) * sinh(L * mu_y / 2.0)) / (sinh(L * mu_y / 2.0) * (mu_y * mu_y - mu_z * mu_z) * (2.0 * sinh(L * mu_z / 2.0) - L * mu_z * cosh(L * mu_z / 2.0)));
		elm_tangstf[7 * 12 + 10] = elm_tangstf[10 * 12 + 7] = -(Mxb * (mu_y * mu_y) * (mu_y * cosh(L * mu_y / 2.0) * sinh(L * mu_z / 2.0) - mu_z * cosh(L * mu_z / 2.0) * sinh(L * mu_y / 2.0))) / (sinh(L * mu_z / 2.0) * (mu_y * mu_y - mu_z * mu_z) * (2.0 * sinh(L * mu_y / 2.0) - L * mu_y * cosh(L * mu_y / 2.0)));
		elm_tangstf[8 * 12 + 11] = elm_tangstf[11 * 12 + 8] = -(Mxb * (mu_z * mu_z) * (mu_y * cosh(L * mu_y / 2.0) * sinh(L * mu_z / 2.0) - mu_z * cosh(L * mu_z / 2.0) * sinh(L * mu_y / 2.0))) / (sinh(L * mu_y / 2.0) * (mu_y * mu_y - mu_z * mu_z) * (2.0 * sinh(L * mu_z / 2.0) - L * mu_z * cosh(L * mu_z / 2.0)));

		elm_tangstf[0 * 12 + 4] = elm_tangstf[4 * 12 + 0] = -Mya / L;
		elm_tangstf[1 * 12 + 5] = elm_tangstf[5 * 12 + 1] = ((((((A * (L * L) * P * (mu_y * mu_y) - 8.0 * A * P * (cosh(L * mu_y / 2.0) * cosh(L * mu_y / 2.0) - 1.0)) - Iz * (L * L) * P * pow(mu_y, 4.0)) + A * L * P * mu_y * sinh(L * mu_y)) + Iz * L * P * pow(mu_y, 3.0) * sinh(L * mu_y)) - A * E * Iz * (L * L) * pow(mu_y, 4.0)) + A * E * Iz * L * pow(mu_y, 3.0) * sinh(L * mu_y)) / (4.0 * A * (cy * cy));
		elm_tangstf[3 * 12 + 7] = elm_tangstf[7 * 12 + 3] = -Mya / L;
		elm_tangstf[4 * 12 + 8] = elm_tangstf[8 * 12 + 4] = ((((((A * (L * L) * P * (mu_z * mu_z) - 8.0 * A * P * (cosh(L * mu_z / 2.0) * cosh(L * mu_z / 2.0) - 1.0)) - Iy * (L * L) * P * pow(mu_z, 4.0)) + A * L * P * mu_z * sinh(L * mu_z)) + Iy * L * P * pow(mu_z, 3.0) * sinh(L * mu_z)) - A * E * Iy * (L * L) * pow(mu_z, 4.0)) + A * E * Iy * L * pow(mu_z, 3.0) * sinh(L * mu_z)) / (4.0 * A * (cz * cz));
		elm_tangstf[5 * 12 + 9] = elm_tangstf[9 * 12 + 5] = (L * mu_y * cosh(L * mu_y / 2.0) / sinh(L * mu_y / 2.0) - 2.0) * (Mya + Myb) / (L * L * (mu_y * mu_y));
		elm_tangstf[6 * 12 + 10] = elm_tangstf[10 * 12 + 6] = Myb / L;
		elm_tangstf[7 * 12 + 11] = elm_tangstf[11 * 12 + 7] = -((((((A * (L * L) * P * (mu_y * mu_y) - 8.0 * A * P * (cosh(L * mu_y / 2.0) * cosh(L * mu_y / 2.0) - 1.0)) - Iz * (L * L) * P * pow(mu_y, 4.0)) + A * L * P * mu_y * sinh(L * mu_y)) + Iz * L * P * pow(mu_y, 3.0) * sinh(L * mu_y)) - A * E * Iz * (L * L) * pow(mu_y, 4.0)) + A * E * Iz * L * pow(mu_y, 3.0) * sinh(L * mu_y)) / (4.0 * A * (cy * cy));

		elm_tangstf[0 * 12 + 5] = elm_tangstf[5 * 12 + 0] = -Mza / L;
		elm_tangstf[3 * 12 + 8] = elm_tangstf[8 * 12 + 3] = -Mza / L;
		elm_tangstf[4 * 12 + 9] = elm_tangstf[9 * 12 + 4] = -((L * mu_z * cosh(L * mu_z / 2.0) / sinh(L * mu_z / 2.0) - 2.0) * (Mza + Mzb)) / (L * L * (mu_z * mu_z));
		elm_tangstf[5 * 12 + 10] = elm_tangstf[10 * 12 + 5] = ((mu_z * mu_z * ((2.0 * Mxb * (((4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0)) + 4.0 * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0))) - 4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0)) + 4.0) + 2.0 * Mxb * (mu_y * mu_y) * (L * L * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) - L * L * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0))) - 2.0 * Mxb * mu_y * (2.0 * L * sinh(L * mu_y) - 4.0 * L * sinh(L * mu_y / 2.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0) * (2.0 * (sinh(L * mu_y / 4.0) * sinh(L * mu_y / 4.0)) + 1.0))) - 2.0 * Mxb * (mu_y * mu_y) * (((4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0)) + 4.0 * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0))) - 4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0)) + 4.0)) + 2.0 * Mxb * (mu_y * mu_y) * mu_z * (2.0 * L * sinh(L * mu_z) - 4.0 * L * sinh(L * mu_z / 2.0) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (2.0 * (sinh(L * mu_z / 4.0) * sinh(L * mu_z / 4.0)) + 1.0))) / ((4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0)) - L * mu_y * sinh(L * mu_y)) * (4.0 * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0)) - L * mu_z * sinh(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[6 * 12 + 11] = elm_tangstf[11 * 12 + 6] = Mzb / L;

		elm_tangstf[0 * 12 + 6] = elm_tangstf[6 * 12 + 0] = -(P + A * E) / L;
		elm_tangstf[1 * 12 + 7] = elm_tangstf[7 * 12 + 1] = -(L * (mu_y * (A * P * mu_y + A * P * mu_y * (cosh(2.0 * L * mu_y) + sinh(2.0 * L * mu_y))) - mu_y * (cosh(L * mu_y) + sinh(L * mu_y)) * ((2.0 * Iz * P * pow(mu_y, 3.0) - 4.0 * A * P * mu_y) + 2.0 * A * E * Iz * pow(mu_y, 3.0))) + mu_y * (((((3.0 * A * P - 3.0 * A * P * (cosh(2.0 * L * mu_y) + sinh(2.0 * L * mu_y))) - Iz * P * (mu_y * mu_y)) + Iz * P * (mu_y * mu_y) * (cosh(2.0 * L * mu_y) + sinh(2.0 * L * mu_y))) - A * E * Iz * (mu_y * mu_y)) + A * E * Iz * (mu_y * mu_y) * (cosh(2.0 * L * mu_y) + sinh(2.0 * L * mu_y)))) / (A * (ay * ay));
		elm_tangstf[2 * 12 + 8] = elm_tangstf[8 * 12 + 2] = -(L * (mu_z * (A * P * mu_z + A * P * mu_z * (cosh(2.0 * L * mu_z) + sinh(2.0 * L * mu_z))) - mu_z * (cosh(L * mu_z) + sinh(L * mu_z)) * ((2.0 * Iy * P * pow(mu_z, 3.0) - 4.0 * A * P * mu_z) + 2.0 * A * E * Iy * pow(mu_z, 3.0))) + mu_z * (((((3.0 * A * P - 3.0 * A * P * (cosh(2.0 * L * mu_z) + sinh(2.0 * L * mu_z))) - Iy * P * (mu_z * mu_z)) + Iy * P * (mu_z * mu_z) * (cosh(2.0 * L * mu_z) + sinh(2.0 * L * mu_z))) - A * E * Iy * (mu_z * mu_z)) + A * E * Iy * (mu_z * mu_z) * (cosh(2.0 * L * mu_z) + sinh(2.0 * L * mu_z)))) / (A * (az * az));
		elm_tangstf[3 * 12 + 9] = elm_tangstf[9 * 12 + 3] = -(K / L) - (G*Ix / L);
		elm_tangstf[4 * 12 + 10] = elm_tangstf[10 * 12 + 4] = (((((((((((((((((2.0 * A * P * sinh(L * mu_z) - A * P * sinh(2.0 * L * mu_z)) + 2.0 * Iy * P * (mu_z * mu_z) * sinh(L * mu_z)) - Iy * P * (mu_z * mu_z) * sinh(2.0 * L * mu_z)) - 6.0 * A * L * P * mu_z) + 6.0 * A * L * P * mu_z * cosh(L * mu_z)) + 2.0 * A * E * Iy * (mu_z * mu_z) * sinh(L * mu_z)) - A * E * Iy * (mu_z * mu_z) * sinh(2.0 * L * mu_z)) - 2.0 * Iy * L * P * pow(mu_z, 3.0) * cosh(L * mu_z)) + A * pow(L, 3.0) * P * pow(mu_z, 3.0) * cosh(L * mu_z)) + 2.0 * Iy * L * P * pow(mu_z, 3.0) * (cosh(L * mu_z) * cosh(L * mu_z))) - Iy * pow(L, 3.0) * P * pow(mu_z, 5.0) * cosh(L * mu_z)) - 3.0 * A * (L * L) * P * (mu_z * mu_z) * sinh(L * mu_z)) + Iy * (L * L) * P * pow(mu_z, 4.0) * sinh(L * mu_z)) + 2.0 * A * E * Iy * L * pow(mu_z, 3.0) * (cosh(L * mu_z) * cosh(L * mu_z))) - A * E * Iy * pow(L, 3.0) * pow(mu_z, 5.0) * cosh(L * mu_z)) + A * E * Iy * (L * L) * pow(mu_z, 4.0) * sinh(L * mu_z)) - 2.0 * A * E * Iy * L * pow(mu_z, 3.0) * cosh(L * mu_z)) / (2.0 * A * mu_z * (cosh(L * mu_z) - 1.0) * ((((4.0 * cosh(L * mu_z) + L * L * (mu_z * mu_z)) + L * L * (mu_z * mu_z) * cosh(L * mu_z)) - 4.0 * L * mu_z * sinh(L * mu_z)) - 4.0));
		elm_tangstf[5 * 12 + 11] = elm_tangstf[11 * 12 + 5] = (((((((((((((((((2.0 * A * P * sinh(L * mu_y) - A * P * sinh(2.0 * L * mu_y)) + 2.0 * Iz * P * (mu_y * mu_y) * sinh(L * mu_y)) - Iz * P * (mu_y * mu_y) * sinh(2.0 * L * mu_y)) - 6.0 * A * L * P * mu_y) + 6.0 * A * L * P * mu_y * cosh(L * mu_y)) + 2.0 * A * E * Iz * (mu_y * mu_y) * sinh(L * mu_y)) - A * E * Iz * (mu_y * mu_y) * sinh(2.0 * L * mu_y)) - 2.0 * Iz * L * P * pow(mu_y, 3.0) * cosh(L * mu_y)) + A * pow(L, 3.0) * P * pow(mu_y, 3.0) * cosh(L * mu_y)) + 2.0 * Iz * L * P * pow(mu_y, 3.0) * (cosh(L * mu_y) * cosh(L * mu_y))) - Iz * pow(L, 3.0) * P * pow(mu_y, 5.0) * cosh(L * mu_y)) - 3.0 * A * (L * L) * P * (mu_y * mu_y) * sinh(L * mu_y)) + Iz * (L * L) * P * pow(mu_y, 4.0) * sinh(L * mu_y)) + 2.0 * A * E * Iz * L * pow(mu_y, 3.0) * (cosh(L * mu_y) * cosh(L * mu_y))) - A * E * Iz * pow(L, 3.0) * pow(mu_y, 5.0) * cosh(L * mu_y)) + A * E * Iz * (L * L) * pow(mu_y, 4.0) * sinh(L * mu_y)) - 2.0 * A * E * Iz * L * pow(mu_y, 3.0) * cosh(L * mu_y)) / (2.0 * A * mu_y * (cosh(L * mu_y) - 1.0) * ((((4.0 * cosh(L * mu_y) + L * L * (mu_y * mu_y)) + L * L * (mu_y * mu_y) * cosh(L * mu_y)) - 4.0 * L * mu_y * sinh(L * mu_y)) - 4.0));

		elm_tangstf[2 * 12 + 9] = elm_tangstf[9 * 12 + 2] = Mzb / L;
		elm_tangstf[3 * 12 + 10] = elm_tangstf[10 * 12 + 3] = -((L * mu_z * cosh(L * mu_z / 2.0) / sinh(L * mu_z / 2.0) - 2.0) * (Mza + Mzb)) / (L * L * (mu_z * mu_z));
		elm_tangstf[4 * 12 + 11] = elm_tangstf[11 * 12 + 4] = -((mu_z * mu_z * ((2.0 * Mxb * (((4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0)) + 4.0 * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0))) - 4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0)) + 4.0) + 2.0 * Mxb * (mu_y * mu_y) * (L * L * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) - L * L * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0))) - 2.0 * Mxb * mu_y * (2.0 * L * sinh(L * mu_y) - 4.0 * L * sinh(L * mu_y / 2.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0) * (2.0 * (sinh(L * mu_y / 4.0) * sinh(L * mu_y / 4.0)) + 1.0))) - 2.0 * Mxb * (mu_y * mu_y) * (((4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0)) + 4.0 * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0))) - 4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0)) + 4.0)) + 2.0 * Mxb * (mu_y * mu_y) * mu_z * (2.0 * L * sinh(L * mu_z) - 4.0 * L * sinh(L * mu_z / 2.0) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (2.0 * (sinh(L * mu_z / 4.0) * sinh(L * mu_z / 4.0)) + 1.0))) / ((4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0)) - L * mu_y * sinh(L * mu_y)) * (4.0 * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0)) - L * mu_z * sinh(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));

		elm_tangstf[1 * 12 + 9] = elm_tangstf[9 * 12 + 1] = Myb / L;
		elm_tangstf[2 * 12 + 10] = elm_tangstf[10 * 12 + 2] = -((((((A * (L * L) * P * (mu_z * mu_z) - 8.0 * A * P * (cosh(L * mu_z / 2.0) * cosh(L * mu_z / 2.0) - 1.0)) - Iy * (L * L) * P * pow(mu_z, 4.0)) + A * L * P * mu_z * sinh(L * mu_z)) + Iy * L * P * pow(mu_z, 3.0) * sinh(L * mu_z)) - A * E * Iy * (L * L) * pow(mu_z, 4.0)) + A * E * Iy * L * pow(mu_z, 3.0) * sinh(L * mu_z)) / (4.0 * A * (cz * cz));
		elm_tangstf[3 * 12 + 11] = elm_tangstf[11 * 12 + 3] = (L * mu_y * cosh(L * mu_y / 2.0) / sinh(L * mu_y / 2.0) - 2.0) * (Mya + Myb) / (L * L * (mu_y * mu_y));

		elm_tangstf[1 * 12 + 10] = elm_tangstf[10 * 12 + 1] = Mxb * (mu_y * mu_y) * (mu_y * cosh(L * mu_y / 2.0) * sinh(L * mu_z / 2.0) - mu_z * cosh(L * mu_z / 2.0) * sinh(L * mu_y / 2.0)) / (sinh(L * mu_z / 2.0) * (mu_y * mu_y - mu_z * mu_z) * (2.0 * sinh(L * mu_y / 2.0) - L * mu_y * cosh(L * mu_y / 2.0)));
		elm_tangstf[2 * 12 + 11] = elm_tangstf[11 * 12 + 2] = Mxb * (mu_z * mu_z) * (mu_y * cosh(L * mu_y / 2.0) * sinh(L * mu_z / 2.0) - mu_z * cosh(L * mu_z / 2.0) * sinh(L * mu_y / 2.0)) / (sinh(L * mu_y / 2.0) * (mu_y * mu_y - mu_z * mu_z) * (2.0 * sinh(L * mu_z / 2.0) - L * mu_z * cosh(L * mu_z / 2.0)));

		elm_tangstf[0 * 12 + 10] = elm_tangstf[10 * 12 + 0] = -Myb / L;
		elm_tangstf[1 * 12 + 11] = elm_tangstf[11 * 12 + 1] = ((((((A * (L * L) * P * (mu_y * mu_y) - 8.0 * A * P * (cosh(L * mu_y / 2.0) * cosh(L * mu_y / 2.0) - 1.0)) - Iz * (L * L) * P * pow(mu_y, 4.0)) + A * L * P * mu_y * sinh(L * mu_y)) + Iz * L * P * pow(mu_y, 3.0) * sinh(L * mu_y)) - A * E * Iz * (L * L) * pow(mu_y, 4.0)) + A * E * Iz * L * pow(mu_y, 3.0) * sinh(L * mu_y)) / (4.0 * A * (cy * cy));

		elm_tangstf[0 * 12 + 11] = elm_tangstf[11 * 12 + 0] = -Mzb / L;

		elm_tangstf[4 * 12 + 5] = elm_tangstf[5 * 12 + 4] = L * Mxb * ((((((((((((4.0 * pow(mu_y, 3.0) * sinh(L * mu_y) + 4.0 * pow(mu_z, 3.0) * sinh(L * mu_z)) - 4.0 * mu_y * (mu_z * mu_z) * sinh(L * mu_y)) - 4.0 * (mu_y * mu_y) * mu_z * sinh(L * mu_z)) + 4.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0)) + 4.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0)) - 8.0 * pow(mu_y, 3.0) * sinh(L * mu_y / 2.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0) * (2.0 * (sinh(L * mu_y / 4.0) * sinh(L * mu_y / 4.0)) + 1.0)) - 8.0 * pow(mu_z, 3.0) * sinh(L * mu_z / 2.0) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (2.0 * (sinh(L * mu_z / 4.0) * sinh(L * mu_z / 4.0)) + 1.0)) + 8.0 * mu_y * (mu_z * mu_z) * sinh(L * mu_y / 2.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0) * (2.0 * (sinh(L * mu_y / 4.0) * sinh(L * mu_y / 4.0)) + 1.0)) + 8.0 * (mu_y * mu_y) * mu_z * sinh(L * mu_z / 2.0) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (2.0 * (sinh(L * mu_z / 4.0) * sinh(L * mu_z / 4.0)) + 1.0)) - 8.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0)) + L * mu_y * pow(mu_z, 3.0) * sinh(L * mu_y) * sinh(L * mu_z)) + L * pow(mu_y, 3.0) * mu_z * sinh(L * mu_y) * sinh(L * mu_z)) / ((4.0 * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0)) - L * mu_z * sinh(L * mu_z)) * (8.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0)) - 2.0 * L * mu_y * sinh(L * mu_y)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[10 * 12 + 11] = elm_tangstf[11 * 12 + 10] = -(L * Mxb * ((((((((((((4.0 * pow(mu_y, 3.0) * sinh(L * mu_y) + 4.0 * pow(mu_z, 3.0) * sinh(L * mu_z)) - 4.0 * mu_y * (mu_z * mu_z) * sinh(L * mu_y)) - 4.0 * (mu_y * mu_y) * mu_z * sinh(L * mu_z)) + 4.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0)) + 4.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0)) - 8.0 * pow(mu_y, 3.0) * sinh(L * mu_y / 2.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0) * (2.0 * (sinh(L * mu_y / 4.0) * sinh(L * mu_y / 4.0)) + 1.0)) - 8.0 * pow(mu_z, 3.0) * sinh(L * mu_z / 2.0) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (2.0 * (sinh(L * mu_z / 4.0) * sinh(L * mu_z / 4.0)) + 1.0)) + 8.0 * mu_y * (mu_z * mu_z) * sinh(L * mu_y / 2.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0) * (2.0 * (sinh(L * mu_y / 4.0) * sinh(L * mu_y / 4.0)) + 1.0)) + 8.0 * (mu_y * mu_y) * mu_z * sinh(L * mu_z / 2.0) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (2.0 * (sinh(L * mu_z / 4.0) * sinh(L * mu_z / 4.0)) + 1.0)) - 8.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0) + 1.0) * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0) + 1.0)) + L * mu_y * pow(mu_z, 3.0) * sinh(L * mu_y) * sinh(L * mu_z)) + L * pow(mu_y, 3.0) * mu_z * sinh(L * mu_y) * sinh(L * mu_z))) / (2.0 * (4.0 * (sinh(L * mu_y / 2.0) * sinh(L * mu_y / 2.0)) - L * mu_y * sinh(L * mu_y)) * (4.0 * (sinh(L * mu_z / 2.0) * sinh(L * mu_z / 2.0)) - L * mu_z * sinh(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
	
		/* --------------------------------------------- Torsion for symetric sections --------------------------------------------- */
		if (Iy == Iz) 
		{
			elm_tangstf[1 * 12 + 4] = elm_tangstf[4 * 12 + 1] = Mxb * mu_y * (sinh(L * mu_y) - L * mu_y) / (2.0 * ((L * mu_y * sinh(L * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0));
			elm_tangstf[2 * 12 + 5] = elm_tangstf[5 * 12 + 2] = Mxb * mu_y * (sinh(L * mu_y) - L * mu_y) / (2.0 * ((L * mu_y * sinh(L *	mu_y) - 2.0 * cosh(L * mu_y)) + 2.0));
			elm_tangstf[4 * 12 + 7] = elm_tangstf[7 * 12 + 4] = -(Mxb * mu_y * (sinh(L * mu_y) - L * mu_y)) / (2.0 * ((L * mu_y * sinh(L * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0));
			elm_tangstf[5 * 12 + 8] = elm_tangstf[8 * 12 + 5] = -(Mxb * mu_y * (sinh(L * mu_y) - L * mu_y)) / (2.0 * ((L * mu_y * sinh(L * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0));
			elm_tangstf[7 * 12 + 10] = elm_tangstf[10 * 12 + 7] = Mxb * mu_y * (sinh(L * mu_y) - L * mu_y) / (2.0 * ((L * mu_y * sinh(L * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0));
			elm_tangstf[8 * 12 + 11] = elm_tangstf[11 * 12 + 8] = Mxb * mu_y * (sinh(L * mu_y) - L * mu_y) / (2.0 * ((L * mu_y * sinh(L * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0));

			elm_tangstf[5 * 12 + 10] = elm_tangstf[10 * 12 + 5] = Mxb * ((L * L * (mu_y * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0) * ((L * mu_y * sinh(L * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0) / (2.0 * (cosh(L * mu_y) - 1.0) * ((((4.0 * cosh(L * mu_y) + L * L * (mu_y * mu_y)) + L * L * (mu_y * mu_y) * cosh(L * mu_y)) - 4.0 * L * mu_y * sinh(L * mu_y)) - 4.0));
			elm_tangstf[4 * 12 + 11] = elm_tangstf[11 * 12 + 4] = -(Mxb * ((L * L * (mu_y * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0) * ((L * mu_y * sinh(L * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0)) / (2.0 * (cosh(L * mu_y) - 1.0) * ((((4.0 * cosh(L * mu_y) + L * L * (mu_y * mu_y)) + L * L * (mu_y * mu_y) * cosh(L * mu_y)) - 4.0 * L * mu_y * sinh(L * mu_y)) - 4.0));

			elm_tangstf[1 * 12 + 10] = elm_tangstf[10 * 12 + 1] = -(Mxb * mu_y * (sinh(L * mu_y) - L * mu_y)) / (2.0 * ((L * mu_y * sinh(L * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0));
			elm_tangstf[2 * 12 + 11] = elm_tangstf[11 * 12 + 2] = -(Mxb * mu_y * (sinh(L * mu_y) - L * mu_y)) / (2.0 * ((L * mu_y * sinh(L * mu_y) - 2.0 * cosh(L * mu_y)) + 2.0));

			elm_tangstf[4 * 12 + 5] = elm_tangstf[5 * 12 + 4] = 0;
			elm_tangstf[10 * 12 + 11] = elm_tangstf[11 * 12 + 10] = 0;
		}
	}

	else if (fac < -toler)
	{
		/* --------------------------------------------- Negative axial force --------------------------------------------- */
		mu_y = sqrt(-P / (E*Iz));
		mu_z = sqrt(-P / (E*Iy));

		K = P*(Iy + Iz) / A;

		ay = (4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) + L * mu_y * sin(L * mu_y)) - 4.0;
		az = (4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) + L * mu_z * sin(L * mu_z)) - 4.0;

		elm_tangstf[0 * 12 + 0] = elm_tangstf[6 * 12 + 6] = (P + A * E) / L;
		elm_tangstf[1 * 12 + 1] = elm_tangstf[7 * 12 + 7] = ((cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0) * (mu_y * sin(L * mu_y) * ((6.0 * A * P + 2.0 * Iz * P * (mu_y * mu_y)) + 2.0 * A * E * Iz * (mu_y * mu_y)) - L * mu_y * ((2.0 * A * P * mu_y + 2.0 * Iz * P * pow(mu_y, 3.0)) + 2.0 * A * E * Iz * pow(mu_y, 3.0))) - A * L * P * (mu_y * mu_y) * (cos(L * mu_y) * cos(L * mu_y) - 1.0)) / (A * (ay * ay));
		elm_tangstf[2 * 12 + 2] = elm_tangstf[8 * 12 + 8] = ((cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0) * (mu_z * sin(L * mu_z) * ((6.0 * A * P + 2.0 * Iy * P * (mu_z * mu_z)) + 2.0 * A * E * Iy * (mu_z * mu_z)) - L * mu_z * ((2.0 * A * P * mu_z + 2.0 * Iy * P * pow(mu_z, 3.0)) + 2.0 * A * E * Iy * pow(mu_z, 3.0))) - A * L * P * (mu_z * mu_z) * (cos(L * mu_z) * cos(L * mu_z) - 1.0)) / (A * (az * az));
		elm_tangstf[3 * 12 + 3] = elm_tangstf[9 * 12 + 9] = K / L + G*Ix / L;
		elm_tangstf[4 * 12 + 4] = elm_tangstf[10 * 12 + 10] = (((((((((P * sin(2.0 * L * mu_z) / 2.0 + (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0) * (-2.0 * E * Iy * L * pow(mu_z, 3.0) + 2.0 * L * P * mu_z)) - sin(L * mu_z) * (((E * Iy * (L * L) * pow(mu_z, 4.0) + P * (L * L) * (mu_z * mu_z)) - E * Iy * (mu_z * mu_z)) + P)) + pow(L, 3.0) * P * pow(mu_z, 3.0) / 2.0) - E * Iy * (mu_z * mu_z) * sin(2.0 * L * mu_z) / 2.0) + E * Iy * pow(L, 3.0) * pow(mu_z, 5.0) / 2.0) - 2.0 * L * P * mu_z * (cos(L * mu_z) * cos(L * mu_z) - 1.0)) - L * L * P * (mu_z * mu_z) * sin(2.0 * L * mu_z) / 4.0) + E * Iy * L * pow(mu_z, 3.0) * (cos(L * mu_z) * cos(L * mu_z) - 1.0)) + E * Iy * (L * L) * pow(mu_z, 4.0) * sin(2.0 * L * mu_z) / 4.0) / (mu_z * (az * az)) + (((((sin(L * mu_z) * (-Iy * P * (L * L) * pow(mu_z, 4.0) + Iy * P * (mu_z * mu_z)) - Iy * P * (mu_z * mu_z) * sin(2.0 * L * mu_z) / 2.0) + Iy * pow(L, 3.0) * P * pow(mu_z, 5.0) / 2.0) + Iy * L * P * pow(mu_z, 3.0) * (cos(L * mu_z) * cos(L * mu_z) - 1.0)) - 2.0 * Iy * L * P * pow(mu_z, 3.0) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0)) + Iy * (L * L) * P * pow(mu_z, 4.0) * sin(2.0 * L * mu_z) / 4.0) / (A * mu_z * (az * az));
		elm_tangstf[5 * 12 + 5] = elm_tangstf[11 * 12 + 11] = (((((((((P * sin(2.0 * L * mu_y) / 2.0 + (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0) * (-2.0 * E * Iz * L * pow(mu_y, 3.0) + 2.0 * L * P * mu_y)) - sin(L * mu_y) * (((E * Iz * (L * L) * pow(mu_y, 4.0) + P * (L * L) * (mu_y * mu_y)) - E * Iz * (mu_y * mu_y)) + P)) + pow(L, 3.0) * P * pow(mu_y, 3.0) / 2.0) - E * Iz * (mu_y * mu_y) * sin(2.0 * L * mu_y) / 2.0) + E * Iz * pow(L, 3.0) * pow(mu_y, 5.0) / 2.0) - 2.0 * L * P * mu_y * (cos(L * mu_y) * cos(L * mu_y) - 1.0)) - L * L * P * (mu_y * mu_y) * sin(2.0 * L * mu_y) / 4.0) + E * Iz * L * pow(mu_y, 3.0) * (cos(L * mu_y) * cos(L * mu_y) - 1.0)) + E * Iz * (L * L) * pow(mu_y, 4.0) * sin(2.0 * L * mu_y) / 4.0) / (mu_y * (ay * ay)) + (((((sin(L * mu_y) * (-Iz * P * (L * L) * pow(mu_y, 4.0) + Iz * P * (mu_y * mu_y)) - Iz * P * (mu_y * mu_y) * sin(2.0 * L * mu_y) / 2.0) + Iz * pow(L, 3.0) * P * pow(mu_y, 5.0) / 2.0) + Iz * L * P * pow(mu_y, 3.0) * (cos(L * mu_y) * cos(L * mu_y) - 1.0)) - 2.0 * Iz * L * P * pow(mu_y, 3.0) * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0)) + Iz * (L * L) * P * pow(mu_y, 4.0) * sin(2.0 * L * mu_y) / 4.0) / (A * mu_y * (ay * ay));

		elm_tangstf[2 * 12 + 3] = elm_tangstf[3 * 12 + 2] = Mza / L;
		elm_tangstf[3 * 12 + 4] = elm_tangstf[4 * 12 + 3] = -Mza / 2.0 - (Mza + Mzb) * ((((L * L * (mu_z * mu_z) - 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * L * (mu_z * mu_z) * (2.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 1.0)) - 4.0 * L * mu_z * sin(L * mu_z)) + 8.0) / (L * L * (mu_z * mu_z) * ((4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) + L * mu_z * sin(L * mu_z)) - 4.0));
		elm_tangstf[4 * 12 + 5] = elm_tangstf[5 * 12 + 4] = 0;
		elm_tangstf[5 * 12 + 6] = elm_tangstf[6 * 12 + 5] = Mza / L;
		elm_tangstf[8 * 12 + 9] = elm_tangstf[9 * 12 + 8] = -Mzb / L;
		elm_tangstf[9 * 12 + 10] = elm_tangstf[10 * 12 + 9] = -Mzb / 2.0 - (Mza + Mzb) * ((((L * L * (mu_z * mu_z) - 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * L * (mu_z * mu_z) * (2.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 1.0)) - 4.0 * L * mu_z * sin(L * mu_z)) + 8.0) / (L * L * (mu_z * mu_z) * ((4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) + L * mu_z * sin(L * mu_z)) - 4.0));
		elm_tangstf[10 * 12 + 11] = elm_tangstf[11 * 12 + 10] = 0;

		elm_tangstf[1 * 12 + 3] = elm_tangstf[3 * 12 + 1] = Mya / L;
		elm_tangstf[2 * 12 + 4] = elm_tangstf[4 * 12 + 2] = (E * Iy * L * pow(mu_z, 3.0) * (sin(L * mu_z) - L * mu_z) / ((((4.0 * (L * L) * (mu_z * mu_z) - 16.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + 4.0 * (L * L) * (mu_z * mu_z) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0)) - 8.0 * L * mu_z * sin(L * mu_z)) + 16.0) - P * (((L * L * (mu_z * mu_z) + 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * mu_z * sin(L * mu_z)) - 8.0) / (2.0 * ((((L * L * (mu_z * mu_z) - 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * L * (mu_z * mu_z) * (2.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 1.0)) - 4.0 * L * mu_z * sin(L * mu_z)) + 8.0))) + Iy * L * P * pow(mu_z, 3.0) * ((2.0 * sin(L * mu_z) - sin(2.0 * L * mu_z)) + 4.0 * L * mu_z * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0)) / (4.0 * A * (az * az));
		elm_tangstf[3 * 10 + 5] = elm_tangstf[5 * 12 + 3] = Mya / 2.0 + (Mya + Myb) * ((((L * L * (mu_y * mu_y) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * L * (mu_y * mu_y) * (2.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) - 1.0)) - 4.0 * L * mu_y * sin(L * mu_y)) + 8.0) / (L * L * (mu_y * mu_y) * ((4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) + L * mu_y * sin(L * mu_y)) - 4.0));
		elm_tangstf[4 * 12 + 6] = elm_tangstf[6 * 12 + 4] = Mya / L;
		elm_tangstf[5 * 12 + 7] = elm_tangstf[7 * 12 + 5] = (E * Iz * L * pow(mu_y, 3.0) * (sin(L * mu_y) - L * mu_y) / ((((4.0 * (L * L) * (mu_y * mu_y) - 16.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + 4.0 * (L * L) * (mu_y * mu_y) * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0)) - 8.0 * L * mu_y * sin(L * mu_y)) + 16.0) - P * (((L * L * (mu_y * mu_y) + 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * mu_y * sin(L * mu_y)) - 8.0) / (2.0 * ((((L * L * (mu_y * mu_y) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * L * (mu_y * mu_y) * (2.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) - 1.0)) - 4.0 * L * mu_y * sin(L * mu_y)) + 8.0))) + Iz * L * P * pow(mu_y, 3.0) * ((2.0 * sin(L * mu_y) - sin(2.0 * L * mu_y)) + 4.0 * L * mu_y * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0)) / (4.0 * A * (ay * ay));
		elm_tangstf[7 * 12 + 9] = elm_tangstf[9 * 12 + 7] = -Myb / L;
		elm_tangstf[8 * 12 + 10] = elm_tangstf[10 * 12 + 8] = (P * (((L * L * (mu_z * mu_z) + 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * mu_z * sin(L * mu_z)) - 8.0) / (2.0 * ((((L * L * (mu_z * mu_z) - 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * L * (mu_z * mu_z) * (2.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 1.0)) - 4.0 * L * mu_z * sin(L * mu_z)) + 8.0)) - E * Iy * L * pow(mu_z, 3.0) * (sin(L * mu_z) - L * mu_z) / ((((4.0 * (L * L) * (mu_z * mu_z) - 16.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + 4.0 * (L * L) * (mu_z * mu_z) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0)) - 8.0 * L * mu_z * sin(L * mu_z)) + 16.0)) - Iy * L * P * pow(mu_z, 3.0) * ((2.0 * sin(L * mu_z) - sin(2.0 * L * mu_z)) + 4.0 * L * mu_z * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0)) / (4.0 * A * (az * az));
		elm_tangstf[9 * 12 + 11] = elm_tangstf[11 * 12 + 9] = Myb / 2.0 + (Mya + Myb) * ((((L * L * (mu_y * mu_y) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * L * (mu_y * mu_y) * (2.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) - 1.0)) - 4.0 * L * mu_y * sin(L * mu_y)) + 8.0) / (L * L * (mu_y * mu_y) * ((4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) + L * mu_y * sin(L * mu_y)) - 4.0));

		elm_tangstf[1 * 12 + 4] = elm_tangstf[4 * 12 + 1] = ((L * (Mxb * (mu_y * mu_y) * (mu_z * mu_z) * (4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + Mxb * pow(mu_y, 3.0) * mu_z * sin(L * mu_y) * sin(L * mu_z)) - Mxb * (mu_y * mu_y) * (4.0 * mu_y * sin(L * mu_y) - 8.0 * mu_y * cos(L * mu_y / 2.0) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) * sin(L * mu_y / 2.0))) + Mxb * (mu_y * mu_y) * mu_z * (4.0 * sin(L * mu_z) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * cos(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[2 * 12 + 5] = elm_tangstf[5 * 12 + 2] = -((L * (Mxb * (mu_y * mu_y) * (mu_z * mu_z) * (4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + Mxb * mu_y * pow(mu_z, 3.0) * sin(L * mu_y) * sin(L * mu_z)) - Mxb * (mu_z * mu_z) * (4.0 * mu_z * sin(L * mu_z) - 8.0 * mu_z * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * cos(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) + Mxb * mu_y * (mu_z * mu_z) * (4.0 * sin(L * mu_y) - 8.0 * cos(L * mu_y / 2.0) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) * sin(L * mu_y / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[4 * 12 + 7] = elm_tangstf[7 * 12 + 4] = -((L * (Mxb * (mu_y * mu_y) * (mu_z * mu_z) * (4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + Mxb * pow(mu_y, 3.0) * mu_z * sin(L * mu_y) * sin(L * mu_z)) - Mxb * (mu_y * mu_y) * (4.0 * mu_y * sin(L * mu_y) - 8.0 * mu_y * cos(L * mu_y / 2.0) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) * sin(L * mu_y / 2.0))) + Mxb * (mu_y * mu_y) * mu_z * (4.0 * sin(L * mu_z) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * cos(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[5 * 12 + 8] = elm_tangstf[8 * 12 + 5] = ((L * (Mxb * (mu_y * mu_y) * (mu_z * mu_z) * (4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + Mxb * mu_y * pow(mu_z, 3.0) * sin(L * mu_y) * sin(L * mu_z)) - Mxb * (mu_z * mu_z) * (4.0 * mu_z * sin(L * mu_z) - 8.0 * mu_z * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * cos(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) + Mxb * mu_y * (mu_z * mu_z) * (4.0 * sin(L * mu_y) - 8.0 * cos(L * mu_y / 2.0) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) * sin(L * mu_y / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[7 * 12 + 10] = elm_tangstf[10 * 12 + 7] = ((L * (Mxb * (mu_y * mu_y) * (mu_z * mu_z) * (4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + Mxb * pow(mu_y, 3.0) * mu_z * sin(L * mu_y) * sin(L * mu_z)) - Mxb * (mu_y * mu_y) * (4.0 * mu_y * sin(L * mu_y) - 8.0 * mu_y * cos(L * mu_y / 2.0) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) * sin(L * mu_y / 2.0))) + Mxb * (mu_y * mu_y) * mu_z * (4.0 * sin(L * mu_z) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * cos(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[8 * 12 + 11] = elm_tangstf[11 * 12 + 8] = -((L * (Mxb * (mu_y * mu_y) * (mu_z * mu_z) * (4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + Mxb * mu_y * pow(mu_z, 3.0) * sin(L * mu_y) * sin(L * mu_z)) - Mxb * (mu_z * mu_z) * (4.0 * mu_z * sin(L * mu_z) - 8.0 * mu_z * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * cos(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) + Mxb * mu_y * (mu_z * mu_z) * (4.0 * sin(L * mu_y) - 8.0 * cos(L * mu_y / 2.0) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) * sin(L * mu_y / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));

		elm_tangstf[0 * 12 + 4] = elm_tangstf[4 * 12 + 0] = -Mya / L;
		elm_tangstf[1 * 12 + 5] = elm_tangstf[5 * 12 + 1] = (P * (((L * L * (mu_y * mu_y) + 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * mu_y * sin(L * mu_y)) - 8.0) / (2.0 * ((((L * L * (mu_y * mu_y) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * L * (mu_y * mu_y) * (2.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) - 1.0)) - 4.0 * L * mu_y * sin(L * mu_y)) + 8.0)) - E * Iz * L * pow(mu_y, 3.0) * (sin(L * mu_y) - L * mu_y) / ((((4.0 * (L * L) * (mu_y * mu_y) - 16.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + 4.0 * (L * L) * (mu_y * mu_y) * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0)) - 8.0 * L * mu_y * sin(L * mu_y)) + 16.0)) - Iz * L * P * pow(mu_y, 3.0) * ((2.0 * sin(L * mu_y) - sin(2.0 * L * mu_y)) + 4.0 * L * mu_y * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0)) / (4.0 * A * (ay * ay));
		elm_tangstf[3 * 12 + 7] = elm_tangstf[7 * 12 + 3] = -Mya / L;
		elm_tangstf[4 * 12 + 8] = elm_tangstf[8 * 12 + 4] = (P * (((L * L * (mu_z * mu_z) + 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * mu_z * sin(L * mu_z)) - 8.0) / (2.0 * ((((L * L * (mu_z * mu_z) - 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * L * (mu_z * mu_z) * (2.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 1.0)) - 4.0 * L * mu_z * sin(L * mu_z)) + 8.0)) - E * Iy * L * pow(mu_z, 3.0) * (sin(L * mu_z) - L * mu_z) / ((((4.0 * (L * L) * (mu_z * mu_z) - 16.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + 4.0 * (L * L) * (mu_z * mu_z) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0)) - 8.0 * L * mu_z * sin(L * mu_z)) + 16.0)) - Iy * L * P * pow(mu_z, 3.0) * ((2.0 * sin(L * mu_z) - sin(2.0 * L * mu_z)) + 4.0 * L * mu_z * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0)) / (4.0 * A * (az * az));
		elm_tangstf[5 * 12 + 9] = elm_tangstf[9 * 12 + 5] = 2.0 * (Mya + Myb) * (((4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) + 2.0 * L * mu_y * sin(L * mu_y)) - L * L * (mu_y * mu_y) * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) - 4.0) / (L * L * (mu_y * mu_y) * ((4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) + L * mu_y * sin(L * mu_y)) - 4.0));
		elm_tangstf[6 * 12 + 10] = elm_tangstf[10 * 12 + 6] = Myb / L;
		elm_tangstf[7 * 12 + 11] = elm_tangstf[11 * 12 + 7] = (E * Iz * L * pow(mu_y, 3.0) * (sin(L * mu_y) - L * mu_y) / ((((4.0 * (L * L) * (mu_y * mu_y) - 16.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + 4.0 * (L * L) * (mu_y * mu_y) * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0)) - 8.0 * L * mu_y * sin(L * mu_y)) + 16.0) - P * (((L * L * (mu_y * mu_y) + 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * mu_y * sin(L * mu_y)) - 8.0) / (2.0 * ((((L * L * (mu_y * mu_y) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * L * (mu_y * mu_y) * (2.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) - 1.0)) - 4.0 * L * mu_y * sin(L * mu_y)) + 8.0))) + Iz * L * P * pow(mu_y, 3.0) * ((2.0 * sin(L * mu_y) - sin(2.0 * L * mu_y)) + 4.0 * L * mu_y * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0)) / (4.0 * A * (ay * ay));

		elm_tangstf[0 * 12 + 5] = elm_tangstf[5 * 12 + 0] = -Mza / L;
		elm_tangstf[3 * 12 + 8] = elm_tangstf[8 * 12 + 3] = -Mza / L;
		elm_tangstf[4 * 12 + 9] = elm_tangstf[9 * 12 + 4] = -(2.0 * (Mza + Mzb) * (((4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) + 2.0 * L * mu_z * sin(L * mu_z)) - L * L * (mu_z * mu_z) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) - 4.0)) / (L * L * (mu_z * mu_z) * ((4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) + L * mu_z * sin(L * mu_z)) - 4.0));
		elm_tangstf[5 * 12 + 10] = elm_tangstf[10 * 12 + 5] = ((mu_z * mu_z * ((2.0 * Mxb * (mu_y * mu_y) * (L * L * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * L * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) - 8.0 * Mxb * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) + 4.0 * L * Mxb * mu_y * sin(L * mu_y) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) + 8.0 * Mxb * (mu_y * mu_y) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) - 4.0 * L * Mxb * (mu_y * mu_y) * mu_z * sin(L * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[5 * 12 + 10] = elm_tangstf[10 * 12 + 5] = -Mxb / 2.0;
		elm_tangstf[6 * 12 + 11] = elm_tangstf[11 * 12 + 6] = Mzb / L;

		elm_tangstf[0 * 12 + 6] = elm_tangstf[6 * 12 + 0] = -(P + A * E) / L;
		elm_tangstf[1 * 12 + 7] = elm_tangstf[7 * 12 + 1] = -((cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0) * (mu_y * sin(L * mu_y) * ((6.0 * A * P + 2.0 * Iz * P * (mu_y * mu_y)) + 2.0 * A * E * Iz * (mu_y * mu_y)) - L * mu_y * ((2.0 * A * P * mu_y + 2.0 * Iz * P * pow(mu_y, 3.0)) + 2.0 * A * E * Iz * pow(mu_y, 3.0))) - A * L * P * (mu_y * mu_y) * (cos(L * mu_y) * cos(L * mu_y) - 1.0)) / (A * (ay * ay));
		elm_tangstf[2 * 12 + 8] = elm_tangstf[8 * 12 + 2] = -((cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0) * (mu_z * sin(L * mu_z) * ((6.0 * A * P + 2.0 * Iy * P * (mu_z * mu_z)) + 2.0 * A * E * Iy * (mu_z * mu_z)) - L * mu_z * ((2.0 * A * P * mu_z + 2.0 * Iy * P * pow(mu_z, 3.0)) + 2.0 * A * E * Iy * pow(mu_z, 3.0))) - A * L * P * (mu_z * mu_z) * (cos(L * mu_z) * cos(L * mu_z) - 1.0)) / (A * (az * az));
		elm_tangstf[3 * 12 + 9] = elm_tangstf[9 * 12 + 3] = -(K / L) - (G*Ix / L);
		elm_tangstf[4 * 12 + 10] = elm_tangstf[10 * 12 + 4] = (((((((((((((2.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 1.0) * (((((Iy * L * P * pow(mu_z, 3.0) - A * pow(L, 3.0) * P * pow(mu_z, 3.0) / 2.0) - Iy * pow(L, 3.0) * P * pow(mu_z, 5.0) / 2.0) + 3.0 * A * L * P * mu_z) + A * E * Iy * L * pow(mu_z, 3.0)) - A * E * Iy * pow(L, 3.0) * pow(mu_z, 5.0) / 2.0) + A * P * sin(L * mu_z)) - A * P * sin(2.0 * L * mu_z) / 2.0) - Iy * P * (mu_z * mu_z) * sin(L * mu_z)) + Iy * P * (mu_z * mu_z) * sin(2.0 * L * mu_z) / 2.0) - 3.0 * A * L * P * mu_z) - A * E * Iy * (mu_z * mu_z) * sin(L * mu_z)) + A * E * Iy * (mu_z * mu_z) * sin(2.0 * L * mu_z) / 2.0) - Iy * L * P * pow(mu_z, 3.0) * (cos(L * mu_z) * cos(L * mu_z))) + 3.0 * A * (L * L) * P * (mu_z * mu_z) * sin(L * mu_z) / 2.0) + Iy * (L * L) * P * pow(mu_z, 4.0) * sin(L * mu_z) / 2.0) - A * E * Iy * L * pow(mu_z, 3.0) * (cos(L * mu_z) * cos(L * mu_z))) + A * E * Iy * (L * L) * pow(mu_z, 4.0) * sin(L * mu_z) / 2.0) / (4.0 * A * mu_z * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0) * (((4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) + 2.0 * L * mu_z * sin(L * mu_z)) - L * L * (mu_z * mu_z) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) - 4.0));
		elm_tangstf[5 * 12 + 11] = elm_tangstf[11 * 12 + 5] = (((((((((((((2.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) - 1.0) * (((((Iz * L * P * pow(mu_y, 3.0) - A * pow(L, 3.0) * P * pow(mu_y, 3.0) / 2.0) - Iz * pow(L, 3.0) * P * pow(mu_y, 5.0) / 2.0) + 3.0 * A * L * P * mu_y) + A * E * Iz * L * pow(mu_y, 3.0)) - A * E * Iz * pow(L, 3.0) * pow(mu_y, 5.0) / 2.0) + A * P * sin(L * mu_y)) - A * P * sin(2.0 * L * mu_y) / 2.0) - Iz * P * (mu_y * mu_y) * sin(L * mu_y)) + Iz * P * (mu_y * mu_y) * sin(2.0 * L * mu_y) / 2.0) - 3.0 * A * L * P * mu_y) - A * E * Iz * (mu_y * mu_y) * sin(L * mu_y)) + A * E * Iz * (mu_y * mu_y) * sin(2.0 * L * mu_y) / 2.0) - Iz * L * P * pow(mu_y, 3.0) * (cos(L * mu_y) * cos(L * mu_y))) + 3.0 * A * (L * L) * P * (mu_y * mu_y) * sin(L * mu_y) / 2.0) + Iz * (L * L) * P * pow(mu_y, 4.0) * sin(L * mu_y) / 2.0) - A * E * Iz * L * pow(mu_y, 3.0) * (cos(L * mu_y) * cos(L * mu_y))) + A * E * Iz * (L * L) * pow(mu_y, 4.0) * sin(L * mu_y) / 2.0) / (4.0 * A * mu_y * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0) * (((4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) + 2.0 * L * mu_y * sin(L * mu_y)) - L * L * (mu_y * mu_y) * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) - 4.0));

		elm_tangstf[2 * 12 + 9] = elm_tangstf[9 * 12 + 2] = Mzb / L;
		elm_tangstf[3 * 12 + 10] = elm_tangstf[10 * 12 + 3] = -(2.0 * (Mza + Mzb) * (((4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) + 2.0 * L * mu_z * sin(L * mu_z)) - L * L * (mu_z * mu_z) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) - 4.0)) / (L * L * (mu_z * mu_z) * ((4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) + L * mu_z * sin(L * mu_z)) - 4.0));
		elm_tangstf[4 * 12 + 11] = elm_tangstf[11 * 12 + 4] = -((mu_z * mu_z * ((2.0 * Mxb * (mu_y * mu_y) * (L * L * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * L * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) - 8.0 * Mxb * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) + 4.0 * L * Mxb * mu_y * sin(L * mu_y) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) + 8.0 * Mxb * (mu_y * mu_y) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) - 4.0 * L * Mxb * (mu_y * mu_y) * mu_z * sin(L * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[1 * 12 + 9] = elm_tangstf[9 * 12 + 1] = Myb / L;
		elm_tangstf[2 * 12 + 10] = elm_tangstf[10 * 12 + 2] = (E * Iy * L * pow(mu_z, 3.0) * (sin(L * mu_z) - L * mu_z) / ((((4.0 * (L * L) * (mu_z * mu_z) - 16.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + 4.0 * (L * L) * (mu_z * mu_z) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0)) - 8.0 * L * mu_z * sin(L * mu_z)) + 16.0) - P * (((L * L * (mu_z * mu_z) + 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * mu_z * sin(L * mu_z)) - 8.0) / (2.0 * ((((L * L * (mu_z * mu_z) - 8.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + L * L * (mu_z * mu_z) * (2.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 1.0)) - 4.0 * L * mu_z * sin(L * mu_z)) + 8.0))) + Iy * L * P * pow(mu_z, 3.0) * ((2.0 * sin(L * mu_z) - sin(2.0 * L * mu_z)) + 4.0 * L * mu_z * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0) - 1.0)) / (4.0 * A * (az * az));
		elm_tangstf[3 * 12 + 11] = elm_tangstf[11 * 12 + 3] = 2.0 * (Mya + Myb) * (((4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) + 2.0 * L * mu_y * sin(L * mu_y)) - L * L * (mu_y * mu_y) * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) - 4.0) / (L * L * (mu_y * mu_y) * ((4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) + L * mu_y * sin(L * mu_y)) - 4.0));

		elm_tangstf[1 * 12 + 10] = elm_tangstf[10 * 12 + 1] = -((L * (Mxb * (mu_y * mu_y) * (mu_z * mu_z) * (4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 4.0 * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0))) + Mxb * pow(mu_y, 3.0) * mu_z * sin(L * mu_y) * sin(L * mu_z)) - Mxb * (mu_y * mu_y) * (4.0 * mu_y * sin(L * mu_y) - 8.0 * mu_y * cos(L * mu_y / 2.0) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) * sin(L * mu_y / 2.0))) + Mxb * (mu_y * mu_y) * mu_z * (4.0 * sin(L * mu_z) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * cos(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[2 * 12 + 11] = elm_tangstf[11 * 12 + 2] = ((L * (Mxb * (mu_y * mu_y) * (mu_z * mu_z) * (4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) - 4.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + Mxb * mu_y * pow(mu_z, 3.0) * sin(L * mu_y) * sin(L * mu_z)) - Mxb * (mu_z * mu_z) * (4.0 * mu_z * sin(L * mu_z) - 8.0 * mu_z * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) * cos(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) + Mxb * mu_y * (mu_z * mu_z) * (4.0 * sin(L * mu_y) - 8.0 * cos(L * mu_y / 2.0) * (cos(L * mu_z / 2.0) * cos(L * mu_z / 2.0)) * sin(L * mu_y / 2.0))) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));

		elm_tangstf[0 * 12 + 10] = elm_tangstf[10 * 12 + 0] = -Myb / L;
		elm_tangstf[1 * 12 + 11] = elm_tangstf[11 * 12 + 1] = (P * (((L * L * (mu_y * mu_y) + 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * mu_y * sin(L * mu_y)) - 8.0) / (2.0 * ((((L * L * (mu_y * mu_y) - 8.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + L * L * (mu_y * mu_y) * (2.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0)) - 1.0)) - 4.0 * L * mu_y * sin(L * mu_y)) + 8.0)) - E * Iz * L * pow(mu_y, 3.0) * (sin(L * mu_y) - L * mu_y) / ((((4.0 * (L * L) * (mu_y * mu_y) - 16.0 * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0))) + 4.0 * (L * L) * (mu_y * mu_y) * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0)) - 8.0 * L * mu_y * sin(L * mu_y)) + 16.0)) - Iz * L * P * pow(mu_y, 3.0) * ((2.0 * sin(L * mu_y) - sin(2.0 * L * mu_y)) + 4.0 * L * mu_y * (cos(L * mu_y / 2.0) * cos(L * mu_y / 2.0) - 1.0)) / (4.0 * A * (ay * ay));

		elm_tangstf[0 * 12 + 11] = elm_tangstf[11 * 12 + 0] = -Mzb / L;

		elm_tangstf[4 * 12 + 5] = elm_tangstf[5 * 12 + 4] = -(L * Mxb * ((((((4.0 * pow(mu_y, 3.0) * sin(L * mu_y) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) + 4.0 * pow(mu_z, 3.0) * sin(L * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0))) - 4.0 * mu_y * (mu_z * mu_z) * sin(L * mu_y) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) - 4.0 * (mu_y * mu_y) * mu_z * sin(L * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0))) + 4.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0))) + 4.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) - 8.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) / 2.0 - L * Mxb * sin(L * mu_z) * (2.0 * L * mu_y * pow(mu_z, 3.0) * sin(L * mu_y) + 2.0 * L * pow(mu_y, 3.0) * mu_z * sin(L * mu_y)) / 4.0) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		elm_tangstf[10 * 12 + 11] = elm_tangstf[11 * 12 + 10] = (L * Mxb * ((((((4.0 * pow(mu_y, 3.0) * sin(L * mu_y) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) + 4.0 * pow(mu_z, 3.0) * sin(L * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0))) - 4.0 * mu_y * (mu_z * mu_z) * sin(L * mu_y) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) - 4.0 * (mu_y * mu_y) * mu_z * sin(L * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0))) + 4.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0))) + 4.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) - 8.0 * L * (mu_y * mu_y) * (mu_z * mu_z) * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0))) / 2.0 - L * Mxb * sin(L * mu_z) * (2.0 * L * mu_y * pow(mu_z, 3.0) * sin(L * mu_y) + 2.0 * L * pow(mu_y, 3.0) * mu_z * sin(L * mu_y)) / 4.0) / ((4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)) * (4.0 * (sin(L * mu_z / 2.0) * sin(L * mu_z / 2.0)) - L * mu_z * sin(L * mu_z)) * (mu_y * mu_y - mu_z * mu_z));
		
		/* --------------------------------------------- Torsion for symetric sections --------------------------------------------- */
		if (Iy == Iz)
		{
			elm_tangstf[1 * 12 + 4] = elm_tangstf[4 * 12 + 1] = Mxb * mu_y * ((2.0 * pow(cos(L * mu_y / 2.0), 3.0) - 2.0 * cos(L * mu_y / 2.0)) + L * mu_y * sin(L * mu_y / 2.0)) / (2.0 * sin(L * mu_y / 2.0) * (4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)));
			elm_tangstf[2 * 12 + 5] = elm_tangstf[5 * 12 + 2] = -(Mxb * mu_y * (sin(L * mu_y) - L * mu_y)) / (2.0 * (4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)));
			elm_tangstf[4 * 12 + 7] = elm_tangstf[7 * 12 + 4] = -(Mxb * mu_y * ((2.0 * pow(cos(L * mu_y / 2.0), 3.0) - 2.0 * cos(L * mu_y / 2.0)) + L * mu_y * sin(L * mu_y / 2.0))) / (2.0 * sin(L * mu_y / 2.0) * (4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)));
			elm_tangstf[5 * 12 + 8] = elm_tangstf[8 * 12 + 5] = Mxb * mu_y * (sin(L * mu_y) - L * mu_y) / (2.0 * (4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)));
			elm_tangstf[7 * 12 + 10] = elm_tangstf[10 * 12 + 7] = Mxb * mu_y * ((2.0 * pow(cos(L * mu_y / 2.0), 3.0) - 2.0 * cos(L * mu_y / 2.0)) + L * mu_y * sin(L * mu_y / 2.0)) / (2.0 * sin(L * mu_y / 2.0) * (4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)));
			elm_tangstf[8 * 12 + 11] = elm_tangstf[11 * 12 + 8] = -(Mxb * mu_y * (sin(L * mu_y) - L * mu_y)) / (2.0 * (4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)));

			elm_tangstf[5 * 12 + 10] = elm_tangstf[10 * 12 + 5] = Mxb * ((2.0 * cos(L * mu_y) + L * L * (mu_y * mu_y)) - 2.0) / ((4.0 * cos(L * mu_y) + 2.0 * L * mu_y * sin(L * mu_y)) - 4.0);
			elm_tangstf[4 * 12 + 11] = elm_tangstf[11 * 12 + 4] = -(Mxb * ((2.0 * cos(L * mu_y) + L * L * (mu_y * mu_y)) - 2.0)) / ((4.0 * cos(L * mu_y) + 2.0 * L * mu_y * sin(L * mu_y)) - 4.0);

			elm_tangstf[1 * 12 + 10] = elm_tangstf[10 * 12 + 1] = -(Mxb * mu_y * ((2.0 * pow(cos(L * mu_y / 2.0), 3.0) - 2.0 * cos(L * mu_y / 2.0)) + L * mu_y * sin(L * mu_y / 2.0))) / (2.0 * sin(L * mu_y / 2.0) * (4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)));
			elm_tangstf[2 * 12 + 11] = elm_tangstf[11 * 12 + 2] = Mxb * mu_y * (sin(L * mu_y) - L * mu_y) / (2.0 * (4.0 * (sin(L * mu_y / 2.0) * sin(L * mu_y / 2.0)) - L * mu_y * sin(L * mu_y)));

			elm_tangstf[4 * 12 + 5] = elm_tangstf[5 * 12 + 4] = 0;
			elm_tangstf[10 * 12 + 11] = elm_tangstf[11 * 12 + 10] = 0;
		}
	}

	else
	{
	/* --------------------------------------------- Null axial force: complete stiffness matrix is the elastic matrix + geometric matrix with 1 term --------------------------------------------- */
		P = 0.0;

		elm_tangstf[0 * 12 + 0] = elm_tangstf[6 * 12 + 6] = E*A / L + P / L;
		elm_tangstf[1 * 12 + 1] = elm_tangstf[7 * 12 + 7] = (12.0*E*Iz / L3);
		elm_tangstf[2 * 12 + 2] = elm_tangstf[8 * 12 + 8] = (12.0*E*Iy / L3);
		elm_tangstf[3 * 12 + 3] = elm_tangstf[9 * 12 + 9] = G*Ix / L;
		elm_tangstf[4 * 12 + 4] = elm_tangstf[10 * 12 + 10] = (4.0*E*Iy / L);
		elm_tangstf[5 * 12 + 5] = elm_tangstf[11 * 12 + 11] = (4.0*E*Iz / L);
		elm_tangstf[1 * 12 + 5] = elm_tangstf[1 * 12 + 11] = (6.0*E*Iz / L2);
		elm_tangstf[2 * 12 + 4] = elm_tangstf[2 * 12 + 10] = -(6.0*E*Iy / L2);
		elm_tangstf[3 * 12 + 9] = elm_tangstf[9 * 12 + 3] = -G*Ix / L;
		elm_tangstf[4 * 12 + 10] = elm_tangstf[10 * 12 + 4] = (2.0*E*Iy / L);
		elm_tangstf[5 * 12 + 11] = elm_tangstf[11 * 12 + 5] = (2.0*E*Iz / L);
		elm_tangstf[4 * 12 + 2] = elm_tangstf[10 * 12 + 2] = elm_tangstf[2 * 12 + 4];
		elm_tangstf[4 * 12 + 8] = elm_tangstf[8 * 12 + 10] = -elm_tangstf[2 * 12 + 4];
		elm_tangstf[5 * 12 + 1] = elm_tangstf[11 * 12 + 1] = elm_tangstf[1 * 12 + 5];
		elm_tangstf[5 * 12 + 7] = elm_tangstf[7 * 12 + 11] = -elm_tangstf[1 * 12 + 5];
		elm_tangstf[6 * 12 + 0] = elm_tangstf[0 * 12 + 6] = -elm_tangstf[0 * 12 + 0];
		elm_tangstf[7 * 12 + 1] = elm_tangstf[1 * 12 + 7] = -elm_tangstf[1 * 12 + 1];
		elm_tangstf[7 * 12 + 5] = elm_tangstf[11 * 12 + 7] = -elm_tangstf[1 * 12 + 5];
		elm_tangstf[8 * 12 + 2] = elm_tangstf[2 * 12 + 8] = -elm_tangstf[2 * 12 + 2];
		elm_tangstf[8 * 12 + 4] = elm_tangstf[10 * 12 + 8] = -elm_tangstf[2 * 12 + 4];

		elm_tangstf[2 * 12 + 3] = elm_tangstf[3 * 12 + 2] = Mza / L;
		elm_tangstf[3 * 12 + 4] = elm_tangstf[4 * 12 + 3] = -Mza / 3.0 + Mzb / 6.0;
		elm_tangstf[5 * 12 + 6] = elm_tangstf[6 * 12 + 5] = Mza / L;
		elm_tangstf[8 * 12 + 9] = elm_tangstf[9 * 12 + 8] = -Mzb / L;
		elm_tangstf[9 * 12 + 10] = elm_tangstf[10 * 12 + 9] = Mza / 6.0 - Mzb / 3.0;

		elm_tangstf[1 * 12 + 3] = elm_tangstf[3 * 12 + 1] = Mya / L;
		elm_tangstf[3 * 10 + 5] = elm_tangstf[5 * 12 + 3] = Mya / 3.0 - Myb / 6.0;
		elm_tangstf[4 * 12 + 6] = elm_tangstf[6 * 12 + 4] = Mya / L;
		elm_tangstf[7 * 12 + 9] = elm_tangstf[9 * 12 + 7] = -Myb / L;
		elm_tangstf[9 * 12 + 11] = elm_tangstf[11 * 12 + 9] = -Mya / 6.0 + Myb / 3.0;

		elm_tangstf[1 * 12 + 4] = elm_tangstf[4 * 12 + 1] = Mxb / L;
		elm_tangstf[2 * 12 + 5] = elm_tangstf[5 * 12 + 2] = Mxb / L;
		elm_tangstf[4 * 12 + 7] = elm_tangstf[7 * 12 + 4] = -Mxb / L;
		elm_tangstf[5 * 12 + 8] = elm_tangstf[8 * 12 + 5] = -Mxb / L;
		elm_tangstf[7 * 12 + 10] = elm_tangstf[10 * 12 + 7] = Mxb / L;
		elm_tangstf[8 * 12 + 11] = elm_tangstf[11 * 12 + 8] = Mxb / L;

		elm_tangstf[0 * 12 + 4] = elm_tangstf[4 * 12 + 0] = -Mya / L;
		elm_tangstf[3 * 12 + 7] = elm_tangstf[7 * 12 + 3] = -Mya / L;
		elm_tangstf[5 * 12 + 9] = elm_tangstf[9 * 12 + 5] = Mya / 6.0 + Myb / 6.0;
		elm_tangstf[6 * 12 + 10] = elm_tangstf[10 * 12 + 6] = Myb / L;

		elm_tangstf[0 * 12 + 5] = elm_tangstf[5 * 12 + 0] = -Mza / L;
		elm_tangstf[3 * 12 + 8] = elm_tangstf[8 * 12 + 3] = -Mza / L;
		elm_tangstf[4 * 12 + 9] = elm_tangstf[9 * 12 + 4] = -Mza / 6.0 - Mzb / 6.0;
		elm_tangstf[5 * 12 + 10] = elm_tangstf[10 * 12 + 5] = -Mxb / 2.0;
		elm_tangstf[6 * 12 + 11] = elm_tangstf[11 * 12 + 6] = Mzb / L;

		elm_tangstf[2 * 12 + 9] = elm_tangstf[9 * 12 + 2] = Mzb / L;
		elm_tangstf[3 * 12 + 10] = elm_tangstf[10 * 12 + 3] = -Mza / 6.0 - Mzb / 6.0;
		elm_tangstf[4 * 12 + 11] = elm_tangstf[11 * 12 + 4] = Mxb / 2.0;

		elm_tangstf[1 * 12 + 9] = elm_tangstf[9 * 12 + 1] = Myb / L;
		elm_tangstf[3 * 12 + 11] = elm_tangstf[11 * 12 + 3] = Mya / 6.0 + Myb / 6.0;

		elm_tangstf[1 * 12 + 10] = elm_tangstf[10 * 12 + 1] = -Mxb / L;
		elm_tangstf[2 * 12 + 11] = elm_tangstf[11 * 12 + 2] = -Mxb / L;

		elm_tangstf[0 * 12 + 10] = elm_tangstf[10 * 12 + 0] = -Myb / L;

		elm_tangstf[0 * 12 + 11] = elm_tangstf[11 * 12 + 0] = -Mzb / L;
	}

}

/* =========================================================== End of File */