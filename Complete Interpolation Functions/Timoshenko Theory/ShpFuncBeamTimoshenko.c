/*
** ShpFuncBeamTimoshenko.c -  this file contains the shape functions for
**                            Timoshenko beam theory.
**
** ----------------------------------------------------------------------------------------------------------------------------------------------
** Authors - Marcos Antonio Campos Rodrigues (Departamento de Engenharia Civil, Universidade Federal do Espírito Santo)
**           Rodrigo Bird Burgos (Departamento de Estruturas e Fundações, Universidade do Estado do Rio de Janeiro)
**			 Luiz Fernando Martha (Departamento de Engenharia Civil e Ambiental, Pontifícia Universidade Católica do Rio de Janeiro)
**                
**            Reference:
**                M. A. C. Rodrigues. "Integrated Solutions for the Formulations of The Geometric Nonlinearity Problem."
**                DSc. Thesis - Pontifícia Universidade Católica do Rio de Janeiro, 
**                Departamento de Engenharia Civil e Ambiental, 2019.
**                
** ----------------------------------------------------------------------------------------------------------------------------------------------
**
** static void ShpFuncBeamTimoshenko(double P, double x, double L, double E, double G,
**	                                 double Ay, double Iz, char endlib, double *elm_shpfunc)
**
**      P  - Axial Force(element final node)                                           (  in )
**      x - Current position in the element;                                           (  in )
**      L - Current element length;                                                    (  in )
**      E - Young's modulus;                                                           (  in )
**      G - Shear modulus;                                                             (  in )
**      Ay - Effective shear area in y direct.;                                        (  in )
**      Iz - Mom. of inertia wrt "z" axis;                                             (  in )
**      endlib -  Element end liberations                                              (  in )
**
**      elm_shpfunc - Shape functions matrix (2x4)                                     ( out )
**                      [ N2v N3v N4v N6v
**                        N2t N3t N4t N6t ]
**      First line contains interpolation functions for transversal displacement (Nv)
**      Second line contains interpolation functions for cross-sectional rotation  (Nt)
**
**      Returns shape functions for a Timoshenko beam element.
**      This function calculates the shape functions for an element considering the Timoshenko beam theory.
**      This shape functions were obtained directly from the solution of the equilibrium differential equation of an deformed infinitesimal element (Rodrigues, 2019).
**
*/

/* ====================== ShpFuncBeamTimoshenko ========================== */

static void ShpFuncBeamTimoshenko(double P, double x, double L, double E, double G,
	                              double Ay, double Iz, char endlib, double *elm_shpfunc)

{
	double  Imin;                    /* minimum cross section mom. inertia */
	double  PE;                      /* Euler critical load */
	double  fac;                     /* factor axial force to Euler load */
	double  toler = 0.01;            /* tolerance for checking null axial force */
	double  L2 = L*L;
	double  L3 = L2*L;
	double  x2 = x*x
	double  mu;
	double  Vy;
	double  Vy2 = Vy*Vy
	double  pi = acos(-1.0);

	if ((Iy != 0.0) && (Iz != 0.0))
	{
		Imin = (Iy < Iz) ? Iy : Ix;
	}
	else if ((Iy == 0.0) && (Iz != 0.0))
	{
		Imin = Iz;
	}
	else if ((Iy != 0.0) && (Iz == 0.0))
	{
		Imin = Iy;
	}
	else
	{
		return;
	}

	PE = pi*pi*E*Imin / L2;

	fac = P / PE;                    /* Verifies if it is important consider axial load in the problem - Tries to avoid numerical instability */

	Oy = (E*Iz) / (G*Ay*L2);         /* Shear to Flexural rig.ratio y */

	switch (endlib)
	{
		/* ================================================================================ Element fixed at both ends ================================================================================ */
	case "fixed-fixed":
		/* --------------------------------------------- Positive axial force --------------------------------------------- */
		if (fac > toler)
		{
			mu = sqrt(P / (E*Iz));
			Vy = mu / (sqrt(1 + Oy*mu*mu*L2));
		
			elm_shpfunc[0, 0] = -(cosh(L*Vy) - cosh(Vy*x) + cosh(Vy*(L - x)) + L2 * Vy2 * Oy - L*Vy*sinh(L*Vy) + Vy*x*sinh(L*Vy) - L2 * Vy2 * Oy*cosh(L*Vy) + L2 * Vy2 * Oy*cosh(Vy*x) - L2 * Vy2 * Oy*cosh(Vy*(L - x)) - 1) / (L*Vy*sinh(L*Vy) - 2 * L2 * Vy2 * Oy - 2 * cosh(L*Vy) + 2 * L2 * Vy2 * Oy*cosh(L*Vy) + 2);
			elm_shpfunc[0, 1] = ((Oy*L2 * Vy2 - 1)*(sinh(L*Vy) - sinh(Vy*x) - sinh(Vy*(L - x)) - Vy*x - L*Vy*cosh(L*Vy) + Vy*x*cosh(L*Vy) + L*Vy*cosh(Vy*(L - x)) - L2 * Vy2 * Oy*sinh(L*Vy) + L2 * Vy2 * Oy*sinh(Vy*x) + L2 * Vy2 * Oy*sinh(Vy*(L - x)))) / (Vy*(L*Vy*sinh(L*Vy) - 2 * L2 * Vy2 * Oy - 2 * cosh(L*Vy) + 2 * L2 * Vy2 * Oy*cosh(L*Vy) + 2));
			elm_shpfunc[0, 2] = (cosh(Vy*(L - x)) - cosh(Vy*x) - cosh(L*Vy) - L2 * Vy2 * Oy + Vy*x*sinh(L*Vy) + L2 * Vy2 * Oy*cosh(L*Vy) + L2 * Vy2 * Oy*cosh(Vy*x) - L2 * Vy2 * Oy*cosh(Vy*(L - x)) + 1) / (L*Vy*sinh(L*Vy) - 2 * L2 * Vy2 * Oy - 2 * cosh(L*Vy) + 2 * L2 * Vy2 * Oy*cosh(L*Vy) + 2);
			elm_shpfunc[0, 3] = -((Oy*L2 * Vy2 - 1)*(sinh(L*Vy) - sinh(Vy*x) - sinh(Vy*(L - x)) + Vy*x - L*Vy + L*Vy*cosh(Vy*x) - Vy*x*cosh(L*Vy) - L2 * Vy2 * Oy*sinh(L*Vy) + L2 * Vy2 * Oy*sinh(Vy*x) + L2 * Vy2 * Oy*sinh(Vy*(L - x)))) / (Vy*(L*Vy*sinh(L*Vy) - 2 * L2 * Vy2 * Oy - 2 * cosh(L*Vy) + 2 * L2 * Vy2 * Oy*cosh(L*Vy) + 2));
		
			elm_shpfunc[1, 0] = (Vy*(sinh(Vy*x) - sinh(L*Vy) + sinh(Vy*(L - x)))) / (L*Vy*sinh(L*Vy) - 2 * L2 * Vy2 * Oy - 2 * cosh(L*Vy) + 2 * L2 * Vy2 * Oy*cosh(L*Vy) + 2);
			elm_shpfunc[1, 1] = (cosh(Vy*x) - cosh(L*Vy) - cosh(Vy*(L - x)) - L2 * Vy2 * Oy + L*Vy*sinh(Vy*(L - x)) + L2 * Vy2 * Oy*cosh(L*Vy) - L2 * Vy2 * Oy*cosh(Vy*x) + L2 * Vy2 * Oy*cosh(Vy*(L - x)) + 1) / (L*Vy*sinh(L*Vy) - 2 * L2 * Vy2 * Oy - 2 * cosh(L*Vy) + 2 * L2 * Vy2 * Oy*cosh(L*Vy) + 2);
			elm_shpfunc[1, 2] = -(Vy*(sinh(Vy*x) - sinh(L*Vy) + sinh(Vy*(L - x)))) / (L*Vy*sinh(L*Vy) - 2 * L2 * Vy2 * Oy - 2 * cosh(L*Vy) + 2 * L2 * Vy2 * Oy*cosh(L*Vy) + 2);
			elm_shpfunc[1, 3] = (cosh(Vy*(L - x)) - cosh(Vy*x) - cosh(L*Vy) - L2 * Vy2 * Oy + L*Vy*sinh(Vy*x) + L2 * Vy2 * Oy*cosh(L*Vy) + L2 * Vy2 * Oy*cosh(Vy*x) - L2 * Vy2 * Oy*cosh(Vy*(L - x)) + 1) / (L*Vy*sinh(L*Vy) - 2 * L2 * Vy2 * Oy - 2 * cosh(L*Vy) + 2 * L2 * Vy2 * Oy*cosh(L*Vy) + 2);
		}

		/* --------------------------------------------- Negative axial force --------------------------------------------- */
		else if (fac < -toler)
		{
			mu = sqrt(-P / (E*Iz));
			Vy = mu / (sqrt(1 - Oy*mu*mu*L2));

			elm_shpfunc[0, 0] = (cos(L*Vy) - cos(Vy*x) + cos(Vy*(L - x)) - L2 * Vy2 * Oy + L*Vy*sin(L*Vy) - Vy*x*sin(L*Vy) + L2 * Vy2 * Oy*cos(L*Vy) - L2 * Vy2 * Oy*cos(Vy*x) + L2 * Vy2 * Oy*cos(Vy*(L - x)) - 1) / (2 * cos(L*Vy) - 2 * L2 * Vy2 * Oy + L*Vy*sin(L*Vy) + 2 * L2 * Vy2 * Oy*cos(L*Vy) - 2);
			elm_shpfunc[0, 1] = -((Oy*L2 * Vy2 + 1)*(sin(Vy*x) - sin(L*Vy) + sin(Vy*(L - x)) + Vy*x + L*Vy*cos(L*Vy) - Vy*x*cos(L*Vy) - L*Vy*cos(Vy*(L - x)) - L2 * Vy2 * Oy*sin(L*Vy) + L2 * Vy2 * Oy*sin(Vy*x) + L2 * Vy2 * Oy*sin(Vy*(L - x)))) / (Vy*(2 * cos(L*Vy) - 2 * L2 * Vy2 * Oy + L*Vy*sin(L*Vy) + 2 * L2 * Vy2 * Oy*cos(L*Vy) - 2));
			elm_shpfunc[0, 2] = (cos(L*Vy) + cos(Vy*x) - cos(Vy*(L - x)) - L2 * Vy2 * Oy + Vy*x*sin(L*Vy) + L2 * Vy2 * Oy*cos(L*Vy) + L2 * Vy2 * Oy*cos(Vy*x) - L2 * Vy2 * Oy*cos(Vy*(L - x)) - 1) / (2 * cos(L*Vy) - 2 * L2 * Vy2 * Oy + L*Vy*sin(L*Vy) + 2 * L2 * Vy2 * Oy*cos(L*Vy) - 2);
			elm_shpfunc[0, 3] = ((Oy*L2 * Vy2 + 1)*(sin(Vy*x) - sin(L*Vy) + sin(Vy*(L - x)) - Vy*x + L*Vy - L*Vy*cos(Vy*x) + Vy*x*cos(L*Vy) - L2 * Vy2 * Oy*sin(L*Vy) + L2 * Vy2 * Oy*sin(Vy*x) + L2 * Vy2 * Oy*sin(Vy*(L - x)))) / (Vy*(2 * cos(L*Vy) - 2 * L2 * Vy2 * Oy + L*Vy*sin(L*Vy) + 2 * L2 * Vy2 * Oy*cos(L*Vy) - 2));

			elm_shpfunc[1, 0] = (Vy*(sin(Vy*x) - sin(L*Vy) + sin(Vy*(L - x)))) / (2 * cos(L*Vy) - 2 * L2 * Vy2 * Oy + L*Vy*sin(L*Vy) + 2 * L2 * Vy2 * Oy*cos(L*Vy) - 2);
			elm_shpfunc[1, 1] = (cos(L*Vy) - cos(Vy*x) + cos(Vy*(L - x)) - L2 * Vy2 * Oy + L*Vy*sin(Vy*(L - x)) + L2 * Vy2 * Oy*cos(L*Vy) - L2 * Vy2 * Oy*cos(Vy*x) + L2 * Vy2 * Oy*cos(Vy*(L - x)) - 1) / (2 * cos(L*Vy) - 2 * L2 * Vy2 * Oy + L*Vy*sin(L*Vy) + 2 * L2 * Vy2 * Oy*cos(L*Vy) - 2);
			elm_shpfunc[1, 2] = -(Vy*(sin(Vy*x) - sin(L*Vy) + sin(Vy*(L - x)))) / (2 * cos(L*Vy) - 2 * L2 * Vy2 * Oy + L*Vy*sin(L*Vy) + 2 * L2 * Vy2 * Oy*cos(L*Vy) - 2);
			elm_shpfunc[1, 3] = (cos(L*Vy) + cos(Vy*x) - cos(Vy*(L - x)) - L2 * Vy2 * Oy + L*Vy*sin(Vy*x) + L2 * Vy2 * Oy*cos(L*Vy) + L2 * Vy2 * Oy*cos(Vy*x) - L2 * Vy2 * Oy*cos(Vy*(L - x)) - 1) / (2 * cos(L*Vy) - 2 * L2 * Vy2 * Oy + L*Vy*sin(L*Vy) + 2 * L2 * Vy2 * Oy*cos(L*Vy) - 2);
		}
		/* --------------------------------------------- Null axial force: Cubic Functions --------------------------------------------- */
		else
		{
			P = 0.0;

			elm_shpfunc[0, 0] = ((L - x)*(L*x + 12 * L2 * Oy + L2 - 2 * x2)) / (L3 * (12 * Oy + 1));
			elm_shpfunc[0, 1] = (x*(L - x)*(L - x + 6 * L*Oy)) / (L2 * (12 * Oy + 1);
			elm_shpfunc[0, 2] = (x*(12 * Oy*L2 + 3 * L*x - 2 * x2)) / (L3 * (12 * Oy + 1));
			elm_shpfunc[0, 3] = -(x*(L - x)*(x + 6 * L*Oy)) / (L2 * (12 * Oy + 1));

			elm_shpfunc[1, 0] = -(6 * x*(L - x)) / (L3 * (12 * Oy + 1));
			elm_shpfunc[1, 1] = ((L - x)*(L - 3 * x + 12 * L*Oy)) / (L2 * (12 * Oy + 1));
			elm_shpfunc[1, 2] = (6 * x*(L - x)) / (L3 * (12 * Oy + 1));
			elm_shpfunc[1, 3] = (x*(3 * x - 2 * L + 12 * L*Oy)) / (L2 * (12 * Oy + 1));
		}
		break;

		/* ================================================================================ Element free at left and fixed at right ================================================================================ */
	case "free-fixed":
		/* --------------------------------------------- Positive axial force --------------------------------------------- */
		if (fac > toler)
		{
			mu = sqrt(P / (E*Iz));
			Vy = mu / (sqrt(1 + Oy*mu*mu*L2));

			elm_shpfunc[0, 0] = -(sinh(L*Vy) - sinh(Vy*x) - L*Vy*cosh(L*Vy) + Vy*x*cosh(L*Vy) - L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(Vy*x)) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[0, 1] = 0;
			elm_shpfunc[0, 2] = (Vy*x*cosh(L*Vy) - sinh(Vy*x) + L ^ 2 * Vy ^ 2 * Oy*sinh(Vy*x)) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[0, 3] = -((Oy*L ^ 2 * Vy ^ 2 - 1)*(L*sinh(Vy*x) - x*sinh(L*Vy))) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));

			elm_shpfunc[1, 0] = -(Vy*(cosh(L*Vy) - cosh(Vy*x))) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[1, 1] = 0;
			elm_shpfunc[1, 2] = (Vy*(cosh(L*Vy) - cosh(Vy*x))) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[1, 3] = (L*Vy*cosh(Vy*x) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy)) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
		}

		/* --------------------------------------------- Negative axial force --------------------------------------------- */
		else if (fac < -toler)
		{
			mu = sqrt(-P / (E*Iz));
			Vy = mu / (sqrt(1 - Oy*mu*mu*L2));

			elm_shpfunc[0, 0] = (sin(L*Vy) - sin(Vy*x) - L*Vy*cos(L*Vy) + Vy*x*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy) - L ^ 2 * Vy ^ 2 * Oy*sin(Vy*x)) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[0, 1] = 0;
			elm_shpfunc[0, 2] = (sin(Vy*x) - Vy*x*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(Vy*x)) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[0, 3] = -((Oy*L ^ 2 * Vy ^ 2 + 1)*(L*sin(Vy*x) - x*sin(L*Vy))) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));

			elm_shpfunc[1, 0] = (Vy*(cos(L*Vy) - cos(Vy*x))) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[1, 1] = 0;
			elm_shpfunc[1, 2] = -(Vy*(cos(L*Vy) - cos(Vy*x))) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[1, 3] = (sin(L*Vy) - L*Vy*cos(Vy*x) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy)) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
		}

		/* --------------------------------------------- Null axial force: Cubic Functions --------------------------------------------- */
		else
		{
			P = 0.0;

			elm_shpfunc[0, 0] = -((L - x)*(L*x - 6 * L2 * Oy - 2 * L2 + x2)) / (2 * L3 * (3 * Oy + 1));
			elm_shpfunc[0, 1] = 0;
			elm_shpfunc[0, 2] = (x*(6 * L2 * Oy + 3 * L2 - x2)) / (2 * L3 * (3 * Oy + 1));
			elm_shpfunc[0, 3] = -(x*(L2 - x2)) / (2 * L2 * (3 * Oy + 1));

			elm_shpfunc[1, 0] = -(3 * (L2 - x2)) / (2 * L3 * (3 * Oy + 1));
			elm_shpfunc[1, 1] = 0;
			elm_shpfunc[1, 2] = (3 * (L2 - x2)) / (2 * L3 * (3 * Oy + 1));
			elm_shpfunc[1, 3] = (6 * L2 * Oy - L2 + 3 * x2) / (2 * L2 * (3 * Oy + 1));
		}
		break;

		/* ================================================================================ Element fixed at left and free at right ================================================================================ */
	case "fixed-free":
		/* --------------------------------------------- Positive axial force --------------------------------------------- */
		if (fac > toler)
		{
			mu = sqrt(P / (E*Iz));
			Vy = mu / (sqrt(1 + Oy*mu*mu*L2));

			elm_shpfunc[0, 0] = -(sinh(Vy*(L - x)) - L*Vy*cosh(L*Vy) + Vy*x*cosh(L*Vy) - L ^ 2 * Vy ^ 2 * Oy*sinh(Vy*(L - x))) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[0, 1] = ((Oy*L ^ 2 * Vy ^ 2 - 1)*(x*sinh(L*Vy) - L*sinh(L*Vy) + L*sinh(Vy*(L - x)))) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[0, 2] = (sinh(Vy*(L - x)) - sinh(L*Vy) + Vy*x*cosh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy) - L ^ 2 * Vy ^ 2 * Oy*sinh(Vy*(L - x))) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[0, 3] = 0;

			elm_shpfunc[1, 0] = -(Vy*(cosh(L*Vy) - cosh(Vy*(L - x)))) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[1, 1] = (L*Vy*cosh(Vy*(L - x)) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy)) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[1, 2] = (Vy*(cosh(L*Vy) - cosh(Vy*(L - x)))) / (L*Vy*cosh(L*Vy) - sinh(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sinh(L*Vy));
			elm_shpfunc[1, 3] = 0;
		}

		/* --------------------------------------------- Negative axial force --------------------------------------------- */
		else if (fac < -toler)
		{
			mu = sqrt(-P / (E*Iz));
			Vy = mu / (sqrt(1 - Oy*mu*mu*L2));

			elm_shpfunc[0, 0] = (sin(Vy*(L - x)) - L*Vy*cos(L*Vy) + Vy*x*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(Vy*(L - x))) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[0, 1] = ((Oy*L ^ 2 * Vy ^ 2 + 1)*(x*sin(L*Vy) - L*sin(L*Vy) + L*sin(Vy*(L - x)))) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[0, 2] = -(sin(Vy*(L - x)) - sin(L*Vy) + Vy*x*cos(L*Vy) - L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(Vy*(L - x))) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[0, 3] = 0;

			elm_shpfunc[1, 0] = (Vy*(cos(L*Vy) - cos(Vy*(L - x)))) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[1, 1] = (sin(L*Vy) - L*Vy*cos(Vy*(L - x)) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy)) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[1, 2] = -(Vy*(cos(L*Vy) - cos(Vy*(L - x)))) / (sin(L*Vy) - L*Vy*cos(L*Vy) + L ^ 2 * Vy ^ 2 * Oy*sin(L*Vy));
			elm_shpfunc[1, 3] = 0;
		}

		/* --------------------------------------------- Null axial force: Cubic Functions --------------------------------------------- */
		else
		{
			P = 0.0;

			elm_shpfunc[0, 0] = ((L - x)*(2 * L*x + 6 * L2 * Oy + 2 * L2 - x2)) / (2 * L3 * (3 * Oy + 1));
			elm_shpfunc[0, 1] = (x*(2 * L2 - 3 * L*x + x2)) / (2 * L2 * (3 * Oy + 1));
			elm_shpfunc[0, 2] = (x*(6 * Oy*L2 + 3 * L*x - x2)) / (2 * L3 * (3 * Oy + 1));
			elm_shpfunc[0, 3] = 0;

			elm_shpfunc[1, 0] = -(3 * x*(2 * L - x)) / (2 * L3 * (3 * Oy + 1));
			elm_shpfunc[1, 1] = (6 * L2 * Oy - 6 * L*x + 2 * L2 + 3 * x2) / (2 * L2 * (3 * Oy + 1));
			elm_shpfunc[1, 2] = (3 * x*(2 * L - x)) / (2 * L3 * (3 * Oy + 1));
			elm_shpfunc[1, 3] = 0;
		}
		break;
	}
}

/* =========================================================== End of File */