% ShpFuncBeamTimoshenko.m -  this file contains the shape functions for 
%                                Timoshenko beam theory.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% Authors - Marcos Antonio Campos Rodrigues (Departamento de Engenharia Civil, Universidade Federal do Espírito Santo)
%           Rodrigo Bird Burgos (Departamento de Estruturas e Fundações, Universidade do Estado do Rio de Janeiro)
%           Luiz Fernando Martha (Departamento de Engenharia Civil e Ambiental, Pontifícia Universidade Católica do Rio de Janeiro)
%
%            Reference:
%                M. A. C. Rodrigues. "Integrated Solutions for the Formulations of The Geometric Nonlinearity Problem."
%                DSc. Thesis - Pontifícia Universidade Católica do Rio de Janeiro,
%                Departamento de Engenharia Civil e Ambiental, 2019.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% function [shp_fun] = ShpFuncBeamTimoshenko(P,x,L,E,G,Ay,Iz,endlib)
%
%      P  - Axial Force(element final node)                         (  in )
%      x - Current position in the element;                         (  in )
%      L - Current element length;                                  (  in )
%      E - Young's modulus;                                         (  in )
%      G - Shear modulus;                                           (  in )
%      Ay - Effective shear area in y direct.;                      (  in )
%      Iz - Mom. of inertia wrt "z" axis;                           (  in )
%      endlib - Element end liberations                             (  in )
%
%      shp_fun - Shape functions matrix (2x4)                       ( out )
%                [ N2v N3v N4v N6v
%                  N2t N3t N4t N6t ]  
%      First line contains interpolation functions for transversal 
%      displacement (Nv)
%      Second line contains interpolation functions for cross-sectional
%      rotation  (Nt)
%
%      Returns shape functions for a Timoshenko beam element.
%      This function calculates the shape functions for an element considering the Timoshenko beam theory.
%      This shape functions were obtained directly from the solution of the equilibrium differential equation of an deformed infinitesimal element (Rodrigues, 2019).
%     
% ----------------------------------------------------------------------------------------------------------------------------------------------

function [shp_fun] = ShpFuncBeamTimoshenko(P,x,L,E,G,Ay,Iz,endlib)

shp_fun = zeros(2,4);

Oy = (E*Iz)/(G*Ay*L^2);     % Shear to Flexural rig. ratio y 

switch (endlib)
% =============== Element fixed at both ends =============== %
case "fixed-fixed"     
% ========== Positive Axial Force ========== %
if (P >= 0)                       
mu = sqrt(P / (E*Iz));
Vy = mu / (sqrt(1 + Oy*mu*mu*L^2));
shp_fun(1,1) = -(cosh(L*Vy) - cosh(Vy*x) + cosh(Vy*(L - x)) + L^2*Vy^2*Oy - L*Vy*sinh(L*Vy) + Vy*x*sinh(L*Vy)...
               - L^2*Vy^2*Oy*cosh(L*Vy) + L^2*Vy^2*Oy*cosh(Vy*x) - L^2*Vy^2*Oy*cosh(Vy*(L - x)) - 1)...
               /(L*Vy*sinh(L*Vy) - 2*L^2*Vy^2*Oy - 2*cosh(L*Vy) + 2*L^2*Vy^2*Oy*cosh(L*Vy) + 2);
shp_fun(1,2) = ((Oy*L^2*Vy^2 - 1)*(sinh(L*Vy) - sinh(Vy*x) - sinh(Vy*(L - x)) - Vy*x - L*Vy*cosh(L*Vy) + Vy*x*cosh(L*Vy)...
               + L*Vy*cosh(Vy*(L - x)) - L^2*Vy^2*Oy*sinh(L*Vy) + L^2*Vy^2*Oy*sinh(Vy*x) + L^2*Vy^2*Oy*sinh(Vy*(L - x))))...
               /(Vy*(L*Vy*sinh(L*Vy) - 2*L^2*Vy^2*Oy - 2*cosh(L*Vy) + 2*L^2*Vy^2*Oy*cosh(L*Vy) + 2));
shp_fun(1,3)= (cosh(Vy*(L - x)) - cosh(Vy*x) - cosh(L*Vy) - L^2*Vy^2*Oy + Vy*x*sinh(L*Vy)...
              + L^2*Vy^2*Oy*cosh(L*Vy) + L^2*Vy^2*Oy*cosh(Vy*x) - L^2*Vy^2*Oy*cosh(Vy*(L - x)) + 1)...
              /(L*Vy*sinh(L*Vy) - 2*L^2*Vy^2*Oy - 2*cosh(L*Vy) + 2*L^2*Vy^2*Oy*cosh(L*Vy) + 2);
shp_fun(1,4) = -((Oy*L^2*Vy^2 - 1)*(sinh(L*Vy) - sinh(Vy*x) - sinh(Vy*(L - x)) + Vy*x - L*Vy + L*Vy*cosh(Vy*x)...
               - Vy*x*cosh(L*Vy) - L^2*Vy^2*Oy*sinh(L*Vy) + L^2*Vy^2*Oy*sinh(Vy*x) + L^2*Vy^2*Oy*sinh(Vy*(L - x))))...
               /(Vy*(L*Vy*sinh(L*Vy) - 2*L^2*Vy^2*Oy - 2*cosh(L*Vy) + 2*L^2*Vy^2*Oy*cosh(L*Vy) + 2));
shp_fun(2,1) = (Vy*(sinh(Vy*x) - sinh(L*Vy) + sinh(Vy*(L - x))))...
               /(L*Vy*sinh(L*Vy) - 2*L^2*Vy^2*Oy - 2*cosh(L*Vy) + 2*L^2*Vy^2*Oy*cosh(L*Vy) + 2);
shp_fun(2,2) = (cosh(Vy*x) - cosh(L*Vy) - cosh(Vy*(L - x)) - L^2*Vy^2*Oy + L*Vy*sinh(Vy*(L - x)) + L^2*Vy^2*Oy*cosh(L*Vy)...
               - L^2*Vy^2*Oy*cosh(Vy*x) + L^2*Vy^2*Oy*cosh(Vy*(L - x)) + 1)...
               /(L*Vy*sinh(L*Vy) - 2*L^2*Vy^2*Oy - 2*cosh(L*Vy) + 2*L^2*Vy^2*Oy*cosh(L*Vy) + 2);
shp_fun(2,3) = -(Vy*(sinh(Vy*x) - sinh(L*Vy) + sinh(Vy*(L - x))))...
               /(L*Vy*sinh(L*Vy) - 2*L^2*Vy^2*Oy - 2*cosh(L*Vy) + 2*L^2*Vy^2*Oy*cosh(L*Vy) + 2);
shp_fun(2,4) = (cosh(Vy*(L - x)) - cosh(Vy*x) - cosh(L*Vy) - L^2*Vy^2*Oy + L*Vy*sinh(Vy*x) + L^2*Vy^2*Oy*cosh(L*Vy)...
               + L^2*Vy^2*Oy*cosh(Vy*x) - L^2*Vy^2*Oy*cosh(Vy*(L - x)) + 1)...
               /(L*Vy*sinh(L*Vy) - 2*L^2*Vy^2*Oy - 2*cosh(L*Vy) + 2*L^2*Vy^2*Oy*cosh(L*Vy) + 2);
end

% ========== Negative Axial Force ========== %
if (P < 0)               
mu = sqrt(-P / (E*Iz));
Vy = mu / (sqrt(1 - Oy*mu*mu*L^2));

shp_fun(1,1) = (cos(L*Vy) - cos(Vy*x) + cos(Vy*(L - x)) - L^2*Vy^2*Oy + L*Vy*sin(L*Vy) - Vy*x*sin(L*Vy)...
               + L^2*Vy^2*Oy*cos(L*Vy) - L^2*Vy^2*Oy*cos(Vy*x) + L^2*Vy^2*Oy*cos(Vy*(L - x)) - 1)...
               /(2*cos(L*Vy) - 2*L^2*Vy^2*Oy + L*Vy*sin(L*Vy) + 2*L^2*Vy^2*Oy*cos(L*Vy) - 2);
shp_fun(1,2) = -((Oy*L^2*Vy^2 + 1)*(sin(Vy*x) - sin(L*Vy) + sin(Vy*(L - x)) + Vy*x + L*Vy*cos(L*Vy) - Vy*x*cos(L*Vy)...
               - L*Vy*cos(Vy*(L - x)) - L^2*Vy^2*Oy*sin(L*Vy) + L^2*Vy^2*Oy*sin(Vy*x) + L^2*Vy^2*Oy*sin(Vy*(L - x))))...
               /(Vy*(2*cos(L*Vy) - 2*L^2*Vy^2*Oy + L*Vy*sin(L*Vy) + 2*L^2*Vy^2*Oy*cos(L*Vy) - 2));
shp_fun(1,3) = (cos(L*Vy) + cos(Vy*x) - cos(Vy*(L - x)) - L^2*Vy^2*Oy + Vy*x*sin(L*Vy)...
               + L^2*Vy^2*Oy*cos(L*Vy) + L^2*Vy^2*Oy*cos(Vy*x) - L^2*Vy^2*Oy*cos(Vy*(L - x)) - 1)...
               /(2*cos(L*Vy) - 2*L^2*Vy^2*Oy + L*Vy*sin(L*Vy) + 2*L^2*Vy^2*Oy*cos(L*Vy) - 2);
shp_fun(1,4) = ((Oy*L^2*Vy^2 + 1)*(sin(Vy*x) - sin(L*Vy) + sin(Vy*(L - x)) - Vy*x + L*Vy - L*Vy*cos(Vy*x) + Vy*x*cos(L*Vy)...
               - L^2*Vy^2*Oy*sin(L*Vy) + L^2*Vy^2*Oy*sin(Vy*x) + L^2*Vy^2*Oy*sin(Vy*(L - x))))...
               /(Vy*(2*cos(L*Vy) - 2*L^2*Vy^2*Oy + L*Vy*sin(L*Vy) + 2*L^2*Vy^2*Oy*cos(L*Vy) - 2));
shp_fun(2,1) = (Vy*(sin(Vy*x) - sin(L*Vy) + sin(Vy*(L - x))))...
               /(2*cos(L*Vy) - 2*L^2*Vy^2*Oy + L*Vy*sin(L*Vy) + 2*L^2*Vy^2*Oy*cos(L*Vy) - 2);
shp_fun(2,2) = (cos(L*Vy) - cos(Vy*x) + cos(Vy*(L - x)) - L^2*Vy^2*Oy + L*Vy*sin(Vy*(L - x)) + L^2*Vy^2*Oy*cos(L*Vy)...
               - L^2*Vy^2*Oy*cos(Vy*x) + L^2*Vy^2*Oy*cos(Vy*(L - x)) - 1)...
               /(2*cos(L*Vy) - 2*L^2*Vy^2*Oy + L*Vy*sin(L*Vy) + 2*L^2*Vy^2*Oy*cos(L*Vy) - 2);
shp_fun(2,3) = -(Vy*(sin(Vy*x) - sin(L*Vy) + sin(Vy*(L - x))))...
               /(2*cos(L*Vy) - 2*L^2*Vy^2*Oy + L*Vy*sin(L*Vy) + 2*L^2*Vy^2*Oy*cos(L*Vy) - 2);
shp_fun(2,4) = (cos(L*Vy) + cos(Vy*x) - cos(Vy*(L - x)) - L^2*Vy^2*Oy + L*Vy*sin(Vy*x) + L^2*Vy^2*Oy*cos(L*Vy)...
               + L^2*Vy^2*Oy*cos(Vy*x) - L^2*Vy^2*Oy*cos(Vy*(L - x)) - 1)...
               /(2*cos(L*Vy) - 2*L^2*Vy^2*Oy + L*Vy*sin(L*Vy) + 2*L^2*Vy^2*Oy*cos(L*Vy) - 2);
end

% =============== Element free at left and fixed at right =============== %
case "free-fixed"    
% ========== Positive Axial Force ========== %
if (P >= 0) 
mu = sqrt(P / (E*Iz));
Vy = mu / (sqrt(1 + Oy*mu*mu*L^2));

shp_fun(1,1) = -(sinh(L*Vy) - sinh(Vy*x) - L*Vy*cosh(L*Vy) + Vy*x*cosh(L*Vy) - L^2*Vy^2*Oy*sinh(L*Vy) + L^2*Vy^2*Oy*sinh(Vy*x))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(1,2) = 0;

shp_fun(1,3)= (Vy*x*cosh(L*Vy) - sinh(Vy*x) + L^2*Vy^2*Oy*sinh(Vy*x))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(1,4) = -((Oy*L^2*Vy^2 - 1)*(L*sinh(Vy*x) - x*sinh(L*Vy)))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(2,1) = -(Vy*(cosh(L*Vy) - cosh(Vy*x)))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(2,2) = 0;

shp_fun(2,3) = (Vy*(cosh(L*Vy) - cosh(Vy*x)))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(2,4) = (L*Vy*cosh(Vy*x) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));
end

% ========== Negative Axial Force ========== %
if (P < 0) 
mu = sqrt(-P / (E*Iz));
Vy = mu / (sqrt(1 - Oy*mu*mu*L^2));

shp_fun(1,1) = (sin(L*Vy) - sin(Vy*x) - L*Vy*cos(L*Vy) + Vy*x*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy) - L^2*Vy^2*Oy*sin(Vy*x))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(1,2) = 0;

shp_fun(1,3) = (sin(Vy*x) - Vy*x*cos(L*Vy) + L^2*Vy^2*Oy*sin(Vy*x))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(1,4) = -((Oy*L^2*Vy^2 + 1)*(L*sin(Vy*x) - x*sin(L*Vy)))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(2,1) = (Vy*(cos(L*Vy) - cos(Vy*x)))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(2,2) = 0;

shp_fun(2,3) = -(Vy*(cos(L*Vy) - cos(Vy*x)))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(2,4) = (sin(L*Vy) - L*Vy*cos(Vy*x) + L^2*Vy^2*Oy*sin(L*Vy))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));
end

% =============== Element fixed at left and free at right =============== %
case "fixed-free"    
% ========== Positive Axial Force ========== %
if (P >= 0) 
mu = sqrt(P / (E*Iz));
Vy = mu / (sqrt(1 + Oy*mu*mu*L^2));

shp_fun(1,1) = -(sinh(Vy*(L - x)) - L*Vy*cosh(L*Vy) + Vy*x*cosh(L*Vy) - L^2*Vy^2*Oy*sinh(Vy*(L - x)))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(1,2) = ((Oy*L^2*Vy^2 - 1)*(x*sinh(L*Vy) - L*sinh(L*Vy) + L*sinh(Vy*(L - x))))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(1,3)= (sinh(Vy*(L - x)) - sinh(L*Vy) + Vy*x*cosh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy) - L^2*Vy^2*Oy*sinh(Vy*(L - x)))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(1,4) = 0;

shp_fun(2,1) = -(Vy*(cosh(L*Vy) - cosh(Vy*(L - x))))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(2,2) = (L*Vy*cosh(Vy*(L - x)) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(2,3) = (Vy*(cosh(L*Vy) - cosh(Vy*(L - x))))/(L*Vy*cosh(L*Vy) - sinh(L*Vy) + L^2*Vy^2*Oy*sinh(L*Vy));

shp_fun(2,4) = 0;
end

% ========== Negative Axial Force ========== %
if (P < 0) 
mu = sqrt(-P / (E*Iz));
Vy = mu / (sqrt(1 - Oy*mu*mu*L^2));

shp_fun(1,1) = (sin(Vy*(L - x)) - L*Vy*cos(L*Vy) + Vy*x*cos(L*Vy) + L^2*Vy^2*Oy*sin(Vy*(L - x)))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(1,2) = ((Oy*L^2*Vy^2 + 1)*(x*sin(L*Vy) - L*sin(L*Vy) + L*sin(Vy*(L - x))))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(1,3) = -(sin(Vy*(L - x)) - sin(L*Vy) + Vy*x*cos(L*Vy) - L^2*Vy^2*Oy*sin(L*Vy) + L^2*Vy^2*Oy*sin(Vy*(L - x)))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(1,4) = 0;

shp_fun(2,1) = (Vy*(cos(L*Vy) - cos(Vy*(L - x))))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(2,2) = (sin(L*Vy) - L*Vy*cos(Vy*(L - x)) + L^2*Vy^2*Oy*sin(L*Vy))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(2,3) = -(Vy*(cos(L*Vy) - cos(Vy*(L - x))))/(sin(L*Vy) - L*Vy*cos(L*Vy) + L^2*Vy^2*Oy*sin(L*Vy));

shp_fun(2,4) = 0;
end

end