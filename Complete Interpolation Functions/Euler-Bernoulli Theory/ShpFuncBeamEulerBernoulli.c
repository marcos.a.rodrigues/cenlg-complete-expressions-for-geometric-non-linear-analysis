/*
** ShpFuncBeamEulerBernoulli.c -  this file contains the shape functions for
**                                Euler-Bernoulli beam theory.
**
** ----------------------------------------------------------------------------------------------------------------------------------------------
** Authors - Marcos Antonio Campos Rodrigues (Departamento de Engenharia Civil, Universidade Federal do Espírito Santo)
**           Rodrigo Bird Burgos (Departamento de Estruturas e Fundações, Universidade do Estado do Rio de Janeiro)
**			 Luiz Fernando Martha (Departamento de Engenharia Civil e Ambiental, Pontifícia Universidade Católica do Rio de Janeiro)
**                
**            Reference:
**                M. A. C. Rodrigues. "Integrated Solutions for the Formulations of The Geometric Nonlinearity Problem."
**                DSc. Thesis - Pontifícia Universidade Católica do Rio de Janeiro, 
**                Departamento de Engenharia Civil e Ambiental, 2019.
**                
** ----------------------------------------------------------------------------------------------------------------------------------------------
**
** static void ShpFuncBeamEulerBernoulli(double P, double x, double L, double E, double Iz,char endlib, double *elm_shpfunc)
**
**      P  - Axial Force(element final node)                                           (  in )
**      x - Current position in the element;                                           (  in )
**      L - Current element length;                                                    (  in )
**      E - Young's modulus;                                                           (  in )
**      Iz - Mom. of inertia wrt "z" axis;                                             (  in )
**      endlib -  Element end liberations                                              (  in )
**
**      elm_shpfunc - Shape functions matrix (2x4)                                     ( out )
**                      [ N2v N3v N4v N6v
**                        N2t N3t N4t N6t ]
**      First line contains interpolation functions for transversal displacement (Nv)
**      Second line contains interpolation functions for cross-sectional rotation  (Nt)
**
**      Returns shape functions for a Euler-Bernoulli beam element.
**      This function calculates the shape functions for an element considering the Euler-Bernoulli beam theory.
**      This shape functions were obtained directly from the solution of the equilibrium differential equation of an deformed infinitesimal element (Rodrigues, 2019).
**
*/

/* ====================== ShpFuncBeamEulerBernoulli ========================== */

static void ShpFuncBeamEulerBernoulli(double P, double x, double L, double E, double Iz, char endlib, double *elm_shpfunc)

{
	double  Imin;                    /* minimum cross section mom. inertia */
	double  PE;                      /* Euler critical load */
	double  fac;                     /* factor axial force to Euler load */
	double  toler = 0.001;           /* tolerance for checking null axial force */
	double  L2 = L*L;
	double  L3 = L2*L;
	double  x2 = x*x;
	double  x3 = x2*x;
	double  mu;
	double  pi = acos(-1.0);

	if ((Iy != 0.0) && (Iz != 0.0))
	{
		Imin = (Iy < Iz) ? Iy : Ix;
	}
	else if ((Iy == 0.0) && (Iz != 0.0))
	{
		Imin = Iz;
	}
	else if ((Iy != 0.0) && (Iz == 0.0))
	{
		Imin = Iy;
	}
	else
	{
		return;
	}

	PE = pi*pi*E*Imin / L2;

	fac = P / PE;                    /* Verifies if it is important consider axial load in the problem - Tries to avoid numerical instability */

	switch (endlib)
	{
		/* ================================================================================ Element fixed at both ends ================================================================================ */
	case "fixed-fixed":
		/* --------------------------------------------- Positive axial force --------------------------------------------- */
		if (fac > toler)
		{
			mu = sqrt(P / (E*Iz));

			elm_shpfunc[0, 0] = -(cosh(L*mu) - cosh(mu*x) + cosh(mu*(L - x)) - L*mu*sinh(L*mu) + mu*x*sinh(L*mu) - 1) / (L*mu*sinh(L*mu) - 2 * cosh(L*mu) + 2);
			elm_shpfunc[0, 1] = (sinh(mu*x) - sinh(L*mu) + sinh(mu*(L - x)) + mu*x + L*mu*cosh(L*mu) - mu*x*cosh(L*mu) - L*mu*cosh(mu*(L - x))) / (mu*(L*mu*sinh(L*mu) - 2 * cosh(L*mu) + 2));
			elm_shpfunc[0, 2] = (cosh(mu*(L - x)) - cosh(mu*x) - cosh(L*mu) + mu*x*sinh(L*mu) + 1) / (L*mu*sinh(L*mu) - 2 * cosh(L*mu) + 2);
			elm_shpfunc[0, 3] = -(sinh(mu*x) - sinh(L*mu) + sinh(mu*(L - x)) + L*mu - mu*x - L*mu*cosh(mu*x) + mu*x*cosh(L*mu)) / (mu*(L*mu*sinh(L*mu) - 2 * cosh(L*mu) + 2));

			elm_shpfunc[1, 0] = (mu*(sinh(mu*x) - sinh(L*mu) + sinh(mu*(L - x)))) / (L*mu*sinh(L*mu) - 2 * cosh(L*mu) + 2);
			elm_shpfunc[1, 1] = (cosh(mu*x) - cosh(L*mu) - cosh(mu*(L - x)) + L*mu*sinh(mu*(L - x)) + 1) / (L*mu*sinh(L*mu) - 2 * cosh(L*mu) + 2);
			elm_shpfunc[1, 2] = -(mu*(sinh(mu*x) - sinh(L*mu) + sinh(mu*(L - x)))) / (L*mu*sinh(L*mu) - 2 * cosh(L*mu) + 2);
			elm_shpfunc[1, 3] = (cosh(mu*(L - x)) - cosh(mu*x) - cosh(L*mu) + L*mu*sinh(mu*x) + 1) / (L*mu*sinh(L*mu) - 2 * cosh(L*mu) + 2);
		}

		/* --------------------------------------------- Negative axial force --------------------------------------------- */
		else if (fac < -toler)
		{
			mu = sqrt(-P / (E*Iz));

			elm_shpfunc[0, 0] = (cos(L*mu) - cos(mu*x) + cos(mu*(L - x)) + L*mu*sin(L*mu) - mu*x*sin(L*mu) - 1) / (2 * cos(L*mu) + L*mu*sin(L*mu) - 2);
			elm_shpfunc[0, 1] = -(sin(mu*x) - sin(L*mu) + sin(mu*(L - x)) + mu*x + L*mu*cos(L*mu) - mu*x*cos(L*mu) - L*mu*cos(mu*(L - x))) / (mu*(2 * cos(L*mu) + L*mu*sin(L*mu) - 2));
			elm_shpfunc[0, 2] = (cos(L*mu) + cos(mu*x) - cos(mu*(L - x)) + mu*x*sin(L*mu) - 1) / (2 * cos(L*mu) + L*mu*sin(L*mu) - 2);
			elm_shpfunc[0, 3] = (sin(mu*x) - sin(L*mu) + sin(mu*(L - x)) + L*mu - mu*x - L*mu*cos(mu*x) + mu*x*cos(L*mu)) / (mu*(2 * cos(L*mu) + L*mu*sin(L*mu) - 2));

			elm_shpfunc[1, 0] = (mu*(sin(mu*x) - sin(L*mu) + sin(mu*(L - x)))) / (2 * cos(L*mu) + L*mu*sin(L*mu) - 2);
			elm_shpfunc[1, 1] = (cos(L*mu) - cos(mu*x) + cos(mu*(L - x)) + L*mu*sin(mu*(L - x)) - 1) / (2 * cos(L*mu) + L*mu*sin(L*mu) - 2);
			elm_shpfunc[1, 2] = -(mu*(sin(mu*x) - sin(L*mu) + sin(mu*(L - x)))) / (2 * cos(L*mu) + L*mu*sin(L*mu) - 2);
			elm_shpfunc[1, 3] = (cos(L*mu) + cos(mu*x) - cos(mu*(L - x)) + L*mu*sin(mu*x) - 1) / (2 * cos(L*mu) + L*mu*sin(L*mu) - 2);
		}

		/* --------------------------------------------- Null axial force: Cubic Functions --------------------------------------------- */
		else
		{
			P = 0.0;

			elm_shpfunc[0, 0] = (2 * x3) / L3 - (3 * x2) / L2 + 1;
			elm_shpfunc[0, 1] = x - (2 * x2) / L + x3 / L2;
			elm_shpfunc[0, 2] = (3 * x2) / L2 - (2 * x3) / L3;
			elm_shpfunc[0, 3] = x3 / L2 - x2 / L;

			elm_shpfunc[1, 0] = (6 * x2) / L3 - (6 * x) / L2;
			elm_shpfunc[1, 1] = (3 * x2) / L2 - (4 * x) / L + 1;
			elm_shpfunc[1, 2] = (6 * x) / L2 - (6 * x2) / L3;
			elm_shpfunc[1, 3] = (3 * x2) / L2 - (2 * x) / L;
		}
		break;

		/* ================================================================================ Element free at left and fixed at right ================================================================================ */
	case "free-fixed":
		/* --------------------------------------------- Positive axial force --------------------------------------------- */
		if (fac > toler)
		{
			mu = sqrt(P / (E*Iz));

			elm_shpfunc[0, 0] = (sinh(L*mu) - sinh(mu*x) - L*mu*cosh(L*mu) + mu*x*cosh(L*mu)) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[0, 1] = 0;
			elm_shpfunc[0, 2] = (sinh(mu*x) - mu*x*cosh(L*mu)) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[0, 3] = -(L*sinh(mu*x) - x*sinh(L*mu)) / (sinh(L*mu) - L*mu*cosh(L*mu));

			elm_shpfunc[1, 0] = (mu*(cosh(L*mu) - cosh(mu*x))) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[1, 1] = 0;
			elm_shpfunc[1, 2] = -(mu*(cosh(L*mu) - cosh(mu*x))) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[1, 3] = (sinh(L*mu) - L*mu*cosh(mu*x)) / (sinh(L*mu) - L*mu*cosh(L*mu));
		}

		/* --------------------------------------------- Negative axial force --------------------------------------------- */
		else if (fac < -toler)
		{
			mu = sqrt(-P / (E*Iz));

			elm_shpfunc[0, 0] = (sin(L*mu) - sin(mu*x) - L*mu*cos(L*mu) + mu*x*cos(L*mu)) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[0, 1] = 0;
			elm_shpfunc[0, 2] = (sin(mu*x) - mu*x*cos(L*mu)) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[0, 3] = -(L*sin(mu*x) - x*sin(L*mu)) / (sin(L*mu) - L*mu*cos(L*mu));

			elm_shpfunc[1, 0] = (mu*(cos(L*mu) - cos(mu*x))) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[1, 1] = 0;
			elm_shpfunc[1, 2] = -(mu*(cos(L*mu) - cos(mu*x))) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[1, 3] = (sin(L*mu) - L*mu*cos(mu*x)) / (sin(L*mu) - L*mu*cos(L*mu));
		}

		/* --------------------------------------------- Null axial force: Cubic Functions --------------------------------------------- */
		else
		{
			P = 0.0;

			elm_shpfunc[0, 0] = x3 / (2 * L3) - (3 * x) / (2 * L) + 1;
			elm_shpfunc[0, 1] = 0;
			elm_shpfunc[0, 2] = -x3 / (2 * L3) + (3 * x) / (2 * L);
			elm_shpfunc[0, 3] = x3 / (2 * L2) - x / 2

				elm_shpfunc[1, 0] = (3 * x2) / (2 * L3) - 3 / (2 * L);
			elm_shpfunc[1, 1] = 0;
			elm_shpfunc[1, 2] = 3 / (2 * L) - (3 * x2) / (2 * L3);
			elm_shpfunc[1, 3] = (3 * x2) / (2 * L2) - 1 / 2;
		}
		break;

		/* ================================================================================ Element fixed at left and free at right ================================================================================ */
	case "fixed-free":
		/* --------------------------------------------- Positive axial force --------------------------------------------- */
		if (fac > toler)
		{
			mu = sqrt(P / (E*Iz));

			elm_shpfunc[0, 0] = (sinh(mu*(L - x)) - L*mu*cosh(L*mu) + mu*x*cosh(L*mu)) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[0, 1] = (x*sinh(L*mu) - L*sinh(L*mu) + L*sinh(mu*(L - x))) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[0, 2] = -(sinh(mu*(L - x)) - sinh(L*mu) + mu*x*cosh(L*mu)) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[0, 3] = 0;

			elm_shpfunc[1, 0] = (mu*(cosh(L*mu) - cosh(mu*(L - x)))) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[1, 1] = (sinh(L*mu) - L*mu*cosh(mu*(L - x))) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[1, 2] = -(mu*(cosh(L*mu) - cosh(mu*(L - x)))) / (sinh(L*mu) - L*mu*cosh(L*mu));
			elm_shpfunc[1, 3] = 0;
		}

		/* --------------------------------------------- Negative axial force --------------------------------------------- */
		else if (fac < -toler)
		{
			mu = sqrt(-P / (E*Iz));

			elm_shpfunc[0, 0] = (sin(mu*(L - x)) - L*mu*cos(L*mu) + mu*x*cos(L*mu)) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[0, 1] = (x*sin(L*mu) - L*sin(L*mu) + L*sin(mu*(L - x))) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[0, 2] = -(sin(mu*(L - x)) - sin(L*mu) + mu*x*cos(L*mu)) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[0, 3] = 0;

			elm_shpfunc[1, 0] = (mu*(cos(L*mu) - cos(mu*(L - x)))) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[1, 1] = (sin(L*mu) - L*mu*cos(mu*(L - x))) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[1, 2] = -(mu*(cos(L*mu) - cos(mu*(L - x)))) / (sin(L*mu) - L*mu*cos(L*mu));
			elm_shpfunc[1, 3] = 0;
		}

		/* --------------------------------------------- Null axial force: Cubic Functions --------------------------------------------- */
		else
		{
			P = 0.0;

			elm_shpfunc[0, 0] = x3 / (2 * L3) - (3 * x2) / (2 * L2) + 1;
			elm_shpfunc[0, 1] = x - (3 * x2) / (2 * L) + x3 / (2 * L2);
			elm_shpfunc[0, 2] = (3 * x2) / (2 * L2) - x3 / (2 * L3);
			elm_shpfunc[0, 3] = 0;

			elm_shpfunc[1, 0] = (3 * x2) / (2 * L3) - (3 * x) / L2;
			elm_shpfunc[1, 1] = (3 * x2) / (2 * L2) - (3 * x) / L + 1;
			elm_shpfunc[1, 2] = (3 * x) / L2 - (3 * x2) / (2 * L3);
			elm_shpfunc[1, 3] = 0;
		}
		break;
	}
}

/* =========================================================== End of File */