% ShpFuncBeamEulerBernoulli.m -  this file contains the shape functions for 
%                                Euler-Bernoulli beam theory.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% Authors - Marcos Antonio Campos Rodrigues (Departamento de Engenharia Civil, Universidade Federal do Espírito Santo)
%           Rodrigo Bird Burgos (Departamento de Estruturas e Fundações, Universidade do Estado do Rio de Janeiro)
%           Luiz Fernando Martha (Departamento de Engenharia Civil e Ambiental, Pontifícia Universidade Católica do Rio de Janeiro)
%
%            Reference:
%                M. A. C. Rodrigues. "Integrated Solutions for the Formulations of The Geometric Nonlinearity Problem."
%                DSc. Thesis - Pontifícia Universidade Católica do Rio de Janeiro,
%                Departamento de Engenharia Civil e Ambiental, 2019.
%
% ----------------------------------------------------------------------------------------------------------------------------------------------
% function [shp_fun] = ShpFuncBeamEulerBernoulli(P,x,L,E,Iz,endlib)
%
%      P  - Axial Force(element final node)                         (  in )
%      x - Current position in the element;                         (  in )
%      L - Current element length;                                  (  in )
%      E - Young's modulus;                                         (  in )
%      Iz - Mom. of inertia wrt "z" axis;                           (  in )
%      endlib - Element end liberations                             (  in )
%
%      shp_fun - Shape functions matrix (2x4)                       ( out )
%                [ N2v N3v N4v N6v
%                  N2t N3t N4t N6t ]  
%      First line contains interpolation functions for transversal 
%      displacement (Nv)
%      Second line contains interpolation functions for cross-sectional
%      rotation  (Nt)
%
%      Returns shape functions for a Euler-Bernoulli beam element.
%      This function calculates the shape functions for an element considering the Euler-Bernoulli beam theory.
%      This shape functions were obtained directly from the solution of the equilibrium differential equation of an deformed infinitesimal element (Rodrigues, 2019).
%     
% ----------------------------------------------------------------------------------------------------------------------------------------------

function [shp_fun] = ShpFuncBeamEulerBernoulli(P,x,L,E,Iz,endlib)
shp_fun = zeros(2,4);
switch (endlib)
% =============== Element fixed at both ends =============== %
case "fixed-fixed"    
    
% ========== Positive Axial Force ========== %
if (P >= 0) 
mu = sqrt(P / (E*Iz));
shp_fun(1,1) = -(cosh(L*mu) - cosh(mu*x) + cosh(mu*(L - x)) - L*mu*sinh(L*mu) + mu*x*sinh(L*mu) - 1)...
                /(L*mu*sinh(L*mu) - 2*cosh(L*mu) + 2);
shp_fun(1,2) = (sinh(mu*x) - sinh(L*mu) + sinh(mu*(L - x)) + mu*x + L*mu*cosh(L*mu) - mu*x*cosh(L*mu) - L*mu*cosh(mu*(L - x)))...
                /(mu*(L*mu*sinh(L*mu) - 2*cosh(L*mu) + 2));
shp_fun(1,3)= (cosh(mu*(L - x)) - cosh(mu*x) - cosh(L*mu) + mu*x*sinh(L*mu) + 1)/(L*mu*sinh(L*mu) - 2*cosh(L*mu) + 2);
shp_fun(1,4) = -(sinh(mu*x) - sinh(L*mu) + sinh(mu*(L - x)) + L*mu - mu*x - L*mu*cosh(mu*x) + mu*x*cosh(L*mu))...
                /(mu*(L*mu*sinh(L*mu) - 2*cosh(L*mu) + 2));
shp_fun(2,1) = (mu*(sinh(mu*x) - sinh(L*mu) + sinh(mu*(L - x))))/(L*mu*sinh(L*mu) - 2*cosh(L*mu) + 2);
shp_fun(2,2) = (cosh(mu*x) - cosh(L*mu) - cosh(mu*(L - x)) + L*mu*sinh(mu*(L - x)) + 1)...
               /(L*mu*sinh(L*mu) - 2*cosh(L*mu) + 2);
shp_fun(2,3) = -(mu*(sinh(mu*x) - sinh(L*mu) + sinh(mu*(L - x))))/(L*mu*sinh(L*mu) - 2*cosh(L*mu) + 2);
shp_fun(2,4) = (cosh(mu*(L - x)) - cosh(mu*x) - cosh(L*mu) + L*mu*sinh(mu*x) + 1)...
               /(L*mu*sinh(L*mu) - 2*cosh(L*mu) + 2);
end
% ========== Negative Axial Force ========== %
if (P < 0) 
mu = sqrt(-P / (E*Iz));
shp_fun(1,1) = (cos(L*mu) - cos(mu*x) + cos(mu*(L - x)) + L*mu*sin(L*mu) - mu*x*sin(L*mu) - 1)...
               /(2*cos(L*mu) + L*mu*sin(L*mu) - 2);
shp_fun(1,2) = -(sin(mu*x) - sin(L*mu) + sin(mu*(L - x)) + mu*x + L*mu*cos(L*mu) - mu*x*cos(L*mu) - L*mu*cos(mu*(L - x)))...
               /(mu*(2*cos(L*mu) + L*mu*sin(L*mu) - 2));
shp_fun(1,3) = (cos(L*mu) + cos(mu*x) - cos(mu*(L - x)) + mu*x*sin(L*mu) - 1)...
               /(2*cos(L*mu) + L*mu*sin(L*mu) - 2);
shp_fun(1,4) = (sin(mu*x) - sin(L*mu) + sin(mu*(L - x)) + L*mu - mu*x - L*mu*cos(mu*x) + mu*x*cos(L*mu))...
               /(mu*(2*cos(L*mu) + L*mu*sin(L*mu) - 2));
shp_fun(2,1) = (mu*(sin(mu*x) - sin(L*mu) + sin(mu*(L - x))))/(2*cos(L*mu) + L*mu*sin(L*mu) - 2);
shp_fun(2,2) = (cos(L*mu) - cos(mu*x) + cos(mu*(L - x)) + L*mu*sin(mu*(L - x)) - 1)...
               /(2*cos(L*mu) + L*mu*sin(L*mu) - 2);
shp_fun(2,3) = -(mu*(sin(mu*x) - sin(L*mu) + sin(mu*(L - x))))/(2*cos(L*mu) + L*mu*sin(L*mu) - 2);
shp_fun(2,4) = (cos(L*mu) + cos(mu*x) - cos(mu*(L - x)) + L*mu*sin(mu*x) - 1)...
               /(2*cos(L*mu) + L*mu*sin(L*mu) - 2);
end

% =============== Element free at left and fixed at right =============== %
case "free-fixed"    
% ========== Positive Axial Force ========== %
if (P >= 0) 
mu = sqrt(P / (E*Iz));

shp_fun(1,1) = (sinh(L*mu) - sinh(mu*x) - L*mu*cosh(L*mu) + mu*x*cosh(L*mu))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(1,2) = 0;

shp_fun(1,3)= (sinh(mu*x) - mu*x*cosh(L*mu))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(1,4) = -(L*sinh(mu*x) - x*sinh(L*mu))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(2,1) = (mu*(cosh(L*mu) - cosh(mu*x)))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(2,2) = 0;

shp_fun(2,3) = -(mu*(cosh(L*mu) - cosh(mu*x)))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(2,4) = (sinh(L*mu) - L*mu*cosh(mu*x))/(sinh(L*mu) - L*mu*cosh(L*mu));
end

% ========== Negative Axial Force ========== %
if (P < 0) 
mu = sqrt(-P / (E*Iz));

shp_fun(1,1) = (sin(L*mu) - sin(mu*x) - L*mu*cos(L*mu) + mu*x*cos(L*mu))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(1,2) = 0;

shp_fun(1,3) = (sin(mu*x) - mu*x*cos(L*mu))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(1,4) = -(L*sin(mu*x) - x*sin(L*mu))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(2,1) = (mu*(cos(L*mu) - cos(mu*x)))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(2,2) = 0;

shp_fun(2,3) = -(mu*(cos(L*mu) - cos(mu*x)))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(2,4) = (sin(L*mu) - L*mu*cos(mu*x))/(sin(L*mu) - L*mu*cos(L*mu));
end

% =============== Element fixed at left and free at right =============== %
case "fixed-free"    
% ========== Positive Axial Force ========== %
if (P >= 0) 
mu = sqrt(P / (E*Iz));

shp_fun(1,1) = (sinh(mu*(L - x)) - L*mu*cosh(L*mu) + mu*x*cosh(L*mu))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(1,2) = (x*sinh(L*mu) - L*sinh(L*mu) + L*sinh(mu*(L - x)))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(1,3)= -(sinh(mu*(L - x)) - sinh(L*mu) + mu*x*cosh(L*mu))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(1,4) = 0;

shp_fun(2,1) = (mu*(cosh(L*mu) - cosh(mu*(L - x))))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(2,2) = (sinh(L*mu) - L*mu*cosh(mu*(L - x)))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(2,3) = -(mu*(cosh(L*mu) - cosh(mu*(L - x))))/(sinh(L*mu) - L*mu*cosh(L*mu));

shp_fun(2,4) = 0;
end

% ========== Negative Axial Force ========== %
if (P < 0) 
mu = sqrt(-P / (E*Iz));

shp_fun(1,1) = (sin(mu*(L - x)) - L*mu*cos(L*mu) + mu*x*cos(L*mu))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(1,2) = (x*sin(L*mu) - L*sin(L*mu) + L*sin(mu*(L - x)))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(1,3) = -(sin(mu*(L - x)) - sin(L*mu) + mu*x*cos(L*mu))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(1,4) = 0;

shp_fun(2,1) = (mu*(cos(L*mu) - cos(mu*(L - x))))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(2,2) = (sin(L*mu) - L*mu*cos(mu*(L - x)))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(2,3) = -(mu*(cos(L*mu) - cos(mu*(L - x))))/(sin(L*mu) - L*mu*cos(L*mu));

shp_fun(2,4) = 0;
end

end